<?php

use Illuminate\Database\Seeder;

class JobTitlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('job_titles')->insert([
            [
                'name' => 'Maintenance Manager',
            ],
            [
                'name' => 'Maintenance Engineer',
            ],
            [
                'name' => 'Procurement',
            ],
            [
                'name' => 'Business Owner',
            ],
            [
                'name' => 'Sales',
            ]
        ]);
    }
}
