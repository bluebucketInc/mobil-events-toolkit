<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'code' => str_random(30),
            'first_name' => 'First',
            'last_name' => 'User',
            'company' => 'Jobless',
            'email' => 'user@mail.com',
            'password' => bcrypt('password'),
        ]);
    }
}
