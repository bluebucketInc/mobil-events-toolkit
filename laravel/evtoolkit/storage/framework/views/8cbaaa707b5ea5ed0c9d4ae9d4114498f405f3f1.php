<?php $__env->startSection('page-title', 'Email Templates'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active">Email templates</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
	<div class="col-md-3">
		<a href="<?php echo e(route('admin.emailTemplates.syncFromSendinblue')); ?>" class="btn btn-primary btn-block margin-bottom">Sync from SendinBlue</a>
	</div>
</div>

<div class="row">
	<?php $__currentLoopData = $templates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $template): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<div class="col-md-3">
			<div class="box">
				<div class="box-head">
					<a href="<?php echo e(url('uploads/email_templates/' . $template->image_preview_filename)); ?>" class="template-popup">
						<img src="<?php echo e(asset('uploads/email_templates/t-' . $template->image_preview_filename)); ?>" class="img-responsive">
					</a>
				</div>
				<div class="box-footer">
					<a href="<?php echo e(route('admin.emailTemplates.edit', $template->id)); ?>" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i> Edit</a>
					<?php if($template->active): ?>
						<a href="#" class="btn btn-success btn-xs" onclick="event.preventDefault(); deactivate(<?php echo e($template->id); ?>)"><i class="fa fa-check-square-o"></i> Active</a>
						<?php echo Form::open( [ 'method' => 'PUT', 'route' => [ 'admin.emailTemplates.deactivate', $template->id ], 'id' => 'form-emailtemplate-deactivate-' . $template->id ] ); ?>

						<?php echo Form::close(); ?>

					<?php else: ?>
						<a href="#" class="btn btn-default btn-xs" onclick="event.preventDefault(); activate(<?php echo e($template->id); ?>)"><i class="fa fa-square-o"></i> Inactive</a>
						<?php echo Form::open( [ 'method' => 'PUT', 'route' => [ 'admin.emailTemplates.activate', $template->id ], 'id' => 'form-emailtemplate-activate-' . $template->id ] ); ?>

						<?php echo Form::close(); ?>

					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<style>
a.template-popup {
	cursor: zoom-in;
}
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script>

function activate(id) {
	$('#form-emailtemplate-activate-' + id).submit();
}
function deactivate(id) {
	$('#form-emailtemplate-deactivate-' + id).submit();
}
$('.template-popup').magnificPopup({type:'image'});

</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>