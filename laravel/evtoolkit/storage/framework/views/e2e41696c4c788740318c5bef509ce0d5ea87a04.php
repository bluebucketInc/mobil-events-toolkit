<?php $__env->startSection('content'); ?>
<section style="background: url(<?php echo e(asset('assets/images/bg-03.jpg')); ?>) no-repeat center center; background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-mobil">
                    <?php echo Form::open(['route' => ['leadform.submit', $user->code]]); ?>

                    <div class="panel-heading">
                        <h3 class="panel-title"><b>Customer Contact Form</b></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <?php echo Form::label('first_name'); ?>

                                <?php echo Form::text('first_name', old('first_name'), ['class' => 'form-control', 'maxlength' => '191']); ?>

                            </div>
                            <div class="form-group col-md-6">
                                <?php echo Form::label('last_name'); ?>

                                <?php echo Form::text('last_name', old('last_name'), ['class' => 'form-control', 'maxlength' => '191']); ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <?php echo Form::label('email'); ?>

                                <?php echo Form::text('email', old('email'), ['class' => 'form-control', 'maxlength' => '191']); ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <?php echo Form::label('company'); ?>

                                <?php echo Form::text('company', old('company'), ['class' => 'form-control', 'maxlength' => '191']); ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="sector_id">Sector</label>
                                <select class="form-control select2" data-placeholder="Choose sector" id="sector_id" name="sector_id">
                                    <option></option>
                                    <?php $__currentLoopData = $sectors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sector): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($sector->id); ?>"><?php echo e($sector->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="job_title_id">Job title</label>
                                <select class="form-control select2" data-placeholder="Choose job title" id="sector_id" name="sector_id">
                                    <option></option>
                                    <?php $__currentLoopData = $jobTitles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jobTitle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($jobTitle->id); ?>"><?php echo e($jobTitle->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <div class="checkbox">
                                    <label><input name="check_permission" type="checkbox"> This person gave me permission to receive email marketing info.</label>
                                    <a href="#">What's this?</a>
                                </div>
                            </div>
                        </div>
                    </div> <!-- ./panel-body -->
                    
                    <div class="panel-footer with-border">
                        <div class="pull-right">
			                <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
            <div class="col-md-6"></div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>