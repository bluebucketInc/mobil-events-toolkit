

<?php $__env->startSection('page-title', 'Dashboard'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li><i class="fa fa-dashboard"></i> Dashboard</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-users"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Administrators</span>
                <span class="info-box-number">7</span>
                <span class="info-see-details"><a href="#">See details</a></span>
            </div>
            <!-- /.info-box-content -->
        </div>        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Users</span>
                <span class="info-box-number">34</span>
                <span class="info-see-details"><a href="#">See details</a></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div> <!-- /.col -->

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-address-card"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Contacts</span>
                <span class="info-box-number">1259</span>
                <span class="info-see-details"><a href="#">See details</a></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div> <!-- /.col -->

</div> <!-- /.row -->

<div class="row">

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-calendar"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Events</span>
                <span class="info-box-number">34</span>
                <span class="info-see-details"><a href="#">See details</a></span>
            </div>
            <!-- /.info-box-content -->
        </div>        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-file-o"></i></span>    
            <div class="info-box-content">
                <span class="info-box-text">Assets</span>
                <span class="info-box-number">82</span>
                <span class="info-see-details"><a href="#">See details</a></span>
            </div>          
            <!-- /.info-box-content -->                                     
        <!-- /.info-box -->
    </div> <!-- /.col -->

</div>
<!-- /.row -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>