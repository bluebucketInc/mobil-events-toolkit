<?php $__env->startSection('content'); ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-mobil">
                    <?php echo Form::open(['route' => 'user.update_profile']); ?>

                    <div class="panel-heading">
                        <h3 class="panel-title"><b>Update Your Profile</b></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <?php echo Form::label('first_name'); ?>

                                <?php echo Form::text('first_name', $user->first_name, ['class' => 'form-control', 'maxlength' => '191']); ?>

                            </div>
                            <div class="form-group col-md-6">
                                <?php echo Form::label('last_name'); ?>

                                <?php echo Form::text('last_name', $user->last_name, ['class' => 'form-control', 'maxlength' => '191']); ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <?php echo Form::label('company'); ?>

                                <?php echo Form::text('company', $user->company, ['class' => 'form-control', 'maxlength' => '191']); ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="sector_id">Sector</label>
                                <select class="form-control select2" data-placeholder="Choose sector" id="sector_id" name="sector_id">
                                    <?php $__currentLoopData = $jobTitles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jobTitle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($jobTitle->id); ?>" <?php echo e(($jobTitle->id == $user->job_title_id) ? 'selected' : ''); ?>><?php echo e($jobTitle->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                    </div> <!-- ./panel-body -->
                    
                    <div class="panel-footer">
                        <div class="pull-right">
			                <?php echo Form::submit('Update', ['class' => 'btn btn-primary']); ?>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>