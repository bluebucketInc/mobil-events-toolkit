<div id="modal-test-email" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                <h4 class="modal-title">Send Test Email</h4>
            </div>
            <?php echo Form::open(['route' => ['invitations.test', $invitation->id]]); ?>

            <div class="modal-body">
                <div class="form-group <?php echo e($errors->has('emails') ? 'has-error' : ''); ?>">
                    <?php echo Form::label('emails'); ?>

                    <?php echo Form::text('emails', old('emails'), ['class' => 'form-control', 'maxlength' => '191']); ?>

                    <span class="help-block"><?php echo e($errors->first('emails', '')); ?></span>
                </div>
            </div>
            <!-- /.modal-body -->
    
            <div class="modal-footer">
                <?php echo Form::submit('Send', ['class' => 'btn btn-primary']); ?>

            </div>
            <?php echo Form::close(); ?>


        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->