<?php $__env->startSection('content'); ?>

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Create New Value</h3>
	</div>
	<?php echo Form::open(['route' => 'admin.values.store']); ?>

		<div class="box-body">
			<div class="form-group">
				<?php echo Form::label('name'); ?>

				<?php echo Form::text('name', '', ['class' => 'form-control', 'maxlength' => '50']); ?>

			</div>
			<div class="form-group">
				<?php echo Form::label('value'); ?>

				<?php echo Form::textarea('value', '', ['class' => 'form-control']); ?>

			</div>
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			<?php echo Form::submit('Submit', ['class' => 'btn btn-primary']); ?>

			<a href="<?php echo e(route('admin.values.index')); ?>" class="btn btn-default">Cancel</a>
		</div>
	<?php echo Form::close(); ?>

</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>