<?php $__env->startSection('content'); ?>
<section class="padding" style="background: url('<?php echo e(asset('assets/images/bg-01.jpg')); ?>') no-repeat center center">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-mobil no-margin">
                    <?php echo Form::open(['route' => 'auth.login']); ?>

                    <div class="panel-body">
                        <h4>Registration Form</h4>
                        <p>Simply fill in the details below to register.</p>
                        <br>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <?php echo Form::label('first_name'); ?>

                                <?php echo Form::text('first_name', old('first_name'), [ 'class' => 'form-control', 'placeholder' => 'Your first name', 'required' => 'required', 'autofocus' => 'autofocus' ]); ?>

                            </div>
                            <div class="form-group col-md-6">
                                <?php echo Form::label('last_name'); ?>

                                <?php echo Form::text('last_name', old('last_name'), [ 'class' => 'form-control', 'placeholder' => 'Your last name', 'required' => 'required' ]); ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <?php echo Form::label('company'); ?>

                                <?php echo Form::text('company', old('company'), [ 'class' => 'form-control', 'placeholder' => 'Your company name', 'required' => 'required', 'autofocus' => 'autofocus' ]); ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="sector_id">Job title</label>
                                <select class="form-control select2" data-placeholder="Choose sector" id="job_title_id" name="job_title_id">
                                    <?php $__currentLoopData = $jobTitles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jobTitle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($jobTitle->id); ?>"><?php echo e($jobTitle->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo Form::label('password'); ?>

                            <?php echo Form::password('password', [ 'class' => 'form-control', 'placeholder' => 'Your password', 'required' => 'required' ]); ?>

                        </div>
                        <?php echo Form::submit('Submit', ['class' => 'btn btn-primary btn-block']); ?>

                        <div class="row margin-top">
                            <div class="col-md-6">
                                <a href="#">Forgot password?</a>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="#">Legal notice</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/front/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>