<?php $__env->startSection('page-title', 'Email Templates'); ?>

<?php $__env->startSection('content'); ?>

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Create New Email Template</h3>
	</div>
	<?php echo Form::open(['route' => 'admin.emailtemplates.store', 'files' => true]); ?>

		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<?php echo Form::label('name'); ?>

						<?php echo Form::text('name', '', ['class' => 'form-control', 'maxlength' => '50']); ?>

					</div>
					<div class="form-group">
						<?php echo Form::label('hubspot_template_id'); ?>

						<?php echo Form::text('hubspot_template_id', '', ['class' => 'form-control', 'maxlength' => '50']); ?>

					</div>		
				</div>
				<!-- /.col-md-6 -->
				<div class="col-md-6"></div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			<?php echo Form::submit('Submit', ['class' => 'btn btn-primary']); ?>

			<a href="<?php echo e(route('admin.emailtemplates.index')); ?>" class="btn btn-default">Cancel</a>
		</div>
	<?php echo Form::close(); ?>

</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>