<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo e(env('APP_NAME')); ?> | Log in</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link href="<?php echo e(URL::asset('assets/vendor/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(URL::asset('assets/vendor/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Theme style -->
    <link href="<?php echo e(URL::asset('assets/adminlte/css/AdminLTE.min.css')); ?>" rel="stylesheet" type="text/css">
	<!-- iCheck -->
    <link href="<?php echo e(URL::asset('assets/vendor/iCheck/square/blue.css')); ?>" rel="stylesheet" type="text/css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(URL::asset('assets/images/logo.png')); ?>"></a>
		</div>
		<!-- /.login-logo -->
		<div class="login-box-body">
			<p class="login-box-msg">Sign in to start your session</p>

			<?php echo Form::open(['route' => 'admin.login']); ?>

				<div class="form-group has-feedback">
				<?php echo Form::email('email', old('email'), [ 'class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required', 'autofocus' => 'autofocus' ]); ?>

					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<?php echo Form::password('password', [ 'class' => 'form-control', 'placeholder' => 'Password' ]); ?>

					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-8">
						<div class="checkbox icheck">
							<label>
								<input type="checkbox"> Remember Me
							</label>
						</div>
					</div>
					<!-- /.col -->
					<div class="col-xs-4">
						<?php echo Form::submit('Sign In', [ 'class' => 'btn btn-primary btn-block btn-flat' ]); ?>	
					</div>
					<!-- /.col -->
				</div>
			<?php echo Form::close(); ?>


	        <a href="<?php echo e(url('password/reset')); ?>">Forgot Your Password?</a>
		</div>
		<!-- /.login-box-body -->
	</div>
	<!-- /.login-box -->

    <script src="<?php echo e(URL::asset('assets/vendor/jquery/jquery-3.3.1.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('assets/vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('assets/vendor/iCheck/icheck.min.js')); ?>"></script>

	<script>
		$(function () {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' // optional
			});
		});
	</script>
</body>
</html>
