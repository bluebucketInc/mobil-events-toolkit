<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">

            <li class="header">MAIN NAVIGATION</li>

            <li class="<?php echo e(\Route::currentRouteName() == 'admin.dashboard.index' ? 'active' : ''); ?>">
                <a href="<?php echo e(route('admin.dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
            </li>
            
            <li class="treeview <?php echo e(str_is('admin.customers*', \Route::currentRouteName()) ? 'active menu-open' : ''); ?>">
                <a href="#">
                    <i class="fa fa-address-card"></i> <span>Customers</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo e(\Route::currentRouteName() == 'admin.customers.index' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('admin.customers.index')); ?>"><i class="fa fa-circle-o"></i> <span>Customer list</span></a>
                    </li>
                    <li class="<?php echo e(\Route::currentRouteName() == 'admin.user.create' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('admin.customers.create')); ?>"><i class="fa fa-circle-o"></i> <span>Add customer</span></a>
                    </li>
                </ul>
            </li>

            <li class="treeview <?php echo e(str_is('admin.assets*', \Route::currentRouteName()) ? 'active menu-open' : ''); ?>">
                <a href="#">
                    <i class="fa fa-files-o"></i> <span>Assets</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo e(\Route::currentRouteName() == 'admin.assets.browse' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('admin.assets.browse')); ?>"><i class="fa fa-circle-o"></i> <span>Asset list</span></a>
                    </li>
                    <li class="<?php echo e(\Route::currentRouteName() == 'admin.assets.create' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('admin.assets.create')); ?>"><i class="fa fa-circle-o"></i> <span>Add asset</span></a>
                    </li>
                    <li class="<?php echo e(\Route::currentRouteName() == 'admin.assetategories.index' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('admin.assetcategories.index')); ?>"><i class="fa fa-circle-o"></i> <span>Category list</span></a>
                    </li>
                </ul>
            </li>

            <li class="treeview <?php echo e(str_is('admin.events*', \Route::currentRouteName()) ? 'active menu-open' : ''); ?>">
                <a href="#">
                    <i class="fa fa-calendar"></i> <span>Events</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo e(\Route::currentRouteName() == 'admin.events.index' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('admin.events.index')); ?>"><i class="fa fa-circle-o"></i> <span>Event list</span></a>
                    </li>
                </ul>
            </li>

            <li class="treeview <?php echo e(str_is('admin.invitations*', \Route::currentRouteName()) ? 'active menu-open' : ''); ?>">
                <a href="#">
                    <i class="fa fa-send"></i> <span>Invitations</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo e(\Route::currentRouteName() == 'admin.invitations.index' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('admin.invitations.index')); ?>"><i class="fa fa-circle-o"></i> <span>Invitation list</span></a>
                    </li>
                </ul>            
            </li>

            <li class="header">SETTINGS</li>

            <li class="<?php echo e(str_is('admin.sectors*', \Route::currentRouteName()) ? 'active menu-open' : ''); ?>">
                <a href="<?php echo e(route('admin.sectors.index')); ?>"><i class="fa fa-building-o"></i> <span>Sectors</span></a>
            </li>

            <li class="<?php echo e(str_is('admin.emailTemplates*', \Route::currentRouteName()) ? 'active menu-open' : ''); ?>">
                <a href="<?php echo e(route('admin.emailTemplates.index')); ?>"><i class="fa fa-file-text-o"></i> <span>Email templates</span></a>
            </li>
            
            <li class="treeview <?php echo e(str_is('admin.admins*', \Route::currentRouteName()) ? 'active menu-open' : ''); ?>">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Admins</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo e(\Route::currentRouteName() == 'admin.admins.index' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('admin.admins.index')); ?>"><i class="fa fa-circle-o"></i> <span>Admin list</span></a>
                    </li>
                    <li class="<?php echo e(\Route::currentRouteName() == 'admin.admins.create' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('admin.admins.create')); ?>"><i class="fa fa-circle-o"></i> <span>Create admin</span></a>
                    </li>
                    <li class="<?php echo e(\Route::currentRouteName() == 'admin.admins.roles' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('admin.admins.roles')); ?>"><i class="fa fa-circle-o"></i> <span>Roles</span></a>
                    </li>
                    <li class="<?php echo e(\Route::currentRouteName() == 'admin.admins.logs' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('admin.admins.logs')); ?>"><i class="fa fa-circle-o"></i> <span>Logs</span></a>
                    </li>
                </ul>
            </li>

            <li class="treeview <?php echo e(str_is('admin.users*', \Route::currentRouteName()) ? 'active menu-open' : ''); ?>">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Users</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo e(\Route::currentRouteName() == 'admin.users.index' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('admin.users.index')); ?>"><i class="fa fa-circle-o"></i> <span>User list</span></a>
                    </li>
                    <li class="<?php echo e(\Route::currentRouteName() == 'admin.users.create' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('admin.users.create')); ?>"><i class="fa fa-circle-o"></i> <span>Create user</span></a>
                    </li>
                </ul>
            </li>

            <!--
            <li class="treeview <?php echo e(str_is('admin.values*', \Route::currentRouteName()) ? 'active menu-open' : ''); ?>">
                <a href="#">
                    <i class="fa fa-book"></i> <span>Values</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo e(\Route::currentRouteName() == 'admin.values.index' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('admin.values.index')); ?>"><i class="fa fa-circle-o"></i> <span>Value list</span></a>
                    </li>
                    <li class="<?php echo e(\Route::currentRouteName() == 'admin.values.create' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('admin.values.create')); ?>"><i class="fa fa-circle-o"></i> <span>Create value</span></a>
                    </li>
                </ul>
            </li>
            -->

        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>