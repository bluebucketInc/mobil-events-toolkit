<?php $__env->startSection('content'); ?>
<section class="padding section-500" style="background: url('<?php echo e(asset('assets/images/bg-01.jpg')); ?>') no-repeat center center;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-mobil no-margin">
                    <?php echo Form::open(['route' => 'register.submit']); ?>

                    <div class="panel-body">
                        <h4><b>Registration Form</b></h4>
                        <p>Simply fill in the details below to register.</p>
                        <br>
                        <div class="row">
                            <div class="form-group col-md-6 <?php echo e($errors->has('first_name') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('first_name'); ?>

                                <?php echo Form::text('first_name', old('first_name'), [ 'class' => 'form-control', 'placeholder' => 'Your first name', 'required' => 'required', 'autofocus' => 'autofocus' ]); ?>

                                <span class="help-block"><?php echo e($errors->first('first_name', '')); ?></span>
                            </div>
                            <div class="form-group col-md-6 <?php echo e($errors->has('last_name') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('last_name'); ?>

                                <?php echo Form::text('last_name', old('last_name'), [ 'class' => 'form-control', 'placeholder' => 'Your last name', 'required' => 'required' ]); ?>

                                <span class="help-block"><?php echo e($errors->first('last_name', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('company') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('company'); ?>

                                <?php echo Form::text('company', old('company'), [ 'class' => 'form-control', 'placeholder' => 'Your company name', 'required' => 'required', 'autofocus' => 'autofocus' ]); ?>

                                <span class="help-block"><?php echo e($errors->first('company', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="job_title">Job title</label>
                                <select class="form-control select2" data-placeholder="Choose sector" id="job_title" name="job_title">
                                    <?php $__currentLoopData = $jobTitles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jobTitle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($jobTitle->id); ?>"><?php echo e($jobTitle->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <span class="help-block"><?php echo e($errors->first('job_title', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('email'); ?>

                                <?php echo Form::email('email', old('email'), [ 'class' => 'form-control', 'placeholder' => 'Your email', 'required' => 'required' ]); ?>

                                <span class="help-block"><?php echo e($errors->first('email', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('password') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('password'); ?>

                                <?php echo Form::password('password', [ 'class' => 'form-control', 'placeholder' => 'Your password', 'required' => 'required' ]); ?>

                                <span class="help-block"><?php echo e($errors->first('password', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('password') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('password_confirmation'); ?>

                                <?php echo Form::password('password_confirmation', [ 'class' => 'form-control', 'placeholder' => 'Type your password once again', 'required' => 'required' ]); ?>

                            </div>
                        </div>
                        <?php echo Form::submit('Submit', ['class' => 'btn btn-primary btn-block']); ?>

                        <div class="row margin-top">
                            <div class="col-md-6">
                                Go back to <a href="<?php echo e(url('/')); ?>">login page</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/front/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>