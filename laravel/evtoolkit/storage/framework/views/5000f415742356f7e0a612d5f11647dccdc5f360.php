<?php $__env->startSection('page-title', 'Invitations'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active">Invitations</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="box">
	<div class="box-header">
		<h3 class="box-title">Invitation List</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="dataTables_wrapper form-inline dt-bootstrap">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped" id="table-invitationList">
				        <thead>
				            <tr>
								<th>Id</th>
								<th>Date</th>
								<th>Delivered</th>
								<th>Opens</th>
								<th>Clicks</th>
				                <th>Action</th>
				            </tr>
				        </thead>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.box-body -->
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-contact-delete-' + id).submit();
	}
}

$(function() {
    $('#table-invitationList').DataTable({
        processing: true,
        serverSide: true,
        ajax: '<?php echo route('admin.invitations.datatable'); ?>',
        columns: [
			{ data: 'id', name: 'id' },
			{ data: 'date', name: 'date' },
			{ data: 'delivered', name: 'delivered' },
			{ data: 'opens', name: 'opens' },
			{ data: 'clicks', name: 'clicks' },
            { 
            	data: 'null', 
            	name: 'action', 
            	orderable: false, 
            	searchable: false,
            	render: function(data, type, row) {
            		var html = '';
            		html += '<a href="<?php echo e(route('admin.invitations.show', ['id' => ':id'])); ?>" class="btn btn-xs btn-default btn-eye"><i class="fa fa-pencil"></i> View</a>';
            		return html.replace(/:id/g, row.id);
            	}
        	}
        ]
    });
});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>