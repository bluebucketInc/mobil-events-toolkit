<?php $__env->startSection('page-title', 'Events'); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Invitations</h3>
			</div>
			<div class="box-body">
				<table class="table">
					<tr>
						<th>Created date</th>
						<th>Sent date</th>
						<th>Sents</th>
						<th>Opens</th>
						<th>Clicks</th>
					<tr>
					<?php $__currentLoopData = $event->invitations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $invitation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr>
						<td><?php echo e(date('j M y h:i A', strtotime($invitation->created_at))); ?></td>
						<td><?php echo e($invitation->sent_at ? date('j M y h:i A', strtotime($invitation->sent_at)) : ''); ?></td>
						<td><?php echo e($invitation->stat_sents > 0 ? $invitation->stat_sents : ''); ?></td>
						<td><?php echo e($invitation->stat_opens > 0 ? $invitation->stat_opens : ''); ?></td>
						<td><?php echo e($invitation->stat_clicks > 0 ? $invitation->stat_clicks : ''); ?></td>
					</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Event Information</h3>
			</div>
			<div class="box-body">
				<table class="table table-tight table-noborder">
					<tr>
						<td>Name</td>
						<td>:</td>
						<td><?php echo e($event->name); ?></td>
					</tr>
					<tr>
						<td>Date</td>
						<td>:</td>
						<td><?php echo e($event->date); ?></td>
					</tr>
					<tr>
						<td>Time</td>
						<td>:</td>
						<td><?php echo e($event->time); ?></td>
					</tr>
					<tr>
						<td>Location</td>
						<td>:</td>
						<td><?php echo e($event->location); ?></td>
					</tr>
					<tr>
						<td>Sector</td>
						<td>:</td>
						<td><?php echo e($event->sector->name); ?></td>
					</tr>
				</table>
			</div> <!-- ./box-body -->
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>