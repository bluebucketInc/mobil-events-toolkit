<?php $__env->startSection('page-title', 'Contacts'); ?>

<?php $__env->startSection('content'); ?>

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Edit Contact</h3>
	</div>
	<?php echo Form::open(['method' => 'PUT', 'route' => ['admin.contacts.update', $contact->id], 'files' => true]); ?>

		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<?php echo Form::label('email'); ?>

						<?php echo Form::email('email', $contact->email, ['class' => 'form-control', 'maxlength' => '191']); ?>

					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<?php echo Form::label('first_name'); ?>

								<?php echo Form::text('first_name', $contact->first_name, ['class' => 'form-control', 'maxlength' => '50']); ?>

							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<?php echo Form::label('last_name'); ?>

								<?php echo Form::text('last_name', $contact->last_name, ['class' => 'form-control', 'maxlength' => '191']); ?>

							</div>
						</div>
					</div>				
				</div>
				<!-- /.col-md-6 -->
				<div class="col-md-6"></div>
			</div>
			
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			<?php echo Form::submit('Submit', ['class' => 'btn btn-primary']); ?>

			<a href="<?php echo e(route('admin.contacts.index')); ?>" class="btn btn-default">Cancel</a>
		</div>
	<?php echo Form::close(); ?>

</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>