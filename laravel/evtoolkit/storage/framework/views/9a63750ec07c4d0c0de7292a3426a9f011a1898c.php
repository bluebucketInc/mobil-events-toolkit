<?php $__env->startSection('page-title', 'Pages'); ?>

<?php $__env->startSection('content'); ?>

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Create New Page</h3>
	</div>
	<?php echo Form::open(['route' => 'admin.pages.store', 'files' => true]); ?>

		<div class="box-body">
			<div class="row">
				<div class="col-md-3 form-group">
					<?php echo Form::label('published'); ?>

					<?php echo Form::select('published', [ 0 => 'No', 1 => 'Yes' ], 0, ['class' => 'form-control']); ?>

				</div>
			</div>
			<div class="form-group">
				<?php echo Form::label('slug'); ?>

				<?php echo Form::text('slug', '', ['class' => 'form-control', 'maxlength' => '100']); ?>

			</div>
			<div class="form-group">
				<?php echo Form::label('title'); ?>

				<?php echo Form::text('title', '', ['class' => 'form-control', 'maxlength' => '50']); ?>

			</div>
			<div class="form-group">
				<?php echo Form::label('description'); ?>

				<?php echo Form::textarea('description', '', ['class' => 'form-control', 'maxlength' => '320']); ?>

			</div>
			<div class="form-group">
				<?php echo Form::label('content'); ?>

				<?php echo Form::textarea('content', '', ['class' => 'form-control tinymce']); ?>

			</div>
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			<?php echo Form::submit('Submit', ['class' => 'btn btn-primary']); ?>

			<a href="<?php echo e(route('admin.pages.index')); ?>" class="btn btn-default">Cancel</a>
		</div>
	<?php echo Form::close(); ?>

</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>