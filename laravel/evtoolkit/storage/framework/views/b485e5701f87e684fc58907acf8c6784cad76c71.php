<?php $__env->startSection('page-title', 'Users'); ?>

<?php $__env->startSection('content'); ?>

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Create New User</h3>
	</div>
	<?php echo Form::open(['route' => 'admin.users.store']); ?>

		<div class="box-body">
			<div class="form-group">
				<?php echo Form::label('name'); ?>

				<?php echo Form::text('name', '', ['class' => 'form-control', 'maxlength' => '50']); ?>

			</div>
			<div class="form-group">
				<?php echo Form::label('email'); ?>

				<?php echo Form::email('email', '', ['class' => 'form-control', 'maxlength' => '191']); ?>

			</div>
			<div class="form-group">
				<?php echo Form::label('password'); ?>

				<?php echo Form::password('password', ['class' => 'form-control', 'maxlength' => '191']); ?>

			</div>
			<div class="form-group">
				<?php echo Form::label('confirm_password'); ?>

				<?php echo Form::password('confirm_password', ['class' => 'form-control', 'maxlength' => '191']); ?>

			</div>
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			<?php echo Form::submit('Submit', ['class' => 'btn btn-primary']); ?>

			<a href="<?php echo e(route('admin.users.index')); ?>" class="btn btn-default">Cancel</a>
		</div>
	<?php echo Form::close(); ?>

</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>