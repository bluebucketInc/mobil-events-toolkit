<?php $__env->startSection('page-title', 'Customers'); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
	<div class="col-md-6">

		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Add New Customer</h3>
			</div> <!-- /.box-header -->
			<?php echo Form::open(['route' => 'admin.customers.store', 'files' => true]); ?>

			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<?php echo Form::label('first_name'); ?>

							<?php echo Form::text('first_name', '', ['class' => 'form-control', 'maxlength' => '50']); ?>

						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<?php echo Form::label('last_name'); ?>

							<?php echo Form::text('last_name', '', ['class' => 'form-control', 'maxlength' => '191']); ?>

						</div>
					</div>
				</div>
				<div class="form-group">
					<?php echo Form::label('email'); ?>

					<?php echo Form::email('email', '', ['class' => 'form-control', 'maxlength' => '191']); ?>

				</div>
				<div class="form-group">
					<?php echo Form::label('company'); ?>

					<?php echo Form::text('company', '', ['class' => 'form-control', 'maxlength' => '100']); ?>

				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="sector_id">Sector</label>
							<select class="form-control select2" data-placeholder="Choose sector" id="sector_id" name="sector_id">
								<option value="")></option>
								<option value="0")>General Manufacture</option>
								<option value="1")>Power</option>
								<option value="2")>Fleet</option>
							</select>
						</div>
						<div class="form-group">
							<?php echo Form::label('phone', 'Phone Number'); ?>

							<?php echo Form::text('phone', '', ['class' => 'form-control', 'maxlength' => '20']); ?>

						</div>
					</div>
				</div>
			</div> <!-- /.box-body -->

			<div class="box-footer">
				<?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

				<a href="<?php echo e(route('admin.customers.index')); ?>" class="btn btn-default">Cancel</a>
			</div>
			<?php echo Form::close(); ?>

		</div>
	</div> <!-- /.col-md-6 -->
	
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
	$('.select2').select2({allowClear: true});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>