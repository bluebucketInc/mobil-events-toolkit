<div id="modal-event-checklist" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                <h4 class="modal-title">Event Checklist</h4>
            </div>
            <div class="modal-body">
                <p>This section will contain:<br>
                    <ol>
                        <li>Booth mockup images</li>
                        <li>Event checklist, a list of things they need to do before the day of their event</li>
                    </ol>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->