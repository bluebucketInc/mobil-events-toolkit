(function(window,$){
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'}});
    window.LaravelDataTables = window.LaravelDataTables || {};
    <?php $__currentLoopData = $editors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $editor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        var <?php echo e($editor->instance); ?> = window.LaravelDataTables["%1$s-<?php echo e($editor->instance); ?>"] = new $.fn.dataTable.Editor({
        <?php if(is_array($editor->ajax)): ?>
            ajax: <?php echo json_encode($editor->ajax, 15, 512) ?>,
        <?php else: ?>
            ajax: "<?php echo e($editor->ajax); ?>",
        <?php endif; ?>
        table: "#<?php echo e($editor->table); ?>",
        <?php if($editor->template): ?>
            template: "<?php echo e($editor->template); ?>",
        <?php endif; ?>
        <?php if($editor->display): ?>
            display: "<?php echo e($editor->display); ?>",
        <?php endif; ?>
        <?php if($editor->idSrc): ?>
            idSrc: "<?php echo e($editor->idSrc); ?>",
        <?php endif; ?>
        fields: <?php echo json_encode($editor->fields, 15, 512) ?>,
        <?php if($editor->language): ?>
            i18n: <?php echo json_encode($editor->language, 15, 512) ?>
        <?php endif; ?>
        });
        <?php echo $editor->scripts; ?>

        <?php $__currentLoopData = (array) $editor->events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php echo e($editor->instance); ?>.on('<?php echo $event['event']; ?>', <?php echo $event['script']; ?>);
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    window.LaravelDataTables["%1$s"] = $("#%1$s").DataTable(%2$s);
})(window,jQuery);
