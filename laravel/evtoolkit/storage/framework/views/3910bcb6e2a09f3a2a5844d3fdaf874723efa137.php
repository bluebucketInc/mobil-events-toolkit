

<?php $__env->startSection('page-title', 'Assets'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="<?php echo e(route('admin.assets.browse')); ?>">Assets</a></li>
<li class="active">Add</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
	<div class="col-md-6">

		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Add New Asset</h3>
			</div> <!-- /.box-header -->
			<?php echo Form::open(['route' => 'admin.assets.store', 'files' => true]); ?>

			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="sector_id">Sector</label>
							<select class="form-control select2" data-placeholder="Choose sector" id="sector_id" name="sector_id">
								<option value=""></option>
								<?php $__currentLoopData = $sectors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sector): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($sector->id); ?>" <?php echo e($sector->id == $sector_id ? 'selected' : ''); ?>><?php echo e($sector->name); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="category_id">Category</label>
							<select class="form-control select2" data-placeholder="Choose category" id="category_id" name="category_id">
								<option value=""></option>
								<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($category->id); ?>" <?php echo e($category->id == $category_id ? 'selected' : ''); ?>><?php echo e($category->name); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<?php echo Form::label('name'); ?>

							<?php echo Form::text('name', '', ['class' => 'form-control', 'maxlength' => '50']); ?>

						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="form-group">
						<?php echo Form::label('asset', 'Asset file'); ?>

						<?php echo Form::file('asset', ['class' => 'form-control']); ?>

					</div>
				</div>
				<div class="form-group">
					<div class="form-group">
						<?php echo Form::label('preview', 'Image preview'); ?>

						<?php echo Form::file('preview', ['class' => 'form-control', 'accept' => 'image/x-png,image/gif,image/jpeg']); ?>

					</div>
				</div>
			</div> <!-- /.box-body -->

			<div class="box-footer">
				<?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

				<a href="<?php echo e(route('admin.assets.browse')); ?>" class="btn btn-default">Cancel</a>
			</div>
			<?php echo Form::close(); ?>

		</div>
	</div> <!-- /.col-md-6 -->
	
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
	$('.select2').select2({allowClear: true});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>