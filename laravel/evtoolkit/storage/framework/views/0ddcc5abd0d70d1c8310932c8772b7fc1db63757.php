<?php $__env->startSection('content'); ?>
<section class="padding" style="background: url('<?php echo e(asset('assets/images/bg-01.jpg')); ?>') no-repeat center center; min-height: 500px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-mobil no-margin">
                    <div class="panel-body">
                        <h4><b>Confirmed!</b></h4>
                        <p>Thank you for registering. Please look out for the notification email confirming account activation.</p>
                        <hr>
                        <div class="row margin-top">
                            <div class="col-md-6">
                                Go back to <a href="<?php echo e(url('/')); ?>">login page</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/front/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>