<?php $__env->startSection('page-title', 'Customers'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active">Customers</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
	<!--
	<div class="col-md-3">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Filter</h3>
			</div>
			<div class="box-body">
				<form role="form">
					<div class="form-group">
						<label>Distributor</label>
						<select id="distributor" class="form-control">
							<option value=""></option>
							<?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($user->id); ?>"><?php echo e($user->first_name . ' ' . $user->last_name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>
					<div class="form-group">
						<label>Sector</label>
					<select id="sector" class="form-control">
							<option value=""></option>
							<?php $__currentLoopData = $sectors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sector): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($sector->id); ?>"><?php echo e($sector->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>
				</form>
			</div>
		</div>
	</div>
	-->
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Customer List</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="dataTables_wrapper form-inline dt-bootstrap">
					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped" id="table-customerList">
								<thead>
									<tr>
										<th>Id</th>
										<th>Name</th>
										<th>Email</th>
										<th>Sector</th>
										<th>Distributor</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-customer-delete-' + id).submit();
	}
}

$(function() {
    $('#table-customerList').DataTable({
        processing: true,
        serverSide: true,
        ajax: '<?php echo route('admin.customers.datatable'); ?>',
        columns: [
			{ data: 'id', name: 'id' },
			{ data: 'first_name', name: 'first_name', orderable: true, searchable: true, render: function(data, type, row) {
					return row.first_name + ' ' + row.last_name;
				} 
			},
			{ data: 'email', name: 'email' },
			{ data: 'sector.name', name: 'sector' },
			{ data: 'null', name: 'distributor', render: function(data, type, row) {
					return row.user ? row.user.first_name : '';
				}
			},
            { 
            	data: 'null', 
            	name: 'action', 
            	orderable: false, 
            	searchable: false,
            	render: function(data, type, row) {
            		var html = '';
            		html += '<a href="<?php echo e(route('admin.customers.show', ['id' => ':id'])); ?>" class="btn btn-xs btn-default btn-flat"><i class="fa fa-eye"></i> View</a>';
            		return html.replace(/:id/g, row.id);
            	}
        	}
        ]
    });

	$('#distributor').select2({
		placeholder: 'All distributors',
		allowClear: true,
	});

	$('#sector').select2({
		placeholder: 'All sectors',
		allowClear: true,
	});

	$('#distributor').on('change', function(e) {
		console.log(this.value);
	});

	$('#sector').on('change', function(e) {
		console.log(this.value);
	});

});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>