    <?php echo $__env->make('modal.coming_soon', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
        <footer class="">
            <div id="footer-cap">
                <div class="container">
                    Energy lives here&trade;
                </div>
            </div>
            <div id="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="<?php echo e(asset('assets/images/logo-em-white.png')); ?>">
                        </div>
                        <ul class="col-md-6">
                            <ul class="list-inline text-right">
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Terms &amp; Conditions</a></li>
                            </ul>
                            <div class="text-right">&copy; Copyright 2013-2019 Exxon Mobil Corporation. All Rights Reserved</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container -->
        </footer>

    </div> <!-- /.body-wrapper -->

    <script src="<?php echo e(asset('assets/vendor/jquery/jquery-3.3.1.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/moment/min/moment.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/datatables/jquery.dataTables.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/datatables/dataTables.bootstrap.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/jquery-magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/vendor/iCheck/icheck.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/select2/select2.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script>
        $('.magnific-pdf').magnificPopup({
            type: 'iframe'
        });
    </script>

	<?php echo $__env->yieldPushContent('scripts'); ?>

</body>

</html>