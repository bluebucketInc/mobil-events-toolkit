<?php $__env->startSection('content'); ?>
<section class="padding section-500" style="background: url('<?php echo e(asset('assets/images/bg-01.jpg')); ?>') no-repeat center center;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-mobil no-margin">
                    <?php echo Form::open(['route' => 'password.update']); ?>

                    <div class="panel-body">
                        <?php if(session('status')): ?>
                            <div class="alert alert-success" role="alert">
                                <?php echo e(session('status')); ?>

                            </div>
                        <?php endif; ?>
                        <h4><b>Reset Password</b></h4>
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('email'); ?>

                                <?php echo Form::email('email', old('email'), [ 'class' => 'form-control', 'placeholder' => 'Your email', 'required' => 'required', 'autofocus' => 'autofocus' ]); ?>

                                <span class="help-block"><?php echo e($errors->first('email', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('password') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('password'); ?>

                                <?php echo Form::password('password', [ 'class' => 'form-control', 'placeholder' => 'Your new password', 'required' => 'required' ]); ?>

                                <span class="help-block"><?php echo e($errors->first('password', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('password') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('password_confirmation'); ?>

                                <?php echo Form::password('password_confirmation', [ 'class' => 'form-control', 'placeholder' => 'Type your password once again', 'required' => 'required' ]); ?>

                                <span class="help-block"><?php echo e($errors->first('password', '')); ?></span>
                            </div>
                        </div>
                        <?php echo Form::submit('Reset Password', ['class' => 'btn btn-primary btn-block']); ?>

                        <div class="row margin-top">
                            <div class="col-md-6">
                                Go back to <a href="<?php echo e(route('login')); ?>">login page</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/front/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>