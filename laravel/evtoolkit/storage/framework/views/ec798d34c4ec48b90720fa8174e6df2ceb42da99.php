<?php $__env->startSection('page-title', 'Email Templates'); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
	<div class="col-md-5">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Email Template</h3>
			</div>
			<?php echo Form::open(['method' => 'PUT', 'route' => ['admin.emailTemplates.update', $template->id], 'files' => true]); ?>

				<div class="box-body">
					<div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
						<?php echo Form::label('name'); ?>

						<?php echo Form::text('name', $template->name, ['class' => 'form-control', 'maxlength' => '50']); ?>

						<span class="help-block"><?php echo e($errors->first('name', '')); ?></span>
					</div>
					<div class="form-group <?php echo e($errors->has('image_preview_filename') ? 'has-error' : ''); ?>">
						<?php echo Form::label('image_preview_filename'); ?>

						<?php echo Form::file('image_preview_filename', ['class' => 'form-control', 'accept' => 'image/x-png,image/gif,image/jpeg']); ?>

						<span class="help-block"><?php echo e($errors->first('image_preview_filename', '')); ?></span>
						<img class="img-thumbnail" src="<?php echo e(asset('uploads/email_templates/' . $template->image_preview_filename)); ?>">
					</div>
				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<?php echo Form::submit('Submit', ['class' => 'btn btn-primary']); ?>

					<a href="<?php echo e(route('admin.emailTemplates.index')); ?>" class="btn btn-default">Cancel</a>
				</div>
			<?php echo Form::close(); ?>

		</div>
	</div>
	<div class="col-md-7">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">HTML Preview</h3>
			</div>
			<div class="edm-html-wrapper">
				<?php echo $template->html_content; ?>

			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>