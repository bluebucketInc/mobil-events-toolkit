<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Mobil Events Toolkit</title>

    <link href="<?php echo e(asset('fav.ico')); ?>" rel="shortcut icon" />

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo e(asset('assets/vendor/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/vendor/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/vendor/iCheck/square/blue.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/vendor/select2/select2.min.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(asset('assets/vendor/datatables/dataTables.bootstrap.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/css/main.css')); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="body-wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-mobil" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo e(route('home')); ?>"><img src="<?php echo e(asset('assets/images/logo.png')); ?>"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav nav-mobil">
                        <li>
                            <a href="#">Customers</a>
                        </li>
                        <li>
                            <a href="#">Events</a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('assets')); ?>">Assets</a>
                        </li>
                        <li>
                            <a href="#">Gallery</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
							<!-- Menu Toggle Button -->
							<a id="avatar" href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-user-circle fa-2x"></i>
								<!-- hidden-xs hides the username on small devices so only the image appears. -->
								<span class="hidden-xs"></span>
							</a>
							<ul class="dropdown-menu">
                                <li><a href="<?php echo e(route('user.profile')); ?>">Profile</a></li>
                                <li><a href="<?php echo e(route('user.change_password')); ?>">Change password</a></li>
                                <li role="separator" class="divider"></li>
                                <li>
	                                <a class="dropdown-item" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
	                                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
	                                    <?php echo csrf_field(); ?>
	                                </form>
                                </li>
							</ul>
                        </li>
                    </ul>
						

                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>
