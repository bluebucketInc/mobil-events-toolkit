<?php $__env->startSection('content'); ?>
<section class="padding section-500" style="background: url('<?php echo e(asset('assets/images/bg-01.jpg')); ?>') no-repeat center center;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-mobil no-margin">
                    <?php echo Form::open(['route' => 'auth.login']); ?>

                    <div class="panel-body">
                        <h1>Mobil Events Toolkit</h1>
                        <p><i>Set the stage for better business</i></p>
                        <br>
                        <p>From assets to CRM tools, the Mobil Events Toolkit is designed to help you create the perfect event, seamlessly.</p>
                        <p>Please sign in to get started.</p>
                        <p>Don't have an account? <a href="<?php echo e(route('register.form')); ?>">Register here</a>.</p>
                        <br>
                        </p>
                        <div class="form-group">
                            <?php echo Form::label('email'); ?>

                            <?php echo Form::email('email', old('email'), [ 'class' => 'form-control', 'placeholder' => 'Your email', 'required' => 'required', 'autofocus' => 'autofocus' ]); ?>

                        </div>
                        <div class="form-group">
                            <?php echo Form::label('password'); ?>

                            <?php echo Form::password('password', [ 'class' => 'form-control', 'placeholder' => 'Your password', 'required' => 'required' ]); ?>

                        </div>
                        <?php echo Form::submit('Submit', ['class' => 'btn btn-primary btn-block']); ?>

                        <div class="row margin-top">
                            <div class="col-md-6">
                                <a href="#">Forgot password?</a>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="#">Legal notice</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/front/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>