<?php $__env->startSection('page-title', 'Assets'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active">Assets</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="box">
	<div class="box-body">
		<div class="btn-group">
			<a href="<?php echo e(route('admin.assets.create', ['sector_id' => $sector_id, 'category_id' => $category_id])); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add new asset</a>
		</div>

		<div class="btn-group pull-right">
			<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Category (<?php echo e($category_id ? $categories[$category_id - 1]->name : 'All'); ?>) <span class="caret"></span></button>
			<ul class="dropdown-menu">
				<?php $index = 0; ?>
				<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<li><a href="<?php echo e(route('admin.assets.browse', ['sector_id' => $sector_id, 'category_id' => $category->id ])); ?>"><?php echo e($category->name); ?>: <?php echo e($category->assets->count()); ?></a></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<li class="divider"></li>
				<li><a href="<?php echo e(route('admin.assets.browse', ['sector_id' => $sector_id, 'category_id' => null ])); ?>">No filter</a></li>
			</ul>
		</div>

		<div class="btn-group pull-right" style="margin-right: 10px;">
			<?php $__currentLoopData = $sectors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sector): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<a href="<?php echo e(route('admin.assets.browse', ['sector_id' => $sector->id, 'category_id' => $category_id ])); ?>" class="btn btn-sm <?php echo e($sector->id == $sector_id ? 'btn-info' : 'btn-default'); ?>"><?php echo e($sector->name); ?>: <?php echo e($sector->assets->count()); ?></a>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>

	</div>
</div>

<div class="row asset-grid">
	<?php $__currentLoopData = $assets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $asset): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<div class="col-md-3 col-sm-6">
			<div class="box asset-grid-item">
				<div class="box-head">
					<?php if($asset->format == 'image'): ?>
					<a href="<?php echo e(url('uploads/event_assets/' . $asset->filename)); ?>" class="magnific-popup">
						<img src="<?php echo e(asset('uploads/event_assets/400x200-' . $asset->preview_filename)); ?>" class="img-responsive">
					</a>
					<?php elseif($asset->format == 'video'): ?>
					<a href="#" data-source="<?php echo e($asset->filename); ?>" class="preview-video">
						<img src="<?php echo e(asset('uploads/event_assets/400x200-' . $asset->preview_filename)); ?>" class="img-responsive">
					</a>
					<?php elseif($asset->format == 'iframe'): ?>
					<a href="<?php echo e(url('uploads/event_assets/' . $asset->filename)); ?>" class="magnific-iframe">
						<img src="<?php echo e(asset('uploads/event_assets/400x200-' . $asset->preview_filename)); ?>" class="img-responsive">
					</a>
					<?php endif; ?>
				</div>
				<div class="box-footer">
					<h5><?php echo e($asset->name); ?></h5>
					<div class="btn-group pull-right">
						<a href="<?php echo e(route('admin.assets.edit', ['id' => $asset->id ])); ?>" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i> Edit</a>
						<a href="#" class="btn btn-danger btn-xs" onclick="event.preventDefault(); confirmDelete(<?php echo e($asset->id); ?>)"><i class="fa fa-trash"></i> Delete</a>
						<?php echo Form::open( [ 'method' => 'DELETE', 'route' => [ 'admin.assets.destroy', $asset->id ], 'id' => 'form-asset-delete-' . $asset->id ] ); ?>

						<?php echo Form::close(); ?>

					</div>
				</div>
			</div>
		</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('modal/video_content', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->startPush('styles'); ?>
<style>
a.magnific-popup {
	cursor: zoom-in;
}
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script>

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-asset-delete-' + id).submit();
	}
}

$(function() {
	$('input[type="checkbox"].minimal').iCheck({
		checkboxClass: 'icheckbox_minimal-blue'
	});

})

$('.magnific-popup').magnificPopup({type:'image'});
$('.preview-video').click(function(e) {
	e.preventDefault();
	var source = "<?php echo e(asset('uploads/event_assets')); ?>/" + $(this).data('source');
	$('#modal-video-content').modal();
	$('#video-container').get(0).pause();
    $('#video-source').attr('src', source);
    $('#video-container').get(0).load();
});
$('.magnific-popup-player').magnificPopup({
	items: {
		//src: '<video class="white-popup" src="http://localhost/extoolkit/public/uploads/event_assets/KyxFUW7amq953NFxkAgW.mp4"></video>',
		src: '<video id="video-container" controls><source id="video-source" src="http://localhost/extoolkit/public/uploads/event_assets/KyxFUW7amq953NFxkAgW.mp4" type="video/mp4"> Your browser does not support the video tag.</video>',
		type: 'inline'
	}
});
$('.magnific-iframe').magnificPopup({
	type: 'iframe'
});
$('.magnific-video').magnificPopup({
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: false,
    iframe: {
        markup: '<div class="mfp-iframe-scaler">'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
              '</div>',

        srcAction: 'iframe_src',
        }
});


</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>