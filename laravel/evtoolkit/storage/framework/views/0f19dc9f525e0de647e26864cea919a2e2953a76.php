<?php $__env->startSection('page-title', 'Assets'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="<?php echo e(route('admin.assets.index')); ?>">Assets</a></li>
<li class="active">Browser</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-3">
		<a href="<?php echo e(route('admin.assets.create')); ?>" class="btn btn-primary btn-block margin-bottom"><i class="fa fa-plus"></i> Add Asset</a>
	</div>
</div>
<div class="row">

	<!-- navigation -->
	<div class="col-md-3">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title"><?php echo e($sector->name); ?></h3>
			</div> <!-- /.box-header -->
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li class="<?php echo e($category->id == $category_id ? 'active' : ''); ?>">
							<a href="<?php echo e(route('admin.assets.browser', [ 'sector_id' => $sector->id, 'category_id' => $category->id ])); ?>"><small class="label" style="background-color: <?php echo e($category->label_color); ?>"><?php echo e($category->name); ?></small><span class="pull-right"><?php echo e($category->assets->count()); ?></span></a>
						</li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>
			</div> <!-- /.box-body -->
		</div> <!-- /.box -->
	</div> <!-- /.col -->

	<!-- assets -->
	<div class="col-md-9">
		<div class="row">
			<?php $__currentLoopData = $assets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $asset): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="col-md-3">
					<div class="box">
						<div class="box-body">
							<img src="<?php echo e(asset('uploads/event_assets/300x150-' . $asset->preview_filename)); ?>" class="img-responsive">
							<h5><?php echo e($asset->name); ?></h5>
						</div>
					</div> <!-- /.box -->
				</div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div> <!-- /.row -->
	</div>

</div> <!-- /.row -->
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>


</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>