@extends('layouts/main')

@section('content')
<section style="background: url({{ asset('assets/images/bg-03.jpg') }}) no-repeat center center; background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-mobil">
                    {!! Form::open(['route' => ['leadform.submit', $user->code]]) !!}
                    <div class="panel-heading">
                        <h3 class="panel-title"><b>Customer Contact Form</b></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group col-md-6 {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                {!! Form::label('first_name') !!}
                                {!! Form::text('first_name', old('first_name'), ['class' => 'form-control', 'maxlength' => '191']) !!}
                                <span class="help-block">{{ $errors->first('first_name', '') }}</span>
                            </div>
                            <div class="form-group col-md-6">
                                {!! Form::label('last_name') !!}
                                {!! Form::text('last_name', old('last_name'), ['class' => 'form-control', 'maxlength' => '191']) !!}
                                <span class="help-block">{{ $errors->first('first_name', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                {!! Form::label('email') !!}
                                {!! Form::text('email', old('email'), ['class' => 'form-control', 'maxlength' => '191']) !!}
                                <span class="help-block">{{ $errors->first('first_name', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                {!! Form::label('company') !!}
                                {!! Form::text('company', old('company'), ['class' => 'form-control', 'maxlength' => '191']) !!}
                                <span class="help-block">{{ $errors->first('first_name', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="sector_id">Sector</label>
                                <select class="form-control select2" data-placeholder="Choose sector" id="sector_id" name="sector_id">
                                    <option></option>
                                    @foreach ($sectors as $sector)
                                    <option value="{{ $sector->id }}" {{ old('sector_id') == $sector->id ? 'selected' : '' }}>{{ $sector->name }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block">{{ $errors->first('first_name', '') }}</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="job_title_id">Job title</label>
                                <select class="form-control select2" data-placeholder="Choose job title" id="job_title_id" name="job_title_id">
                                    <option></option>
                                    @foreach ($jobTitles as $jobTitle)
                                    <option value="{{ $jobTitle->id }}" {{ old('job_title_id') == $jobTitle->id ? 'selected' : '' }}>{{ $jobTitle->name }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block">{{ $errors->first('first_name', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <div class="checkbox">
                                    <label><input name="check_permission" type="checkbox"> I have read and agreed with the <a href="#" data-toggle="modal" data-target="#modal-customer-consent">Terms and Conditions</a></label>
                                </div>
                            </div>
                        </div>
                    </div> <!-- ./panel-body -->
                    
                    <div class="panel-footer with-border">
                        <div class="pull-right">
			                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-6"></div>
        </div>
    </div>
</section>
@endsection

@include('modal.customer_consent')