<div id="modal-test-email" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                <h4 class="modal-title">Send Test Email</h4>
            </div>
            {!! Form::open(['route' => ['invitations.test', $invitation->id]]) !!}
            <div class="modal-body">
                <div class="form-group {{ $errors->has('emails') ? 'has-error' : '' }}">
                    {!! Form::label('emails') !!}
                    {!! Form::text('emails', old('emails'), ['class' => 'form-control', 'maxlength' => '191']) !!}
                    <span class="help-block">{{ $errors->first('emails', '') }}</span>
                </div>
            </div>
            <!-- /.modal-body -->
    
            <div class="modal-footer">
                {!! Form::submit('Send', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->