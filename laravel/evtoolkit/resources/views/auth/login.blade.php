@extends('layouts/front/main')

@section('content')
<section class="padding section-500" style="background: url('{{ asset('assets/images/bg-01.jpg') }}') no-repeat center center;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-mobil no-margin">
                    {!! Form::open(['route' => 'login']) !!}
                    <div class="panel-body">
                        <h1>Mobil Events Toolkit</h1>
                        <p><i>Set the stage for better business</i></p>
                        <br>
                        <p>From assets to CRM tools, the Mobil Events Toolkit is designed to help you create the perfect event, seamlessly.</p>
                        <p>Please sign in to get started.</p>
                        <p>Don't have an account? <a href="{{ route('register') }}">Register here</a>.</p>
                        <p>For Terms &amp; Conditions and Privacy Policy, click <a href="#" data-toggle="modal" data-target="#modal-register-consent">here</a></p>
                        <br>
                        </p>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            {!! Form::label('email') !!}
                            {!! Form::email('email', old('email'), [ 'class' => 'form-control', 'placeholder' => 'Your email', 'required' => 'required', 'autofocus' => 'autofocus' ]) !!}
                            <span class="help-block">{{ $errors->first('email', '') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                            {!! Form::label('password') !!}
                            {!! Form::password('password', [ 'class' => 'form-control', 'placeholder' => 'Your password', 'required' => 'required' ]) !!}
                            <span class="help-block">{{ $errors->first('password', '') }}</span>
                        </div>
                        {!! Form::submit('Submit', ['class' => 'btn btn-primary btn-block']) !!}
                        <div class="row margin-top">
                            <div class="col-md-6">
                                <a href="{{ route('password.request') }}">Forgot password?</a>
                            </div>
                            <div class="col-md-6 text-right">
                                <!--<a href="#">Legal notice</a>-->
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@include('modal.register_consent')
@endsection