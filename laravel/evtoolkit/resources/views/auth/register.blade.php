@extends('layouts/front/main')

@section('content')
<section class="padding section-500" style="background: url('{{ asset('assets/images/bg-01.jpg') }}') no-repeat center center;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-mobil no-margin">
                    {!! Form::open(['route' => 'register']) !!}
                    <div class="panel-body">
                        <h4><b>Registration Form</b></h4>
                        <p>Simply fill in the details below to register.</p>
                        <p>For Terms &amp; Conditions and Privacy Policy, click <a href="#" data-toggle="modal" data-target="#modal-register-consent">here</a></p>
                        <br>
                        <div class="row">
                            <div class="form-group col-md-6 {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                {!! Form::label('first_name') !!}
                                {!! Form::text('first_name', old('first_name'), [ 'class' => 'form-control', 'placeholder' => 'Your first name', 'required' => 'required', 'autofocus' => 'autofocus' ]) !!}
                                <span class="help-block">{{ $errors->first('first_name', '') }}</span>
                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                {!! Form::label('last_name') !!}
                                {!! Form::text('last_name', old('last_name'), [ 'class' => 'form-control', 'placeholder' => 'Your last name', 'required' => 'required' ]) !!}
                                <span class="help-block">{{ $errors->first('last_name', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 {{ $errors->has('company') ? 'has-error' : '' }}">
                                {!! Form::label('company') !!}
                                {!! Form::text('company', old('company'), [ 'class' => 'form-control', 'placeholder' => 'Your company name', 'required' => 'required', 'autofocus' => 'autofocus' ]) !!}
                                <span class="help-block">{{ $errors->first('company', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="job_title">Region</label>
                                <select class="form-control select2" data-placeholder="Choose region" id="region" name="region">
                                    @foreach ($regions as $region)
                                    <option value="{{ $region->id }}">{{ $region->name }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block">{{ $errors->first('job_title', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="job_title">Job title</label>
                                <select class="form-control select2" data-placeholder="Choose sector" id="job_title" name="job_title">
                                    @foreach ($jobTitles as $jobTitle)
                                    <option value="{{ $jobTitle->id }}">{{ $jobTitle->name }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block">{{ $errors->first('job_title', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 {{ $errors->has('email') ? 'has-error' : '' }}">
                                {!! Form::label('email') !!}
                                {!! Form::email('email', old('email'), [ 'class' => 'form-control', 'placeholder' => 'Your email', 'required' => 'required' ]) !!}
                                <span class="help-block">{{ $errors->first('email', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 {{ $errors->has('password') ? 'has-error' : '' }}">
                                {!! Form::label('password') !!}
                                {!! Form::password('password', [ 'class' => 'form-control', 'placeholder' => 'Your password', 'required' => 'required' ]) !!}
                                <span class="help-block">{{ $errors->first('password', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 {{ $errors->has('password') ? 'has-error' : '' }}">
                                {!! Form::label('password_confirmation') !!}
                                {!! Form::password('password_confirmation', [ 'class' => 'form-control', 'placeholder' => 'Type your password once again', 'required' => 'required' ]) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 {{ $errors->has('agreed') ? 'has-error' : '' }}">
                                <div class="checkbox">
                                    <label><input name="agreed" type="checkbox"> I have read and agreed with the <a href="#" data-toggle="modal" data-target="#modal-register-consent">Terms and Conditions</a></label>
                                </div>
                                <span class="help-block">{{ $errors->first('agreed', '') }}</span>
                            </div>
                        </div>
                        {!! Form::submit('Submit', ['class' => 'btn btn-primary btn-block']) !!}
                        <div class="row margin-top">
                            <div class="col-md-6">
                                Go back to <a href="{{ route('login') }}">login page</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@include('modal.register_consent')
@endsection