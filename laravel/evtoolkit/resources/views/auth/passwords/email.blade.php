@extends('layouts/front/main')

@section('content')
<section class="padding section-500" style="background: url('{{ asset('assets/images/bg-01.jpg') }}') no-repeat center center;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-mobil no-margin">
                    {!! Form::open(['route' => 'password.email']) !!}
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <h4><b>Reset Password</b></h4>
                        <p>Type in your email below</p>
                        <br>
                        <div class="row">
                            <div class="form-group col-md-12 {{ $errors->has('email') ? 'has-error' : '' }}">
                                {!! Form::label('email') !!}
                                {!! Form::email('email', old('email'), [ 'class' => 'form-control', 'placeholder' => 'Your email', 'required' => 'required', 'autofocus' => 'autofocus' ]) !!}
                                <span class="help-block">{{ $errors->first('email', '') }}</span>
                            </div>
                        </div>
                        {!! Form::submit('Send password reset link', ['class' => 'btn btn-primary btn-block']) !!}
                        <div class="row margin-top">
                            <div class="col-md-6">
                                Go back to <a href="{{ route('login') }}">login page</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection