@extends('layouts/main')

@section('content')
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">Events</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="panel panel-mobil panel-filter">
                    <div class="panel-heading with-border">
                        <h3 class="panel-title">Filter</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="from_date">From date</label>
                            <div class='input-group date' id='datepicker1'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="to_date">To date</label>
                            <div class='input-group date' id='datepicker2'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sector_id">Sector</label>
                            <select class="form-control select2" data-placeholder="Choose sector" id="sector_id">
                                <option value="a" {{ $fs == 'a' ? 'selected' : '' }}>All</option>
                                @foreach ($sectors as $sector)
                                <option value="{{ $sector->id }}" {{ $fs == $sector->id ? 'selected' : '' }}>{{ $sector->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <a id="filter" href="#" class="btn btn-secondary btn-sm pull-right">Apply</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="panel panel-mobil">
                    <div class="panel-heading with-border">
                        <h3 class="panel-title">Event List</h3>
                        <div class="panel-tools pull-right">
                            <a href="{{ route('events.create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add new event</a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped" id="table-events">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Sector</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-event-delete-' + id).submit();
	}
}

$(function() {
    $('#table-events').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('events.datatable', ['fd1' => $fd1, 'fd2' => $fd2, 'fs' => $fs, 'fst' => $fst]) !!}',
        columns: [
			{ data: 'name', name: 'name' },
			{ data: 'date', name: 'date' },
			{ data: 'status', name: 'status' },
			{ data: 'sector', name: 'sector' },
            { 
            	data: 'null', 
            	name: 'action', 
            	orderable: false, 
            	searchable: false,
            	render: function(data, type, row) {
            		var html = '';
            		html += '<a href="{{ route('events.show', ['id' => ':id']) }}" class="btn btn-xs btn-default btn-flat"><i class="fa fa-eye"></i> View</a>';
	                html += '<a href="#" class="btn btn-danger btn-flat btn-xs" onclick="event.preventDefault(); confirmDelete(:id)"><i class="fa fa-trash"></i> Delete</a>';
	                html += '{!! Form::open( [ 'method' => 'DELETE', 'route' => [ 'events.destroy', ':id' ], 'id' => 'form-event-delete-:id' ] ) !!}';
	                html += '{!! Form::close() !!}';
            		return html.replace(/:id/g, row.id);
            	}
        	}
        ]
    });

    $('#filter').click(function(e) {
        e.preventDefault();
        var fd1 = $('#datepicker1').find("input").val();
        var fd2 = $('#datepicker2').find("input").val();
        var fs = $('#sector_id').val();
        url = "{!! route('events', ['fd1' => '_fd1_', 'fd2' => '_fd2_', 'fs' => '_fs_']) !!}";
        url = url.replace('_fd1_', fd1);
        url = url.replace('_fd2_', fd2);
        url = url.replace('_fs_', fs);
        //console.log(url);
        window.location.href = url;
    });

    $('#datepicker1').datetimepicker({
        format: 'YYYY-MM-DD',
        date: '{{ $fd1 }}',
        showClear: true,
    });
    $('#datepicker2').datetimepicker({
        format: 'YYYY-MM-DD',
        date: '{{ $fd2 }}',
        showClear: true,
    });
});
</script>
@endpush