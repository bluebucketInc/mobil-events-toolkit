@extends('layouts/main')

@section('content')
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li><a href="{{ route('customers') }}">Customers</a></li>
            <li class="active">Add new customer</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-mobil">
                    {!! Form::open(['route' => 'customers.store']) !!}
                    <div class="panel-heading">
                        <h3 class="panel-title"><b>Customer Contact Form</b></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group col-md-6 {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                {!! Form::label('first_name') !!}
                                {!! Form::text('first_name', old('first_name'), ['class' => 'form-control', 'maxlength' => '191']) !!}
                                <span class="help-block">{{ $errors->first('first_name', '') }}</span>
                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('last_name]') ? 'has-error' : '' }}">
                                {!! Form::label('last_name') !!}
                                {!! Form::text('last_name', old('last_name'), ['class' => 'form-control', 'maxlength' => '191']) !!}
                                <span class="help-block">{{ $errors->first('last_name', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 {{ $errors->has('email') ? 'has-error' : '' }}">
                                {!! Form::label('email') !!}
                                {!! Form::text('email', old('email'), ['class' => 'form-control', 'maxlength' => '191']) !!}
                                <span class="help-block">{{ $errors->first('email', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 {{ $errors->has('company') ? 'has-error' : '' }}">
                                {!! Form::label('company') !!}
                                {!! Form::text('company', old('company'), ['class' => 'form-control', 'maxlength' => '191']) !!}
                                <span class="help-block">{{ $errors->first('company', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 {{ $errors->has('sector_id') ? 'has-error' : '' }}">
                                <label for="sector_id">Sector</label>
                                <select class="form-control select2" data-placeholder="Choose sector" id="sector_id" name="sector_id">
                                    <option></option>
                                    @foreach ($sectors as $sector)
                                    <option value="{{ $sector->id }}" {{ old('sector_id') == $sector->id ? ' selected' : '' }}>{{ $sector->name }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block">{{ $errors->first('sector_id', '') }}</span>
                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('job_title_id') ? 'has-error' : '' }}">
                                <label for="job_title_id">Job title</label>
                                <select class="form-control select2" data-placeholder="Choose job title" id="job_title_id" name="job_title_id">
                                    <option></option>
                                    @foreach ($jobTitles as $jobTitle)
                                    <option value="{{ $jobTitle->id }}" {{ old('job_title_id') == $jobTitle->id ? ' selected' : '' }}>{{ $jobTitle->name }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block">{{ $errors->first('job_title_id', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 {{ $errors->has('check_permission') ? 'has-error' : '' }}">
                                <div class="checkbox">
                                    <label><input name="check_permission" type="checkbox"> This person agrees to receive marketing emails from ExxonMobil from time to time.</label>
                                    <a href="#" data-toggle="modal" data-target="#modal-customer-consent">What's this?</a>
                                    <span class="help-block">{{ $errors->first('check_permission', '') }}</span>
                                </div>
                            </div>
                        </div>
                    </div> <!-- ./panel-body -->
                    
                    <div class="panel-footer with-border">
                        <div class="pull-right">
			                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
@endsection

@include('modal.customer_consent')

@push('scripts')
<script>
$('.select2').select2();
</script>
@endpush