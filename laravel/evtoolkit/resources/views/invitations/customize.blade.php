@extends('layouts/main')

@section('content')
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li><a href="{{ route('events') }}">Events</a></li>
            <li><a href="{{ route('events.show', $invitation->event->id) }}">{{ $invitation->event->name }}</a></li>
            <li class="active">Invitation - Customize</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-mobil">
                    <div class="panel-heading">
                        <h3 class="panel-title">Customize your eDMs</h3>
                    </div>
                    <div class="panel-body">
                        <p>If you wish to create custom content, place it in the field below</p>
                        <div class="form-group">
                            {!! Form::textarea('text_block_1', $invitation->text_block_1, ['id' => 'text_block_1', 'rows' => 4, 'class' => 'form-control', 'placeholder' => '_TEXT_BLOCK_1_']) !!}
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="pull-right">
                            <a href="#" id="save" class="btn btn-primary">Save</a>
                            <a href="#" id="next" class="btn btn-secondary">Save &amp; Next</a>
                        </div>
                        <div clas="pull-left">
                            <a href="{{ route('invitations.template', $invitation->id) }}" class="btn btn-default">Back</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="panel panel-mobil">
                    <div class="edm-html-wrapper">
                        {!! $invitation->html_content !!}
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /.container -->
</section>
@endsection

@push('scripts')
<script>


    $('#save').click(function(e) {
        e.preventDefault();

        var saveUrl = "{{ route('ajax.invitations.save', $invitation->id) }}";
        var saveData = {
            text_block_1: $('#text_block_1').val()
        };

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            type: 'POST',
            url: saveUrl,
            data: saveData,
            datatype: 'json',
            success: function (data) { 
                window.location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    });
    
     $('#next').click(function(e) {
        e.preventDefault();

        var saveUrl = "{{ route('ajax.invitations.save', $invitation->id) }}";
        var saveData = {
            text_block_1: $('#text_block_1').val()
        };

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            type: 'POST',
            url: saveUrl,
            data: saveData,
            datatype: 'json',
            success: function (data) { 
                window.location = "{{ route('invitations.confirmation', $invitation->id) }}";
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    });
       
</script>
@endpush