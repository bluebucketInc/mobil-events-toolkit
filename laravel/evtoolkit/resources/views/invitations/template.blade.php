@extends('layouts/main')

@section('content')
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li><a href="{{ route('events') }}">Events</a></li>
            <li><a href="{{ route('events.show', $invitation->event->id) }}">{{ $invitation->event->name }}</a></li>
            <li class="active">Invitation - Choose template</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="panel panel-mobil">
            <div class="panel-body">
                <div class="row">
                    @foreach ($templates as $template)
                        <div class="col-md-2">
                            <div class="box">
                                <div class="box-head">
                                    <a href="#" data-id="{{ $template->id }}" class="template template-popup {{ $invitation->email_template_id == $template->id ? 'selected' : '' }}">
                                        <img src="{{ asset('uploads/email_templates/t-' . $template->image_preview_filename) }}" class="img-responsive">
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="panel-footer">
                <div class="pull-right">
                    <a href="#" id="next" class="btn btn-primary">Next</a>
                </div>
                <div clas="pull-left">
                    <a href="{{ route('events.show', $invitation->event_id) }}" class="btn btn-default">Back</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div> <!-- /.container -->
</section>
@endsection

@push('scripts')
<script>

$('.template').click(function(e) {
    e.preventDefault();
    $('.template').removeClass('selected');
    $(this).addClass('selected');
});

$('#next').click(function(e) {
    e.preventDefault();
    var templateId = $('.template.selected').data('id');
    var url = "{{ route('invitations.choose_template', ['id' => '_id_', 'template_id' => '_template_id_']) }}";
    console.log(url);
    url = url.replace('_id_', {{ $invitation->id }});
    url = url.replace('_template_id_', templateId);
    window.location = url;
});

</script>
@endpush