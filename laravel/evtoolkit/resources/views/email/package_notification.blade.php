<p>Hi,</p>
<p>Your Event Toolkit Package link is ready: <a href="{{ $link }}" download>{{ $link }}</a></p>
<p>You could share this link to others.</p>
<p>This link will be expired in 7 days.</p>