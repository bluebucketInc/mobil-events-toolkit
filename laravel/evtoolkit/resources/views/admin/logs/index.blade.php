@extends('admin.layouts.main')

@section('page-title', 'Logs')

@section('breadcrumb')
<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active">Logs</li>
@endsection
@section('content')

<div class="box">
	<div class="box-header">
		<h3 class="box-title">Logs</h3>
        <div class="box-tools">
        </div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="dataTables_wrapper form-inline dt-bootstrap">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped" id="table-pageList">
				        <thead>
				            <tr>
				                <th>Id</th>
				                <th>Time</th>
				                <th>Description</th>
				            </tr>
				        </thead>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.box-body -->
</div>

@endsection

@push('scripts')
<script>

$(function() {
    $('#table-pageList').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.logs.datatable') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'created_at', name: 'created_at' },
            { data: 'description', name: 'description' },
        ]
    });
});
</script>
@endpush