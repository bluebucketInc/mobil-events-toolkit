@extends('admin.layouts.main')

@section('page-title', 'Admins')

@section('breadcrumb')
<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active">Admins</li>
@endsection

@section('content')
<div class="box">
	<div class="box-header">
		<h3 class="box-title">Admin List</h3>
        <div class="box-tools">
            <div class="btn-group btn-group-sm">
                <a href="{{ route('admin.admins.create') }}" class="btn btn-primary btn-flat">Create Admin</a>
            </div>
        </div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="dataTables_wrapper form-inline dt-bootstrap">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped" id="table-adminList">
				        <thead>
				            <tr>
								<th>Id</th>
								<th>Name</th>
								<th>Email</th>
								<th>Role</th>
				                <th>Action</th>
				            </tr>
				        </thead>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.box-body -->
</div>
@endsection

@push('scripts')
<script>

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-admin-delete-' + id).submit();
	}
}

$(function() {
    $('#table-adminList').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.admins.datatable') !!}',
        columns: [
			{ data: 'id', name: 'id' },
			{ data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
			{ data: 'role', name: 'role' },	
            { 
            	data: 'null', 
            	name: 'action', 
            	orderable: false, 
            	searchable: false,
            	render: function(data, type, row) {
            		var html = '';
            		html += '<a href="{{ route('admin.admins.edit', ['id' => ':id']) }}" class="btn btn-xs btn-default btn-flat"><i class="fa fa-pencil"></i> Edit</a>';
	                html += '<a href="#" class="btn btn-danger btn-flat btn-xs" onclick="event.preventDefault(); confirmDelete(:id)"><i class="fa fa-trash"></i> Delete</a>';
	                html += '{!! Form::open( [ 'method' => 'DELETE', 'route' => [ 'admin.admins.destroy', ':id' ], 'id' => 'form-admin-delete-:id' ] ) !!}';
	                html += '{!! Form::close() !!}';
            		return html.replace(/:id/g, row.id);
            	}
        	}
        ]
    });
});
</script>
@endpush