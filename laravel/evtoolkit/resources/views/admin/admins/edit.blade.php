@extends('admin.layouts.main')

@section('page-title', 'Users')

@section('content')

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Edit Admin</h3>
	</div>
	{!! Form::open(['method' => 'PUT', 'route' => ['admin.admins.update', $admin->id]]) !!}
		<div class="box-body">
			<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
				{!! Form::label('name') !!}
				{!! Form::text('name', $admin->name, ['class' => 'form-control', 'maxlength' => '50']) !!}
				<span class="help-block">{{ $errors->first('name', '') }}</span>
			</div>
			<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
				{!! Form::label('email') !!}
				{!! Form::email('email', $admin->email, ['class' => 'form-control', 'maxlength' => '191']) !!}
				<span class="help-block">{{ $errors->first('email', '') }}</span>
			</div>
			<div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">
				{!! Form::label('role') !!}
				{!! Form::select('role', ['Super Admin' => 'Super Admin', 'Normal Admin' => 'Normal Admin'], $admin->role, ['class' => 'form-control']) !!}
				<span class="help-block">{{ $errors->first('role', '') }}</span>
			</div>
			<div class="form-group {{ $errors->has('region_id') ? 'has-error' : '' }}">
				<label for="region_id">Region</label>
				<select class="form-control" id="region_id" name="region_id">
					@foreach ($regions as $region)
						<option value="{{ $region->id }}" {{ $region->id == $admin->region_id ? 'selected' : '' }}>{{ $region->name }}</option>
					@endforeach
				</select>
				<span class="help-block">{{ $errors->first('region_id', '') }}</span>
			</div>
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
			<a href="{{ route('admin.admins.index') }}" class="btn btn-default">Cancel</a>
		</div>
	{!! Form::close() !!}
</div>

@endsection