@extends('admin.layouts.main')

@section('page-title', 'Users')

@section('breadcrumb')
<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active">Users</li>
@endsection

@section('content')
<div class="box">
	<div class="box-header">
		<h3 class="box-title">User List</h3>
        <div class="box-tools">
            <div class="btn-group btn-group-sm">
                <a href="{{ route('admin.users.create') }}" class="btn btn-primary btn-flat">Create User</a>
            </div>
        </div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="dataTables_wrapper form-inline dt-bootstrap">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped" id="table-pageList">
				        <thead>
				            <tr>
								<th>Name</th>
								<th>Email</th>
								<th>Approved by</th>
				                <th>Action</th>
				            </tr>
				        </thead>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.box-body -->
</div>
@endsection

@push('scripts')
<script>

function confirmApprove(id) {
	if (confirm('Are you sure you wish to approve this user?')) {
		$('#form-user-approve-' + id).submit();
	}
}

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-user-delete-' + id).submit();
	}
}

$(function() {
    $('#table-pageList').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.users.datatable') !!}',
        columns: [
			{ data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
			{ 
				data: 'null', 
				name: 'approved_by', 
				orderable: false, 
				searchable: false, 
				render: function(data, type, row) {
					if (row.approver) {
						return row.approver.name;
					}
					var html = '<a href="#" class="btn btn-success btn-flat btn-xs" onclick="event.preventDefault(); confirmApprove(:id)"><i class="fa fa-check-square-o"></i> Approve</a>';
					html += '{!! Form::open( [ 'method' => 'POST', 'route' => [ 'admin.users.approve', ':id' ], 'id' => 'form-user-approve-:id' ] ) !!}';
					html += '{!! Form::close() !!}';
					html = html.replace(/:id/g, row.id)
					return html;
				}
			},
            { 
            	data: 'null', 
            	name: 'action', 
            	orderable: false, 
            	searchable: false,
            	render: function(data, type, row) {
            		var html = '';
            		html += '<a href="{{ route('admin.users.edit', ['id' => ':id']) }}" class="btn btn-xs btn-default btn-flat"><i class="fa fa-pencil"></i> Edit</a>';
					if (!row.isself) {
						html += '<a href="#" class="btn btn-danger btn-flat btn-xs" onclick="event.preventDefault(); confirmDelete(:id)"><i class="fa fa-trash"></i> Delete</a>';
						html += '{!! Form::open( [ 'method' => 'DELETE', 'route' => [ 'admin.users.destroy', ':id' ], 'id' => 'form-user-delete-:id' ] ) !!}';
						html += '{!! Form::close() !!}';
					}
            		return html.replace(/:id/g, row.id);
            	}
        	}
        ]
    });
});
</script>
@endpush