<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{{ env('APP_NAME') }}</title>

	<link href="{{ asset('fav.ico') }}" rel="shortcut icon" />
	
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/dataTables.bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/Ionicons/css/ionicons.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-daterangepicker/daterangepicker.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/iCheck/all.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/select2/select2.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-magnific-popup/magnific-popup.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/adminlte/css/AdminLTE.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/admin.css') }}">
<!-- AdminLTE Skins. We have chosen the skin-blue for this starter
page. However, you can choose any other skin. Make sure you
apply the skin class to the body tag so the changes take effect. -->
<link rel="stylesheet" href="{{ asset('assets/adminlte/css/skins/skin-blue.min.css') }}">

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

	@stack('styles')
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<!-- Main Header -->
		<header class="main-header">

			<!-- Logo -->
			<a href="{{ route('admin.dashboard.index') }}" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini">EM</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><img src="{{ asset('assets/images/logo-em-white.png') }}" height="24"></span>
			</a>

			<!-- Header Navbar -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- User Account Menu -->
						<li class="dropdown">
							<!-- Menu Toggle Button -->
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-user-circle"></i>
								<!-- hidden-xs hides the username on small devices so only the image appears. -->
								<span class="hidden-xs"></span>
							</a>
							<ul class="dropdown-menu">
								<li>
								<a class="dropdown-item" href="{{ route('admin.password.edit') }}">Change password</a>
                                </li>
                                <li>
	                                <a class="dropdown-item" href="{{ route('admin.auth.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
	                                <form id="logout-form" action="{{ route('admin.auth.logout') }}" method="POST" style="display: none;">
	                                    @csrf
	                                </form>
                                </li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		
		@include('admin.layouts.sidebar')

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					@yield('page-title', 'CMS')
				</h1>
				<ol class="breadcrumb">
					@yield('breadcrumb')
				</ol>
			</section>

			<!-- Main content -->
			<section class="content container-fluid">
				@if (\Session::has('error'))
				<div class="alert alert-error alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					{{ \Session::get('error') }}
				</div>
				@endif
				@if (\Session::has('success'))
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					{{ \Session::get('success') }}
				</div>
				@endif
