@extends('admin.layouts.main')

@section('page-title', 'Invitations')

@section('content')

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Create New Invitation</h3>
	</div>
	{!! Form::open(['route' => 'admin.invitations.store', 'files' => true]) !!}
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="city_id">City</label>
						<select class="form-control select2" data-placeholder="Choose city" id="email_template_id" name="email_template_id">
							@foreach ($templates as $template)
							<option value="{{ $template->id }}" @if (old('email_template_id') == $template->id) {{ 'selected' }} @endif>{{ $template->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<!-- /.col-md-6 -->
				<div class="col-md-6"></div>
			</div>
			
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
			<a href="{{ route('admin.invitations.index') }}" class="btn btn-default">Cancel</a>
		</div>
	{!! Form::close() !!}
</div>

@endsection
