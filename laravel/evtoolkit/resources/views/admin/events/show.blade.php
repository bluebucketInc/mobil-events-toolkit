@extends('admin.layouts.main')

@section('page-title', 'Events')

@section('content')

<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Invitations</h3>
			</div>
			<div class="box-body">
				<table class="table">
					<tr>
						<th>Created date</th>
						<th>Sent date</th>
						<th>Sents</th>
						<th>Opens</th>
						<th>Clicks</th>
					<tr>
					@foreach ($event->invitations as $invitation)
					<tr>
						<td>{{ date('j M y h:i A', strtotime($invitation->created_at)) }}</td>
						<td>{{ $invitation->sent_at ? date('j M y h:i A', strtotime($invitation->sent_at)) : '' }}</td>
						<td>{{ $invitation->stat_sents > 0 ? $invitation->stat_sents : '' }}</td>
						<td>{{ $invitation->stat_opens > 0 ? $invitation->stat_opens : ''  }}</td>
						<td>{{ $invitation->stat_clicks > 0 ? $invitation->stat_clicks : ''  }}</td>
					</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Event Information</h3>
			</div>
			<div class="box-body">
				<table class="table table-tight table-noborder">
					<tr>
						<td>Name</td>
						<td>:</td>
						<td>{{ $event->name }}</td>
					</tr>
					<tr>
						<td>Date</td>
						<td>:</td>
						<td>{{ $event->date }}</td>
					</tr>
					<tr>
						<td>Time</td>
						<td>:</td>
						<td>{{ $event->time }}</td>
					</tr>
					<tr>
						<td>Location</td>
						<td>:</td>
						<td>{{ $event->location }}</td>
					</tr>
					<tr>
						<td>Sector</td>
						<td>:</td>
						<td>{{ $event->sector->name }}</td>
					</tr>
				</table>
			</div> <!-- ./box-body -->
		</div>
	</div>
</div>

@endsection