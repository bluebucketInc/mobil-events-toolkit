@extends('admin.layouts.main')

@section('page-title', 'Assets')

@section('breadcrumb')
<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active">Assets</li>
@endsection

@section('content')

<div class="box">
	<div class="box-body">
		<div class="btn-group">
			<a href="{{ route('admin.assets.create', ['sector_id' => $sector_id, 'category_id' => $category_id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add new asset</a>
		</div>

		<div class="btn-group pull-right">
			<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Category ({{ $category_id ? $categories[$category_id - 1]->name : 'All' }}) <span class="caret"></span></button>
			<ul class="dropdown-menu">
				<?php $index = 0; ?>
				@foreach ($categories as $category)
					<li><a href="{{ route('admin.assets.browse', ['sector_id' => $sector_id, 'category_id' => $category->id ]) }}">{{ $category->name }}: {{ $category->assets->count() }}</a></li>
				@endforeach
				<li class="divider"></li>
				<li><a href="{{ route('admin.assets.browse', ['sector_id' => $sector_id, 'category_id' => null ]) }}">No filter</a></li>
			</ul>
		</div>

		<div class="btn-group pull-right" style="margin-right: 10px;">
			@foreach ($sectors as $sector)
				<a href="{{ route('admin.assets.browse', ['sector_id' => $sector->id, 'category_id' => $category_id ]) }}" class="btn btn-sm {{ $sector->id == $sector_id ? 'btn-info' : 'btn-default' }}">{{ $sector->name }}: {{ $sector->assets->count() }}</a>
			@endforeach
		</div>

	</div>
</div>

<div class="row asset-grid">
	@foreach ($assets as $asset)
		<div class="col-md-3 col-sm-6">
			<div class="box asset-grid-item">
				<div class="box-head">
					@if ($asset->format == 'image')
					<a href="{{ url('uploads/event_assets/' . $asset->filename) }}" class="magnific-popup">
						<img src="{{ asset('uploads/event_assets/400x200-' . $asset->preview_filename) }}" class="img-responsive">
					</a>
					@elseif ($asset->format == 'video')
					<a href="#" data-source="{{ $asset->filename }}" class="preview-video">
						<img src="{{ asset('uploads/event_assets/400x200-' . $asset->preview_filename) }}" class="img-responsive">
					</a>
					@elseif ($asset->format == 'iframe')
					<a href="{{ url('uploads/event_assets/' . $asset->filename) }}" class="magnific-iframe">
						<img src="{{ asset('uploads/event_assets/400x200-' . $asset->preview_filename) }}" class="img-responsive">
					</a>
					@endif
				</div>
				<div class="box-footer">
					<h5>{{ $asset->name }}</h5>
					<div class="btn-group pull-right">
						<a href="{{ route('admin.assets.edit', ['id' => $asset->id ]) }}" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i> Edit</a>
						<a href="#" class="btn btn-danger btn-xs" onclick="event.preventDefault(); confirmDelete({{ $asset->id }})"><i class="fa fa-trash"></i> Delete</a>
						{!! Form::open( [ 'method' => 'DELETE', 'route' => [ 'admin.assets.destroy', $asset->id ], 'id' => 'form-asset-delete-' . $asset->id ] ) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	@endforeach	
</div>

@endsection

@include('modal/video_content')

@push('styles')
<style>
a.magnific-popup {
	cursor: zoom-in;
}
</style>
@endpush

@push('scripts')
<script>

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-asset-delete-' + id).submit();
	}
}

$(function() {
	$('input[type="checkbox"].minimal').iCheck({
		checkboxClass: 'icheckbox_minimal-blue'
	});

})

$('.magnific-popup').magnificPopup({type:'image'});
$('.preview-video').click(function(e) {
	e.preventDefault();
	var source = "{{ asset('uploads/event_assets') }}/" + $(this).data('source');
	$('#modal-video-content').modal();
	$('#video-container').get(0).pause();
    $('#video-source').attr('src', source);
    $('#video-container').get(0).load();
});
$('.magnific-popup-player').magnificPopup({
	items: {
		//src: '<video class="white-popup" src="http://localhost/extoolkit/public/uploads/event_assets/KyxFUW7amq953NFxkAgW.mp4"></video>',
		src: '<video id="video-container" controls><source id="video-source" src="http://localhost/extoolkit/public/uploads/event_assets/KyxFUW7amq953NFxkAgW.mp4" type="video/mp4"> Your browser does not support the video tag.</video>',
		type: 'inline'
	}
});
$('.magnific-iframe').magnificPopup({
	type: 'iframe'
});
$('.magnific-video').magnificPopup({
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: false,
    iframe: {
        markup: '<div class="mfp-iframe-scaler">'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
              '</div>',

        srcAction: 'iframe_src',
        }
});


</script>
@endpush