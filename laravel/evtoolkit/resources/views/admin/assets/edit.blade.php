@extends('admin.layouts.main')

@section('page-title', 'Assets')

@section('breadcrumb')
<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="{{ route('admin.assets.browse', [ 'sector_id' => $asset->sector_id, 'category_id' => $asset->category_id ]) }}">Assets</a></li>
<li class="active">Edit</li>
@endsection

@section('content')

<div class="col-md-6">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Edit Asset</h3>
		</div>
		{!! Form::open(['method' => 'PUT', 'route' => ['admin.assets.update', $asset->id], 'files' => true]) !!}
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('sector_id') ? 'has-error' : '' }}">
							<label for="sector_id">Sector</label>
							<select class="form-control select2" data-placeholder="Choose sector" id="sector_id" name="sector_id">
								@foreach ($sectors as $sector)
									<option value="{{ $sector->id }}" {{ $sector->id == $asset->sector_id ? 'selected' : '' }}>{{ $sector->name }}</option>
								@endforeach
							</select>
							<span class="help-block">{{ $errors->first('sector_id', '') }}</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
							<label for="category_id">Category</label>
							<select class="form-control select2" data-placeholder="Choose category" id="category_id" name="category_id">
								@foreach ($categories as $category)
									<option value="{{ $category->id }}" {{ $category->id == $asset->category_id ? 'selected' : '' }}>{{ $category->name }}</option>
								@endforeach
							</select>
							<span class="help-block">{{ $errors->first('category_id', '') }}</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
							{!! Form::label('name') !!}
							{!! Form::text('name', $asset->name, ['class' => 'form-control', 'maxlength' => '50']) !!}
							<span class="help-block">{{ $errors->first('name', '') }}</span>
						</div>
					</div>
				</div>
				<div class="form-group {{ $errors->has('asset') ? 'has-error' : '' }}">
					<div class="form-group">
						{!! Form::label('asset', 'Asset file') !!}
						{!! Form::file('asset', ['class' => 'form-control', 'accept' => 'image/x-png,image/gif,image/jpeg,image/tif,image/tiff,video/mp4,application/zip,application/pdf,application/vnd.openxmlformats-officedocument.presentationml.presentation']) !!}
						<span class="help-block">{{ $errors->first('asset', '') }}</span>
					</div>
				</div>
				<div class="form-group">
					<div class="form-group {{ $errors->has('preview') ? 'has-error' : '' }}">
						{!! Form::label('preview', 'Image preview') !!}
						{!! Form::file('preview', ['class' => 'form-control', 'accept' => 'image/x-png,image/gif,image/jpeg']) !!}
						<span class="help-block">{{ $errors->first('preview', '') }}</span>
					</div>
				</div>
			</div> <!-- /.box-body -->

			<div class="box-footer">
				{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
				<a href="{{ route('admin.assets.browse', [ 'sector_id' => $asset->sector_id, 'category_id' => $asset->category_id ] ) }}" class="btn btn-default">Cancel</a>
			</div>
		{!! Form::close() !!}
	</div> <!-- /.box -->
</div> <!-- /.col-md-6 -->

<div class="col-md-6">
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">Asset Media</h3>
		</div>
		<div class="box-body">
			@if ($asset->file_extension == 'mp4')
			<video width="320" height="240" controls>
				<source src="{{ url('uploads/event_assets/' . $asset->filename) }}" type="video/mp4">
				Your browser does not support the video tag.
			</video>
			@elseif ($asset->file_extension == 'pdf')
			<a href="{{ url('uploads/event_assets/' . $asset->filename) }}" class="magnific-iframe">
				<img src="{{ asset('uploads/event_assets/400x400-' . $asset->preview_filename) }}" class="img-responsive">
			</a>
			@else
			<img src="{{ asset('uploads/event_assets/' . $asset->filename) }}" class="img-responsive">
			@endif
		</div>
	</div> <!-- /.box -->
</div>

@endsection

@push('scripts')
<script>
	$('.select2').select2();
	$('.magnific-iframe').magnificPopup({
		type: 'iframe'
	});
</script>
@endpush