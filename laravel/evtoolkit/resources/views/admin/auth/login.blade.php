<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{{ env('APP_NAME') }} | Log in</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link href="{{ URL::asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Theme style -->
    <link href="{{ URL::asset('assets/adminlte/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css">
	<!-- iCheck -->
    <link href="{{ URL::asset('assets/vendor/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="{{ url('/') }}"><img src="{{ URL::asset('assets/images/logo.png') }}"></a>
		</div>
		<!-- /.login-logo -->
		<div class="login-box-body">
			<p class="login-box-msg">Sign in to start your session</p>

			{!! Form::open(['route' => 'admin.auth.login']) !!}
				<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
					{!! Form::email('email', old('email'), [ 'class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required', 'autofocus' => 'autofocus' ]) !!}
					<span class="help-block">{{ $errors->first('email', '') }}</span>
				</div>
				<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
					{!! Form::password('password', [ 'class' => 'form-control', 'placeholder' => 'Password' ]) !!}
					<span class="help-block">{{ $errors->first('password', '') }}</span>
				</div>
				<div class="row">
					<div class="col-xs-8">
						<div class="checkbox icheck">
							<label>
								<input type="checkbox"> Remember Me
							</label>
						</div>
					</div>
					<!-- /.col -->
					<div class="col-xs-4">
						{!! Form::submit('Sign In', [ 'class' => 'btn btn-primary btn-block btn-flat' ]) !!}	
					</div>
					<!-- /.col -->
				</div>
			{!! Form::close() !!}

	        <a href="{{ url('password/reset') }}">Forgot Your Password?</a>
		</div>
		<!-- /.login-box-body -->
	</div>
	<!-- /.login-box -->

    <script src="{{ URL::asset('assets/vendor/jquery/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ URL::asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/vendor/iCheck/icheck.min.js') }}"></script>

	<script>
		$(function () {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' // optional
			});
		});
	</script>
</body>
</html>
