@extends('admin.layouts.main')

@section('page-title', 'Email Templates')

@section('breadcrumb')
<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active">Email templates</li>
@endsection

@section('content')

<div class="row">
	<div class="col-md-3">
		<a href="{{ route('admin.emailTemplates.syncFromSendinblue') }}" class="btn btn-primary btn-block margin-bottom">Sync from SendinBlue</a>
	</div>
</div>

<div class="row">
	@foreach ($templates as $template)
		<div class="col-md-3">
			<div class="box">
				<div class="box-head">
					<a href="{{ url('uploads/email_templates/' . $template->image_preview_filename) }}" class="template-popup">
						<img src="{{ asset('uploads/email_templates/t-' . $template->image_preview_filename) }}" class="img-responsive">
					</a>
				</div>
				<div class="box-footer">
					<a href="{{ route('admin.emailTemplates.edit', $template->id) }}" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i> Edit</a>
					@if ($template->active)
						<a href="#" class="btn btn-success btn-xs" onclick="event.preventDefault(); deactivate({{ $template->id }})"><i class="fa fa-check-square-o"></i> Active</a>
						{!! Form::open( [ 'method' => 'PUT', 'route' => [ 'admin.emailTemplates.deactivate', $template->id ], 'id' => 'form-emailtemplate-deactivate-' . $template->id ] ) !!}
						{!! Form::close() !!}
					@else
						<a href="#" class="btn btn-default btn-xs" onclick="event.preventDefault(); activate({{ $template->id }})"><i class="fa fa-square-o"></i> Inactive</a>
						{!! Form::open( [ 'method' => 'PUT', 'route' => [ 'admin.emailTemplates.activate', $template->id ], 'id' => 'form-emailtemplate-activate-' . $template->id ] ) !!}
						{!! Form::close() !!}
					@endif
				</div>
			</div>
		</div>
	@endforeach
</div>

@endsection

@push('styles')
<style>
a.template-popup {
	cursor: zoom-in;
}
</style>
@endpush

@push('scripts')
<script>

function activate(id) {
	$('#form-emailtemplate-activate-' + id).submit();
}
function deactivate(id) {
	$('#form-emailtemplate-deactivate-' + id).submit();
}
$('.template-popup').magnificPopup({type:'image'});

</script>
@endpush