@extends('admin.layouts.main')

@section('page-title', 'Email Templates')

@section('content')

<div class="row">
	<div class="col-md-5">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Email Template</h3>
			</div>
			{!! Form::open(['method' => 'PUT', 'route' => ['admin.emailTemplates.update', $template->id], 'files' => true]) !!}
				<div class="box-body">
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						{!! Form::label('name') !!}
						{!! Form::text('name', $template->name, ['class' => 'form-control', 'maxlength' => '50']) !!}
						<span class="help-block">{{ $errors->first('name', '') }}</span>
					</div>
					<div class="form-group {{ $errors->has('image_preview_filename') ? 'has-error' : '' }}">
						{!! Form::label('image_preview_filename') !!}
						{!! Form::file('image_preview_filename', ['class' => 'form-control', 'accept' => 'image/x-png,image/gif,image/jpeg']) !!}
						<span class="help-block">{{ $errors->first('image_preview_filename', '') }}</span>
						<img class="img-thumbnail" src="{{ asset('uploads/email_templates/' . $template->image_preview_filename) }}">
					</div>
				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
					<a href="{{ route('admin.emailTemplates.index') }}" class="btn btn-default">Cancel</a>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
	<div class="col-md-7">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">HTML Preview</h3>
			</div>
			<div class="edm-html-wrapper">
				{!! $template->html_content !!}
			</div>
		</div>
	</div>
</div>

@endsection