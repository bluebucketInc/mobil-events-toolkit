@extends('admin.layouts.main')

@section('page-title', 'Email Templates')

@section('content')

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Create New Email Template</h3>
	</div>
	{!! Form::open(['route' => 'admin.emailtemplates.store', 'files' => true]) !!}
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('name') !!}
						{!! Form::text('name', '', ['class' => 'form-control', 'maxlength' => '50']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('hubspot_template_id') !!}
						{!! Form::text('hubspot_template_id', '', ['class' => 'form-control', 'maxlength' => '50']) !!}
					</div>		
				</div>
				<!-- /.col-md-6 -->
				<div class="col-md-6"></div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
			<a href="{{ route('admin.emailtemplates.index') }}" class="btn btn-default">Cancel</a>
		</div>
	{!! Form::close() !!}
</div>

@endsection
