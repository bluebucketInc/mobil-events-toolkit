@extends('admin.layouts.main')

@section('page-title', 'Customers')

@section('content')

<div class="row">
	<div class="col-md-6">

		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Add New Customer</h3>
			</div> <!-- /.box-header -->
			{!! Form::open(['route' => 'admin.customers.store']) !!}
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
							{!! Form::label('first_name') !!}
							{!! Form::text('first_name', '', ['class' => 'form-control', 'maxlength' => '50']) !!}
							<span class="help-block">{{ $errors->first('first_name', '') }}</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
							{!! Form::label('last_name') !!}
							{!! Form::text('last_name', '', ['class' => 'form-control', 'maxlength' => '191']) !!}
							<span class="help-block">{{ $errors->first('last_name', '') }}</span>
						</div>
					</div>
				</div>
				<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
					{!! Form::label('email') !!}
					{!! Form::email('email', '', ['class' => 'form-control', 'maxlength' => '191']) !!}
					<span class="help-block">{{ $errors->first('email', '') }}</span>
				</div>
				<div class="form-group {{ $errors->has('company') ? 'has-error' : '' }}">
					{!! Form::label('company') !!}
					{!! Form::text('company', '', ['class' => 'form-control', 'maxlength' => '100']) !!}
					<span class="help-block">{{ $errors->first('company', '') }}</span>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="sector_id">Sector</label>
							<select class="form-control select2" id="sector_id" name="sector_id">
								@foreach ($sectors as $sector)
								<option value="{{ $sector->id }}")>{{ $sector->name }}</option>
								@endforeach
							</select>
						</div>
						
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="job_title">Job title</label>
							<select class="form-control select2" id="job_title" name="job_title">
								@foreach ($jobTitles as $jobTitle)
								<option value="{{ $jobTitle->id }}">{{ $jobTitle->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div> <!-- /.box-body -->

			<div class="box-footer">
				{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
				<a href="{{ route('admin.customers.index') }}" class="btn btn-default">Cancel</a>
			</div>
			{!! Form::close() !!}
		</div>
	</div> <!-- /.col-md-6 -->
	
</div>

@endsection

@push('scripts')
<script>
	$('.select2').select2();
</script>
@endpush