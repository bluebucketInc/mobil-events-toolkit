@extends('admin.layouts.main')

@section('content')

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Create New Value</h3>
	</div>
	{!! Form::open(['route' => 'admin.values.store']) !!}
		<div class="box-body">
			<div class="form-group">
				{!! Form::label('name') !!}
				{!! Form::text('name', '', ['class' => 'form-control', 'maxlength' => '50']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('value') !!}
				{!! Form::textarea('value', '', ['class' => 'form-control']) !!}
			</div>
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
			<a href="{{ route('admin.values.index') }}" class="btn btn-default">Cancel</a>
		</div>
	{!! Form::close() !!}
</div>

@endsection
