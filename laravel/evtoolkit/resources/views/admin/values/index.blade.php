@extends('admin.layouts.main')

@section('content')
<div class="box">
	<div class="box-header">
		<h3 class="box-title">Value List</h3>
        <div class="box-tools">
            <div class="btn-group btn-group-sm">
                <a href="{{ route('admin.values.create') }}" class="btn btn-primary btn-flat">Create Value</a>
            </div>
        </div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="dataTables_wrapper form-inline dt-bootstrap">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped" id="table-valueList">
				        <thead>
				            <tr>
				                <th>Id</th>
								<th>Name</th>
								<th>Value</th>
				                <th>Action</th>
				            </tr>
				        </thead>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.box-body -->
</div>
@endsection

@push('scripts')
<script>

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-value-delete-' + id).submit();
	}
}

$(function() {
    $('#table-valueList').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.values.datatable') !!}',
        columns: [
            { data: 'id', name: 'id' },
			{ data: 'name', name: 'name' },
			{ data: 'value', name: 'value' },
            { 
            	data: 'null', 
            	name: 'action', 
            	orderable: false, 
            	searchable: false,
            	render: function(data, type, row) {
            		var html = '';
            		html += '<a href="{{ route('admin.values.edit', ['id' => ':id']) }}" class="btn btn-xs btn-default btn-flat"><i class="fa fa-pencil"></i> Edit</a>';
	                html += '<a href="#" class="btn btn-danger btn-flat btn-xs" onclick="event.preventDefault(); confirmDelete(:id)"><i class="fa fa-trash"></i> Delete</a>';
	                html += '{!! Form::open( [ 'method' => 'DELETE', 'route' => [ 'admin.values.destroy', ':id' ], 'id' => 'form-value-delete-:id' ] ) !!}';
	                html += '{!! Form::close() !!}';
            		return html.replace(/:id/g, row.id);
            	}
        	}
        ]
    });
});
</script>
@endpush