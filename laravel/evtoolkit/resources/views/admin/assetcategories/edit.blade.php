@extends('admin.layouts.main')

@section('page-title', 'Asset Categories')

@section('content')

<div class="row">
	<div class="col-md-6">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Asset Category</h3>
			</div>
			{!! Form::open(['route' => 'admin.assetcategories.store']) !!}
				<div class="box-body">
					
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						{!! Form::label('name') !!}
						{!! Form::text('name', $assetCategory->name, ['class' => 'form-control', 'maxlength' => '50']) !!}
						<span class="help-block">{{ $errors->first('name', '') }}</span>
					</div>
					<div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
						{!! Form::label('slug') !!}
						{!! Form::text('slug', $assetCategory->slug, ['class' => 'form-control', 'maxlength' => '50']) !!}
						<span class="help-block">{{ $errors->first('slug', '') }}</span>
					</div>
					<div class="form-group {{ $errors->has('order') ? 'has-error' : '' }}">
						{!! Form::label('order') !!}
						{!! Form::number('order', $assetCategory->order, ['class' => 'form-control']) !!}
						<span class="help-block">{{ $errors->first('order', '') }}</span>
					</div>
					<div class="form-group {{ $errors->has('label_color') ? 'has-error' : '' }}">
						{!! Form::label('label_color') !!}
						{!! Form::text('label_color', $assetCategory->label_color, ['class' => 'form-control']) !!}
						<span class="help-block">{{ $errors->first('label_color', '') }}</span>
					</div>

				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
					<a href="{{ route('admin.assetcategories.index') }}" class="btn btn-default">Cancel</a>
				</div>
			{!! Form::close() !!}
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col-md-6 -->
</div>
<!-- /.row -->

@endsection
