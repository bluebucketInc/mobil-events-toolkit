@extends('admin.layouts.main')

@section('page-title', 'Regions')

@section('content')

<div class="row">
	<div class="col-md-6">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Region</h3>
			</div>
			{!! Form::open(['method' => 'PUT', 'route' => ['admin.regions.update', $region->id], 'files' => true]) !!}
				<div class="box-body">
					
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						{!! Form::label('name') !!}
						{!! Form::text('name', $region->name, ['class' => 'form-control', 'maxlength' => '50']) !!}
						<span class="help-block">{{ $errors->first('name', '') }}</span>
					</div>
					<div class="form-group {{ $errors->has('order') ? 'has-error' : '' }}">
						{!! Form::label('order') !!}
						{!! Form::number('order', $region->order, ['class' => 'form-control']) !!}
						<span class="help-block">{{ $errors->first('order', '') }}</span>
					</div>
					<div class="form-group {{ $errors->has('order') ? 'has-error' : '' }}">
						{!! Form::label('active') !!}
						{!! Form::select('active', [ 0 => 'No', 1 => 'Yes' ], $region->active, ['class' => 'form-control']) !!}
						<span class="help-block">{{ $errors->first('active', '') }}</span>
					</div>

				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
					<a href="{{ route('admin.regions.index') }}" class="btn btn-default">Cancel</a>
				</div>
			{!! Form::close() !!}
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col-md-6 -->
</div>
<!-- /.row -->

@endsection
