@extends('admin.layouts.main')

@section('page-title', 'Pages')

@section('content')
<div class="box">
	<div class="box-header">
		<h3 class="box-title">Page List</h3>
        <div class="box-tools">
            <div class="btn-group btn-group-sm">
                <a href="{{ route('admin.pages.create') }}" class="btn btn-primary btn-flat">Create Page</a>
            </div>
        </div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="dataTables_wrapper form-inline dt-bootstrap">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped" id="table-pageList">
				        <thead>
				            <tr>
								<th>Id</th>
								<th>Title</th>
								<th>Slug</th>
								<th>Published</th>
				                <th>Action</th>
				            </tr>
				        </thead>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.box-body -->
</div>
@endsection

@push('scripts')
<script>

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-page-delete-' + id).submit();
	}
}

$(function() {
    $('#table-pageList').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.pages.datatable') !!}',
        columns: [
			{ data: 'id', name: 'id' },
			{ data: 'title', name: 'title' },
			{ data: 'slug', name: 'slug' },
			{ data: 'published', name: 'published' },
            { 
            	data: 'null', 
            	name: 'action', 
            	orderable: false, 
            	searchable: false,
            	render: function(data, type, row) {
            		var html = '';
            		html += '<a href="{{ route('admin.pages.edit', ['id' => ':id']) }}" class="btn btn-xs btn-default btn-flat"><i class="fa fa-pencil"></i> Edit</a>';
	                html += '<a href="#" class="btn btn-danger btn-flat btn-xs" onclick="event.preventDefault(); confirmDelete(:id)"><i class="fa fa-trash"></i> Delete</a>';
	                html += '{!! Form::open( [ 'method' => 'DELETE', 'route' => [ 'admin.pages.destroy', ':id' ], 'id' => 'form-page-delete-:id' ] ) !!}';
	                html += '{!! Form::close() !!}';
            		return html.replace(/:id/g, row.id);
            	}
        	}
        ]
    });
});
</script>
@endpush