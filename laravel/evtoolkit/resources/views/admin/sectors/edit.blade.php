@extends('admin.layouts.main')

@section('page-title', 'Sectors')

@section('content')

<div class="row">
	<div class="col-md-6">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Sector</h3>
			</div>
			{!! Form::open(['method' => 'PUT', 'route' => ['admin.sectors.update', $sector->id], 'files' => true]) !!}
				<div class="box-body">
					
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						{!! Form::label('name') !!}
						{!! Form::text('name', $sector->name, ['class' => 'form-control', 'maxlength' => '50']) !!}
						<span class="help-block">{{ $errors->first('name', '') }}</span>
					</div>
					<div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
						{!! Form::label('slug') !!}
						{!! Form::text('slug', $sector->slug, ['class' => 'form-control', 'maxlength' => '50']) !!}
						<span class="help-block">{{ $errors->first('slug', '') }}</span>
					</div>
					<div class="form-group {{ $errors->has('order') ? 'has-error' : '' }}">
						{!! Form::label('order') !!}
						{!! Form::number('order', $sector->order, ['class' => 'form-control']) !!}
						<span class="help-block">{{ $errors->first('order', '') }}</span>
					</div>
					<div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
						{!! Form::label('image', 'Image file') !!}
						{!! Form::file('image', ['class' => 'form-control']) !!}
						<img src="{{ asset('uploads/sectors/preview-' . $sector->image_path) }}" class="img-thumbnail">
						<span class="help-block">{{ $errors->first('image', '') }}</span>
					</div>

				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
					<a href="{{ route('admin.sectors.index') }}" class="btn btn-default">Cancel</a>
				</div>
			{!! Form::close() !!}
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col-md-6 -->
</div>
<!-- /.row -->

@endsection
