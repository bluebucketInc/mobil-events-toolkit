@extends('layouts/main')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-mobil">
                    {!! Form::open(['route' => 'user.update_password']) !!}
                    <div class="panel-heading">
                        <h3 class="panel-title"><b>Change Password</b></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group col-md-12 {{ $errors->has('old_password') ? 'has-error' : '' }}">
                                {!! Form::label('old_password') !!}
                                {!! Form::password('old_password', ['class' => 'form-control', 'maxlength' => '191']) !!}
                                <span class="help-block">{{ $errors->first('old_password', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 {{ $errors->has('password') ? 'has-error' : '' }}">
                                {!! Form::label('password', 'New password') !!}
                                {!! Form::password('password', ['class' => 'form-control', 'maxlength' => '191']) !!}
                                <span class="help-block">{{ $errors->first('password', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                {!! Form::label('password_confirmation', 'Confirm new password') !!}
                                {!! Form::password('password_confirmation', ['class' => 'form-control', 'maxlength' => '191']) !!}
                            </div>
                        </div>
                    </div> <!-- ./panel-body -->
                    
                    <div class="panel-footer">
                        <div class="pull-right">
			                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
@endsection