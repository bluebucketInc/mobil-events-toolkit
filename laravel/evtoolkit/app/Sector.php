<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    public $timestamps = false;

    public function assets()
    {
        return $this->hasMany('App\Asset');
    }
}
