<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sichikawa\LaravelSendgridDriver\SendGrid;

class ExhibitionInvitation extends Mailable
{
    use Queueable, SerializesModels, SendGrid;

    public $personalizations;
    public $template_id;
    public $code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($personalizations, $template_id, $code)
    {
        $this->personalizations = $personalizations;
        $this->template_id = $template_id;
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this
            ->view([])
            ->subject('Exhibition Invitation')
            ->from('info@extoolkit.com')
            ->sendgrid([
                'personalizations' => $this->personalizations,
                'template_id' => $this->template_id,
            ])
            ->withSwiftMessage(function ($message) {
                $message->getHeaders()->addTextHeader('invitation-code', $this->code);
            });
    }
}
