<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminLog extends Model
{
    public $timestamps = false;
    protected $dates = ['created_at'];

    public function jsonValues()
    {
        return $this->jsonValues = json_decode($this->values);
    }

    public static function record($description, $values = [], $admin_id = null)
    {
        $log = new AdminLog;
        $log->admin_id = $admin_id ? $admin_id : \Auth::guard('admin')->user()->id;
        $log->description = $description;
        $log->values = json_encode($values);
        $log->created_at = date('Y-m-d H:i:s');
        $log->save();
    }
}
