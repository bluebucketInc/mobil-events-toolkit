<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    public function sector()
    {
        return $this->belongsTo('App\Sector');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function invitations()
    {
        return $this->hasMany('App\Invitation');
    }
}
