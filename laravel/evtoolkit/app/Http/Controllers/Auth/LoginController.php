<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        $user = User::where('email', $request->input('email'))->first();
        if (!$user) {
            return redirect()->back()->withErrors([
                'email' => 'Account not found or incorrect password'
            ])->withInput($request->only('email', 'remember'));
        }

        if (!$user->approved_at) {
            return redirect()->back()->withErrors([
                'email' => 'Your account has not been approved yet'
            ])->withInput($request->only('email', 'remember'));
        }

        // Attempt to log the user in
        if (Auth::guard()->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(url('/'));
        }

        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->back()->withErrors([
            'email' => 'Account not found or incorrect password'
        ])->withInput($request->only('email', 'remember'));
    }

    public function logout()
    {
        Auth::guard()->logout();
        return redirect('/');
    }
    
}
