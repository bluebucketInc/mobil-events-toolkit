<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\JobTitle;
use App\User;

class RegisterController extends Controller
{

    use RegistersUsers;

    protected $redirectTo = '/register/complete';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function form()
    {
        $jobTitles = JobTitle::orderBy('name')->get();
        return view('register.form', compact('jobTitles'));
    }

    public function submit(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'company' => 'required',
            'job_title' => 'required',
            'email' => 'required',
            'password' => 'required|min:6|confirmed',
            'agreed' => 'required',
        ]);

        return view('register', compact('jobTitles'));
    }

    public function complete()
    {
        return view('register.complete');
    }

        /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'code' => str_random(30),
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'company' => $data['company'],
            'job_title_id' => $data['job_title'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
