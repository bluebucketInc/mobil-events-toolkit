<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\EmailTemplate;
use App\Event;
use App\Invitation;
use App\Sector;
use DB;
use Sendinblue;

class InvitationController extends Controller
{

    public function template($id)
    {
        $invitation = Invitation::with('event')->where('id', $id)->first();
        if (!$invitation) return redirect('events')->with('error', 'Invitation ID not found');

        $templates = EmailTemplate::all();
        return view('invitations.template', compact('invitation', 'templates'));
    }

    public function chooseTemplate($id, $template_id)
    {
        $invitation = Invitation::find($id);
        if (!$invitation) return redirect('events')->with('error', 'Invitation ID not found');

        $email_template = EmailTemplate::find($template_id);
        
        $invitation->email_template_id = $template_id;

        $html = $email_template->html_content;
        if ($invitation->text_block_1) {
            $html = str_replace('_TEXT_BLOCK_1_', nl2br($invitation->text_block_1), $html);
        }
        if ($invitation->text_block_2) {
            $html = str_replace('_TEXT_BLOCK_2_', nl2br($invitation->text_block_2), $html);
        }
        $invitation->html_content = $html;

        $invitation->save();

        return redirect()->route('invitations.customize', $id);
    }

    public function customize($id)
    {
        $invitation = Invitation::with('email_template')->where('id', $id)->first();
        if (!$invitation) return redirect('events')->with('error', 'Invitation ID not found');

        return view('invitations.customize', compact('invitation'));
    }

    public function confirmation($id)
    {
        $invitation = Invitation::with('email_template')->where('id', $id)->first();
        if (!$invitation) return redirect('events')->with('error', 'Invitation ID not found');

        return view('invitations.confirmation', compact('invitation'));
    }

    public function test(Request $request, $id)
    {
        $request->validate([
            'emails' => 'required',
        ]);
        $invitation = Invitation::find($id);
        $campaignInstance = new \SendinBlue\Client\Api\EmailCampaignsApi(null, Sendinblue::getConfiguration());
        $campaignId = $invitation->sendinblue_campaign_id;
        $emailTo = new \SendinBlue\Client\Model\SendTestEmail(); // \SendinBlue\Client\Model\SendTestEmail | 
        $emails = explode(",", $request->input('emails'));
        $emailTo->setEmailTo($emails);

        try {
            $campaignInstance->sendTestEmail($campaignId, $emailTo);
        } catch (\Exception $e) {
            dd($e);
        }

        return redirect()->back()->with('success', 'A test email has been sent');
    }

    public function ajax_save(Request $request, $id)
    {
        $invitation = Invitation::with('email_template')->where('id', $id)->first();
        $invitation->text_block_1 = $request->input('text_block_1');
        $invitation->text_block_2 = $request->input('text_block_2');

        $html = $invitation->email_template->html_content;
        if ($invitation->text_block_1) {
            $html = str_replace('_TEXT_BLOCK_1_', nl2br($invitation->text_block_1), $html);
        }
        if ($invitation->text_block_2) {
            $html = str_replace('_TEXT_BLOCK_2_', nl2br($invitation->text_block_2), $html);
        }
        $invitation->html_content = $html;

        $invitation->save();

        return response()->json([
            'status' => 'success',
        ]);
    }

    public function ajax_send(Request $request, $id)
    {
        $invitation = Invitation::find($id);

        $campaignInstance = new \SendinBlue\Client\Api\EmailCampaignsApi(null, Sendinblue::getConfiguration());
        $campaignId = $invitation->sendinblue_campaign_id;
        
        try {
            $campaignInstance->sendEmailCampaignNow($campaignId);

            $invitation->sent = true;
            $invitation->sent_at = date('Y-m-d H:i:s');
            $invitation->save();
        } catch (Exception $e) {
            echo 'Exception when calling EmailCampaignsApi->sendEmailCampaignNow: ', $e->getMessage(), PHP_EOL;
        }        
    }

}
