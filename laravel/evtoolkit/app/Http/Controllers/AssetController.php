<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\AssetCategory;
use App\Package;
use App\Sector;

class AssetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function selectSector()
    {
        $sectors = Sector::with('assets')->where('active', 1)->orderBy('order')->get();
        return view('assets.sector', compact('sectors'));
    }

    public function browse($sector_slug, $view = 'grid', $sort = 'newest', $fc = 'all', $search = '')
    {
        
        $sector = Sector::where('slug', $sector_slug)->first();
        if (!$sector) {
            return redirect('assets');
        }

        
        $categories = AssetCategory::with('assets')
        ->where('active', 1)
        ->orderBy('order')
        ->get();
        
        $newPackage = Package::with('assets')->where('user_id', \Auth::user()->id)->where('status', 0)->first();
        
        $user = \Auth::user();
        $q = Asset::with('category')
                ->where('sector_id', $sector->id)
                ->Where('name', 'LIKE', '%' . $search . '%')
                ->where('region_id', $user->region_id);

        if ($fc != 'all') {
            $q = $q->whereHas('category', function ($query) use ($fc) {
                $query->where('slug', $fc);
            });
        }

        if ($sort == 'oldest') {
            $q = $q->orderBy('created_at', 'asc');
        } else if ($sort == 'smallest') {
            $q = $q->orderBy('filename', 'asc');
        } else if ($sort == 'largest') {
            $q = $q->orderBy('filename', 'desc');
        } else {
            $q = $q->orderBy('created_at', 'desc');
        }      

        $assets = $q->paginate(16);
        foreach ($assets as $asset) {
            $arr = explode(".", $asset->filename);
            $ext = end($arr);
            $asset->file_extension = $ext;
            if ($ext == 'mp4') {
                $asset->format = 'video';
            } else if ($ext == 'png' | $ext == 'jpg' | $ext == 'jpeg' | $ext == 'gif') {
                $asset->format = 'image';
            } else if ($ext == 'pdf') {
                $asset->format = 'pdf';
            } else {
                $asset->format = 'iframe';
            }
        }

        return view('assets.browse', compact('assets', 'categories', 'newPackage', 'sector', 'fc', 'sort', 'view', 'search'));
    }

}
