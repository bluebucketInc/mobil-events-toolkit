<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\JobTitle;
use App\Sector;
use App\User;

class LeadFormController extends Controller
{

    public function form($user_code)
    {
        $user = User::where('code', $user_code)->first();
        if (!$user) {
            return redirect('/');
        }

        $jobTitles = JobTitle::orderBy('name', 'ASC')->get();
        $sectors = Sector::where('active', 1)->orderBy('name', 'ASC')->get();
        return view('leadform.form', compact('jobTitles', 'sectors', 'user'));
    }

    public function submit(Request $request, $user_code)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'company' => 'required',
            'sector_id' => 'required',
            'job_title_id' => 'required',
            'check_permission' => 'required',
        ]);

        $user = User::where('code', $user_code)->first();
        if (!$user) {
            return redirect()->back();
        }

        $customer = new Customer;
        $customer->email = $request->email;
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->company = $request->company;
        $customer->sector_id = $request->sector_id;
        $customer->job_title_id = $request->job_title_id;
        $customer->user_id = $user->id;
        $customer->save();

        $customer->hubspot_sync($user->id);

        return redirect()->route('leadform.form', ['user_code' => $user_code])->with('success', 'success');
    }

}
