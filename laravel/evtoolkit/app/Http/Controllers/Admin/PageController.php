<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DataTables;
use File;
use Image;

use App\Page;

class PageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.pages.index');
    }

    public function create()
    {
        return view('admin.pages.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'slug' => 'required',
            'title' => 'required',
        ]);

        $page = new Page;
        $page->slug        = $request->input('slug');
        $page->title       = $request->input('title');
        $page->description = $request->input('description');
        $page->content     = $request->input('content');
        $page->published   = $request->input('published');
        $page->save();

        return redirect()->route('admin.pages.index');
    }

    public function edit($id)
    {
        $page = Page::find($id);

        return view('admin.pages.edit', compact('page'));
    }

    public function update(Request $request, $id)
    {
        $page = Page::find($id);
        $page->slug        = $request->input('slug');
        $page->title       = $request->input('title');
        $page->description = $request->input('description');
        $page->content     = $request->input('content');
        $page->published   = $request->input('published');
        $page->save();

        return redirect()->route('admin.pages.index');
    }

    public function destroy($id)
    {
        $page = Page::find($id);
        $page->delete();
        
        return back();
    }

    public function datatable()
    {
        return DataTables::of(Page::all())
            ->make(true);
    }

}
