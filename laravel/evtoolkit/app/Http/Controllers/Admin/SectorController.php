<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DataTables;
use File;
use Image;

use App\AdminLog;
use App\Sector;

class SectorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.sectors.index');
    }

    public function create()
    {
        $last_sector = Sector::orderBy('order', 'DESC')->first();
        $new_order = $last_sector ? $last_sector->order + 1 : 0;
        return view('admin.sectors.create', compact('new_order'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:sectors',
            'slug' => 'required|unique:sectors',
            'order' => 'required|numeric',
            'image' => 'required|image',
        ]);

        //create directory/folder if doesn't exist
        $path = public_path().'/uploads';
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        $path = public_path().'/uploads/sectors';
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        
        $sector = new Sector;

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            
            $filename = str_random(20) . '.' . $ext;

            $img = Image::make($file)->orientate();
            $img->fit(300, 181)->save(public_path('uploads/sectors/' . $filename));
            $img->fit(150, 90)->save(public_path('uploads/sectors/preview-' . $filename));

            $sector->image_path = $filename;
        } 

        $sector->name = $request->input('name');
        $sector->slug = $request->input('slug');
        $sector->order = $request->input('order');
        $sector->active = true;

        $sector->save();

        AdminLog::record('Created a sector: ' . $sector->name, ['sector_id' => $sector->id]);

        return redirect()->route('admin.sectors.index');
    }

    public function edit($id)
    {
        $sector = Sector::find($id);

        return view('admin.sectors.edit', compact('sector'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:sectors,id,' . $id,
            'slug' => 'required|unique:sectors,id,' . $id,
            'order' => 'required|numeric',
            'image' => 'image',
        ]);

        //create directory/folder if doesn't exist
        $path = public_path().'/uploads';
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        $path = public_path().'/uploads/sectors';
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

        $sector = Sector::find($id);

        if ($request->hasFile('image')) {

            //delete previous file
            File::delete(public_path('uploads/sectors/' . $sector->image_path));
            File::delete(public_path('uploads/sectors/preview-' . $sector->image_path));

            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            
            $filename = str_random(20) . '.' . $ext;

            $img = Image::make($file)->orientate();
            $img->fit(300, 181)->save(public_path('uploads/sectors/' . $filename));
            $img->fit(150, 90)->save(public_path('uploads/sectors/preview-' . $filename));

            $sector->image_path = $filename;
        } 

        $sector->name = $request->input('name');
        $sector->slug = $request->input('slug');
        $sector->order = $request->input('order');
        $sector->active = true;

        $sector->save();

        AdminLog::record('Updated a sector: ' . $sector->name, ['sector_id' => $sector->id]);

        return redirect()->back();
    }

    public function destroy($id)
    {
        $sector = Sector::find($id);
        AdminLog::record('Deleted a sector: ' . $sector->name, ['sector_id' => $sector->id]);
        $sector->delete();
        return back();
    }

    public function datatable()
    {
        return DataTables::of(Sector::all())
            ->make(true);
    }

}
