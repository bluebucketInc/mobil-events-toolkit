<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;

use App\Value;

class ValueController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.values.index');
    }

    public function create()
    {
        return view('admin.values.create');
    }

    public function store(Request $request)
    {
        $value = new Value;
        $value->name     = $request->input('name');
        $value->value    = $request->input('value');
        $value->save();

        return redirect()->route('admin.values.index');
    }

    public function edit($id)
    {
        $value = Value::find($id);
        
        return view('admin.values.edit', compact('value'));
    }

    public function update(Request $request, $id)
    {
        $value = Value::find($id);
        $value->name  = $request->input('name');
        $value->value = $request->input('value');
        $value->save();

        return redirect()->route('admin.values.index');
    }

    public function destroy($id)
    {
        $value = Value::find($id);
        $value->delete();
        
        return back();
    }

    public function datatable()
    {
        return DataTables::of(Value::all())
            ->make(true);
    }

}
