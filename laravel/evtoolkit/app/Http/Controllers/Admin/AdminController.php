<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;

use App\Admin;
use App\Region;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');    
    }

    public function index()
    {
        if (\Auth::guard('admin')->user()->role == 'Normal Admin') return redirect()->route('admin.dashboard.index');
        return view('admin.admins.index');
    }

    public function create()
    {
        if (\Auth::guard('admin')->user()->role == 'Normal Admin') return redirect()->route('admin.dashboard.index');
        $regions = Region::where('active', 1)->orderBy('order', 'ASC')->get();
        return view('admin.admins.create', compact('regions'));
    }

    public function store(Request $request)
    {
        if (\Auth::guard('admin')->user()->role == 'Normal Admin') return redirect()->route('admin.dashboard.index');
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:admins',
            'password' => 'required|min:6|confirmed'
        ]);

        $admin           = new Admin;
        $admin->name     = $request->input('name');
        $admin->email    = $request->input('email');
        $admin->password = bcrypt($request->input('password'));
        $admin->role     = $request->input('role');
        $admin->region_id = $request->input('region_id');
        $admin->save();

        return redirect()->route('admin.admins.index');
    }

    public function edit($id)
    {
        if (\Auth::guard('admin')->user()->role == 'Normal Admin') return redirect()->route('admin.dashboard.index');
        
        $admin = Admin::with('region')->where('id', $id)->first();
        $regions = Region::where('active', 1)->orderBy('order', 'ASC')->get();

        return view('admin.admins.edit', compact('admin', 'regions'));
    }

    public function update(Request $request, $id)
    {
        if (\Auth::guard('admin')->user()->role == 'Normal Admin') return redirect()->route('admin.dashboard.index');
        
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:admins,email,' . $id,
        ]);
        
        $admin = Admin::find($id);
        $admin->name  = $request->input('name');
        $admin->email = $request->input('email');
        $admin->role = $request->input('role');
        $admin->region_id = $request->input('region_id');
        $admin->save();

        return redirect()->route('admin.admins.index');
    }

    public function destroy($id)
    {
        if (\Auth::guard('admin')->user()->role == 'Normal Admin') return redirect()->route('admin.dashboard.index');
        
        $user = Admin::find($id);
        $user->delete();
        
        return back();
    }

    public function datatable()
    {
        if (\Auth::guard('admin')->user()->role == 'Normal Admin') return redirect()->route('admin.dashboard.index');
        $region_id = \Auth::guard('admin')->user()->region_id;
        $admins = Admin::where('region_id', $region_id)->get();
        return DataTables::of($admins)
            ->make(true);
    }

}
