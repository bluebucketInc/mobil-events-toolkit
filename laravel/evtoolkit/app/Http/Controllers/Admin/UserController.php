<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use DataTables;

use App\AdminLog;
use App\JobTitle;
use App\Region;
use App\User;
use App\Notifications\UserApproved;
use App\Notifications\UserCreated;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.users.index');
    }

    public function create()
    {
        $jobTitles = JobTitle::orderBy('name')->get();
        $regions = Region::where('active', 1)->orderBy('order')->get();
        return view('admin.users.create', compact('jobTitles', 'regions'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'company' => 'required',
            //'job_title_id' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);

        $user = new User;
        $user->first_name   = $request->input('first_name');
        $user->last_name    = $request->input('last_name');
        $user->email        = $request->input('email');
        $user->company      = $request->input('company');
        //$user->job_title_id = $request->input('job_title_id');
        $user->code         = Str::random(30);
        $user->password     = bcrypt($request->input('password'));
        $user->approved_by  = \Auth::guard('admin')->user()->id;
        $user->approved_at  = date('Y-m-d H:i:s');
        $user->region_id    = \Auth::guard('admin')->user()->region_id;
        $user->save();

        $user->notify(new UserCreated($request->input('password')));

        //$user->sendEmailVerificationNotification();

        AdminLog::record('Created a user: ' . $user->first_name . ' ' . $user->last_name, ['user_id' => $user->id]);

        \Session::flash('success', 'A new user successfully created');

        return redirect()->route('admin.users.index');
    }

    public function edit($id)
    {
        $user = User::find($id);
        $jobTitles = JobTitle::orderBy('name')->get();
        $regions = Region::where('active', 1)->orderBy('order')->get();
        return view('admin.users.edit', compact('user', 'jobTitles', 'regions'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'company' => 'required',
            //'job_title_id' => 'required',
            'email' => 'required|unique:users,id,' . $id,
        ]);

        $user = User::find($id);
        $user->first_name   = $request->input('first_name');
        $user->last_name    = $request->input('last_name');
        $user->email        = $request->input('email');
        $user->company      = $request->input('company');
        //$user->job_title_id = $request->input('job_title_id');
        $user->save();

        AdminLog::record('Edited a user: ' . $user->first_name . ' ' . $user->last_name, ['user_id' => $user->id]);

        \Session::flash('success', 'A user successfully updated');

        return redirect()->route('admin.users.index');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        AdminLog::record('Deleted a user: ' . $user->first_name . ' ' . $user->last_name, ['user_id' => $user->id]);
        if ($user) $user->delete();

        return back()->with('success', 'A user successfully deleted');
    }

    public function approve($id)
    {
        $user = User::find($id);
        if ($user) {
            $user->approved_at = date('Y-m-d H:i:s');
            $user->approved_by = \Auth::user()->id;
            $user->save();
            $user->notify(new UserApproved());
        }
        AdminLog::record('Approved a user: ' . $user->first_name . ' ' . $user->last_name, ['user_id' => $user->id]);

        return back()->with('success', 'A user successfully approved');
    }

    public function datatable()
    {
        $admin = \Auth::guard('admin')->user();
        $users = User::with('approver')->where('region_id', $admin->region_id)->get();
        return DataTables::of($users)
            ->addColumn('name', function (User $user) {
                return $user->first_name . ' ' . $user->first_name . ' ' . $user->last_name;
            })
            ->addColumn('isself', function (User $user) {
                return $user->id == \Auth::user()->id;
            })
            ->make(true);
    }

}
