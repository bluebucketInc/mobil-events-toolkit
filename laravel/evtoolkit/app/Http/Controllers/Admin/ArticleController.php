<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DataTables;
use File;
use Image;

use App\Article;

class ArticleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.articles.index');
    }

    public function create()
    {
        return view('admin.articles.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);

        $article = new Article;

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $filename = str_random(20) . '.' . $ext;

            //create directory/folder if doesn't exist
            $path = public_path().'/uploads';
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
            $path = public_path().'/uploads/articles';
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

            $img = Image::make($file)->orientate();
            $img->fit(165, 110)->save(public_path('uploads/articles/t-' . $filename));
            $img->fit(750, 500)->save(public_path('uploads/articles/m-' . $filename));
            $img->fit(960, 640)->save(public_path('uploads/articles/l-' . $filename));

            $article->image_path = $filename;
        }

        $article->title          = $request->input('title');
        $article->excerpt        = $request->input('excerpt');
        $article->content        = $request->input('content');
        $article->youtube_id     = $request->input('youtube_id');
        $article->date_published = $request->input('date_published');
        $article->published      = $request->input('published');
        $article->save();

        $article->slug = str_slug($article->title, '-') . '-' . $article->id;
        $article->save();

        return redirect()->route('admin.articles.index');
    }

    public function edit($id)
    {
        $article = Article::find($id);

        return view('admin.articles.edit', compact('article'));
    }

    public function update(Request $request, $id)
    {
        $article = Article::find($id);

        if ($request->hasFile('image')) {

            //delete previous image files
            File::delete(public_path('uploads/articles/t-' . $article->image_path));
            File::delete(public_path('uploads/articles/m-' . $article->image_path));
            File::delete(public_path('uploads/articles/l-' . $article->image_path));

            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $filename = str_random(20) . '.' . $ext;

            //create directory/folder if doesn't exist
            $path = public_path().'/uploads';
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
            $path = public_path().'/uploads/articles';
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

            $img = Image::make($file)->orientate();
            $img->fit(165, 110)->save(public_path('uploads/articles/t-' . $filename));
            $img->fit(750, 500)->save(public_path('uploads/articles/m-' . $filename));
            $img->fit(960, 640)->save(public_path('uploads/articles/l-' . $filename));


            $article->image_path = $filename;
        }
 
        $article->title  = $request->input('title');
        $article->excerpt = $request->input('excerpt');
        $article->content = $request->input('content');
        $article->youtube_id = $request->input('youtube_id');
        $article->date_published = $request->input('date_published');
        $article->published      = $request->input('published');
        $article->save();

        return redirect()->route('admin.articles.index');
    }

    public function destroy($id)
    {
        $article = Article::find($id);
        $article->delete();
        
        return back();
    }

    public function datatable()
    {
        return DataTables::of(Article::all())
            ->make(true);
    }

}
