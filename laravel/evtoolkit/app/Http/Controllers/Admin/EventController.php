<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DataTables;
use File;
use Image;

use App\Event;

class EventController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.events.index');
    }

    public function show($id)
    {
        $event = Event::with(['sector', 'invitations' => function($q) {
            $q->orderBy('created_at', 'DESC');
        }])->where('id', $id)->first();
        return view('admin.events.show', compact('event'));
    }

    public function datatable()
    {
        $events = Event::with(['user', 'sector', 'invitations'])->get();
        return DataTables::of($events)
            ->addColumn('sector_name', function (Event $event) {
                return $event->sector->name;
            })
            ->addColumn('distributor_name', function (Event $event) {
                return $event->user->first_name . ' ' . $event->user->last_name;
            })
            ->addColumn('invitations_count', function (Event $event) {
                return $event->invitations->count();
            })
            ->make(true);
    }

}
