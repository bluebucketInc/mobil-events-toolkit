<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DataTables;
use App\Admin;
use App\AdminLog;

class LogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.logs.index');
    }

    public function datatable()
    {
        $logs = AdminLog::where('admin_id', \Auth::guard('admin')->user()->id)
            ->orderBy('created_at', 'desc')
            ->get();

        return Datatables::of($logs)
            ->editColumn('created_at', function ($activity) {
                return $activity->created_at->diffForHumans();//$track->registration->created_at->format('d M y H:i:s');
            })
            ->make(true);
    }

}
