<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JobTitle;

class UserController extends Controller
{

    public function profile()
    {
        $user = \Auth::user();
        $jobTitles = JobTitle::orderBy('name')->get();
        return view('user.profile', compact('user', 'jobTitles'));
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'company' => 'required',
        ]);

        $user = \Auth::user();
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->company = $request->input('company');
        $user->job_title_id = $request->input('job_title_id');
        $user->save();

        return redirect('/');
    }

    public function changePassword()
    {
        return view('user.change_password');
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'old_password' => 'required',
            'password' => 'required|min:8|confirmed|different:old_password',
        ]);

        $old_password = $request->input('old_password');
        $user = \Auth::user();
        if (\Hash::check($old_password, $user->password)) {
            $user->password = \Hash::make($request->input('password'));
            $user->save();
        }
        
        return redirect('/');
    }

}
