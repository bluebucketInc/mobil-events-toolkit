<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    public function category()
    {
        return $this->belongsTo('App\AssetCategory', 'category_id');
    }

    public function sector()
    {
        return $this->belongsTo('App\Sector');
    }

    public function assets()
    {
        return $this->belongsToMany('App\Package');
    }
}
