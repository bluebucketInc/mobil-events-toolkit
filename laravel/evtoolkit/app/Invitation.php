<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SendinBlue\Client\Model\CreateModel;
use Sendinblue;
use App\Customer;

class Invitation extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function email_template()
    {
        return $this->belongsTo('App\EmailTemplate');
    }

    public function sendinblue_sync()
    {

        $invitation_name = 'INV_' . $this->id;

        $contactsApi = new \SendinBlue\Client\Api\ContactsApi(null, Sendinblue::getConfiguration());
        $user = \Auth::user();

        $folderId = $user->sendinblue_folder_id; // int | id of the folder

        try {
            $result = $contactsApi->getFolder($folderId);
        } catch (\Exception $e) {

            $createFolder = new \SendinBlue\Client\Model\CreateUpdateFolder(); // \SendinBlue\Client\Model\CreateUpdateFolder | Name of the folder
            $createFolder->setName('UID_' . $user->id);

            try {
                $result = $contactsApi->createFolder($createFolder);
                $user->sendinblue_folder_id = $result->getId();
                $user->save();
            } catch (\Exception $e) {
                dd($e);
            }

        }

        // Create folder if not created yet
        if (!$user->sendinblue_folder_id) {
            $createFolder = new \SendinBlue\Client\Model\CreateUpdateFolder(); // \SendinBlue\Client\Model\CreateUpdateFolder | Name of the folder
            $createFolder->setName('UID_' . $user->id);

            try {
                $result = $contactsApi->createFolder($createFolder);
                $user->sendinblue_folder_id = $result->getId();
                $user->save();
            } catch (\Exception $e) {
                dd($e);
                //echo 'Exception when calling ContactsApi->createFolder: ', $e->getMessage(), PHP_EOL;
            }
        }

        // Check wheter sendinblue list has already created or not
        if (!$this->sendinblue_list_id) {            
            $createList = new \SendinBlue\Client\Model\CreateList(); // \SendinBlue\Client\Model\CreateList | Values to create a list
            $createList->setName($invitation_name);
            $createList->setFolderId($user->sendinblue_folder_id);

            try {
                $result = $contactsApi->createList($createList);
                $this->sendinblue_list_id = $result->getId();
                $this->save();
            } catch (\Exception $e) {
                dd($e);
                //echo 'Exception when calling ContactsApi->createList: ', $e->getMessage(), PHP_EOL;
            }
        }


        // Add contacts to list
        $listId = $this->sendinblue_list_id;
        $contactEmails = new \SendinBlue\Client\Model\AddContactToList(); // \SendinBlue\Client\Model\AddContactToList | Emails addresses of the contacts

        $customers = Customer::where('user_id', $user->id)->get();
        $emails = [];
        foreach ($customers as $customer) {
            $emails[] = $customer->email;
        }
        $contactEmails->setEmails($emails);

        try {
            $result = $contactsApi->addContactToList($listId, $contactEmails);
        } catch (Exception $e) {
            echo 'Exception when calling ContactsApi->addContactToList: ', $e->getMessage(), PHP_EOL;
        }


        // Create campaign
        $campaignApi = new \SendinBlue\Client\Api\EmailCampaignsApi(null, Sendinblue::getConfiguration());
        
        $sender = new \SendinBlue\Client\Model\CreateEmailCampaignSender();
        $sender->setName('Mobil Event Toolkit');
        $sender->setEmail('dika.mardika@rackdigital.com');
        $recipients = new \SendinBlue\Client\Model\CreateEmailCampaignRecipients();
        $recipients->setListIds([$listId]);
        $emailCampaigns = new \SendinBlue\Client\Model\CreateEmailCampaign(); // \SendinBlue\Client\Model\CreateEmailCampaign | Values to create a campaign
        $emailCampaigns->setReplyTo('noreply@rackdigital.com');
        $emailCampaigns->setName($invitation_name);
        $emailCampaigns->setHtmlContent('<html>Mobil Event Toolkit</html>');
        $emailCampaigns->setSender($sender);
        $emailCampaigns->setRecipients($recipients);
        $emailCampaigns->setSubject('You Are Invited');
        
        /*
        $emailCampaigns = new \SendinBlue\Client\Model\CreateEmailCampaign([
            'name' => $invitation_name,
            'from_name' => 'Mobil Event Toolkit',
            'from_email' => 'dika.mardika@rackdigital.com',
            'html_content' => '',
            'listid' => [ $listId ],
            'subject' => 'You Are Invited',
        ]);
        */
        
        try {
            $result = $campaignApi->createEmailCampaign($emailCampaigns);
            $this->sendinblue_campaign_id = $result->getId();
            $this->save();
        } catch (\Exception $e) {
            dd($e);
            echo 'Exception when calling EmailCampaignsApi->createEmailCampaign: ', $e->getMessage(), PHP_EOL;
        }
    }
}
