<?php

use Illuminate\Http\Request;

//Auth::routes();

Route::get('login', ['as' => 'auth.login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login', ['as' => 'auth.login', 'uses' => 'Auth\LoginController@login']);
Route::post('logout', ['as' => 'auth.logout', 'uses' => 'Auth\LoginController@logout']);

// Registration Routes...
//Route::get('register', ['as' => 'auth.register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
//Route::post('register', ['as' => 'auth.register', 'uses' => 'Auth\RegisterController@register']);

// Password Reset Routes...
Route::get('password/forgot', ['as' => 'auth.password.forgot', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
Route::post('password/email', ['as' => 'auth.password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
Route::get('password/reset/{token?}', 'Auth\ResetPasswordController@showResetForm')->name('auth.password.reset');
Route::post('password/reset', ['as' => 'auth.password.reset', 'uses' => 'Auth\ResetPasswordController@reset']);

Route::get('password/edit', 'PasswordController@edit')->name('password.edit');
Route::put('password/update', 'PasswordController@update')->name('password.update');

Route::get('/', 'DashboardController@index')->name('dashboard.index');

Route::get('customers/datatable', 'CustomerController@datatable')->name('customers.datatable');
Route::get('customers/distributors', 'CustomerController@distributors')->name('customers.distributors');
Route::resource('customers', 'CustomerController')->except([ 'create', 'edit', 'destroy' ]);

Route::resource('assets', 'AssetController')->except([ 'index', 'show' ]);
Route::get('assets/create/{sector_id?}/{category_id?}', 'AssetController@create')->name('assets.create');
Route::get('assets/browse/{sector_id?}/{category_id?}', 'AssetController@browse')->name('assets.browse');
Route::get('assets/{id}/load-content', 'AssetController@loadContent')->name('assets.loadcontent');

Route::get('assetCategories/datatable', 'AssetCategoryController@datatable')->name('assetcategories.datatable');
Route::resource('assetcategories', 'AssetCategoryController')->except([ 'show' ]);

Route::get('events/datatable', 'EventController@datatable')->name('events.datatable');
Route::resource('events', 'EventController')->only([ 'index', 'show' ]);

Route::get('invitations/datatable', 'InvitationController@datatable')->name('invitations.datatable');
Route::resource('invitations', 'InvitationController')->only([ 'index', 'show' ]);

Route::get('sectors/datatable', 'SectorController@datatable')->name('sectors.datatable');
Route::resource('sectors', 'SectorController')->except([ 'show' ]);

Route::get('regions/datatable', 'RegionController@datatable')->name('regions.datatable');
Route::resource('regions', 'RegionController')->except([ 'show' ]);

Route::get('events/datatable', 'EventController@datatable')->name('events.datatable');
Route::resource('events', 'EventController')->except([ 'create', 'edit', 'delete' ]);

Route::get('emailTemplates/datatable', 'EmailTemplateController@datatable')->name('emailtemplates.datatable');
Route::get('emailTemplates/syncFromSendinblue', 'EmailTemplateController@syncFromSendinblue')->name('emailTemplates.syncFromSendinblue');
Route::put('emailTemplates/{id}/activate', 'EmailTemplateController@activate')->name('emailTemplates.activate');
Route::put('emailTemplates/{id}/deactivate', 'EmailTemplateController@deactivate')->name('emailTemplates.deactivate');
Route::resource('emailTemplates', 'EmailTemplateController')->except([ 'create', 'store', 'show', 'destroy' ]);

Route::get('admins/roles', 'AdminController@roles')->name('admins.roles');
Route::get('admins/datatable', 'AdminController@datatable')->name('admins.datatable');
Route::resource('admins', 'AdminController')->except([ 'show' ]);

Route::get('logs', 'LogController@index')->name('logs.index');
Route::get('logs/datatable', 'LogController@datatable')->name('logs.datatable');

Route::get('users/datatable', 'UserController@datatable')->name('users.datatable');
Route::post('users/{id}/approve', 'UserController@approve')->name('users.approve');
Route::resource('users', 'UserController')->except([ 'show' ]);

Route::get('values/datatable', 'ValueController@datatable')->name('values.datatable');
Route::resource('values', 'ValueController')->except([ 'show' ]);
