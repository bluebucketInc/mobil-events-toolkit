@extends('admin.layouts.main')

@section('page-title', 'Region')

@section('breadcrumb')
<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active">Regions</li>
@endsection

@section('content')
<div class="box">
	<div class="box-header">
		<h3 class="box-title">Region List</h3>
        <div class="box-tools">
			<!--
            <div class="btn-group btn-group-sm">
                <a href="{{ route('admin.regions.create') }}" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Add Region</a>
			</div>
			-->
        </div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="dataTables_wrapper form-inline dt-bootstrap">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped" id="table-regionList">
				        <thead>
				            <tr>
								<th>Id</th>
								<th>Name</th>
								<th>Active</th>
				                <th>Action</th>
				            </tr>
				        </thead>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.box-body -->
</div>
@endsection

@push('scripts')
<script>

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-region-delete-' + id).submit();
	}
}

$(function() {
    $('#table-regionList').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.regions.datatable') !!}',
        columns: [
			{ data: 'id', name: 'id' },
			{ data: 'name', name: 'name' },
			{ data: 'active', name: 'active', render: function(data, type, row) {
					return row.active == 0 ? '<span class="label label-default">Inactive</span>' : '<span class="label label-success">Active</span>';
				}
			},
            { 
            	data: 'null', 
            	name: 'action', 
            	orderable: false, 
            	searchable: false,
            	render: function(data, type, row) {
            		var html = '';
            		html += '<a href="{{ route('admin.regions.edit', ['id' => ':id']) }}" class="btn btn-xs btn-default btn-flat"><i class="fa fa-pencil"></i> Edit</a>';
	                html += '<a href="#" class="btn btn-danger btn-flat btn-xs" onclick="event.preventDefault(); confirmDelete(:id)"><i class="fa fa-trash"></i> Delete</a>';
	                html += '{!! Form::open( [ 'method' => 'DELETE', 'route' => [ 'admin.regions.destroy', ':id' ], 'id' => 'form-region-delete-:id' ] ) !!}';
	                html += '{!! Form::close() !!}';
            		return html.replace(/:id/g, row.id);
            	}
        	}
        ]
    });
});
</script>
@endpush