@extends('admin.layouts.main')

@section('page-title', 'Events')
@section('breadcrumb')
<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="{{ route('admin.events.index') }}">Events</a></li>
<li class="active">Event Detail</li>
@endsection

@section('content')

<div class="box">
	<div class="box-body">
		<div class="btn-group pull-right">
			<a href="{{ route('admin.eventgallery.create', [$event->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add new gallery</a>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Invitations</h3>
			</div>
			<div class="box-body">
				<table class="table">
					<tr>
						<th>Created date</th>
						<th>Sent date</th>
						<th>Sents</th>
						<th>Opens</th>
						<th>Clicks</th>
					<tr>
					@foreach ($event->invitations as $invitation)
					<tr>
						<td>{{ date('j M y h:i A', strtotime($invitation->created_at)) }}</td>
						<td>{{ $invitation->sent_at ? date('j M y h:i A', strtotime($invitation->sent_at)) : '' }}</td>
						<td>{{ $invitation->stat_sents > 0 ? $invitation->stat_sents : '' }}</td>
						<td>{{ $invitation->stat_opens > 0 ? $invitation->stat_opens : ''  }}</td>
						<td>{{ $invitation->stat_clicks > 0 ? $invitation->stat_clicks : ''  }}</td>
					</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Event Information</h3>
			</div>
			<div class="box-body">
				<table class="table table-tight table-noborder">
					<tr>
						<td>Name</td>
						<td>:</td>
						<td>{{ $event->name }}</td>
					</tr>
					<tr>
						<td>Date</td>
						<td>:</td>
						<td>{{ $event->date }}</td>
					</tr>
					<tr>
						<td>Time</td>
						<td>:</td>
						<td>{{ $event->time }}</td>
					</tr>
					<tr>
						<td>Location</td>
						<td>:</td>
						<td>{{ $event->location }}</td>
					</tr>
					<tr>
						<td>Sector</td>
						<td>:</td>
						<td>{{ $event->sector->name }}</td>
					</tr>
				</table>
			</div> <!-- ./box-body -->
		</div>
	</div>
</div>



<h3 class="box-title">Event Photos</h3>
<div class="row asset-grid">
	@foreach ($galleries as $gallery)
		@if ($gallery->gallery_type == 2)
		<div class="col-md-3 col-sm-6">
			<div class="box asset-grid-item">
				<div class="box-head">
					@if ($gallery->format == 'image')
					<a href="{{ url('uploads/event_gallery/' . $gallery->filename) }}" class="magnific-popup">
						<img src="{{ asset('uploads/event_gallery/400x200-' . $gallery->preview_filename) }}" class="img-responsive">
					</a>
					@elseif ($gallery->format == 'video')
					<a href="#" data-source="{{ $gallery->filename }}" class="preview-video">
						<img src="{{ asset('assets/images/video-icon.jpg') }}" class="img-responsive">
					</a>
					@elseif ($gallery->format == 'iframe')
					<a href="{{ url('uploads/event_gallery/' . $gallery->filename) }}" class="magnific-iframe">
						<img src="{{ asset('assets/images/pdf-icon.jpg') }}" class="img-responsive">
					</a>
					@endif
				</div>
				<div class="box-footer">
					<h5>{{ $gallery->name }}</h5>
					<div class="btn-group pull-right">
						<a href="{{ route('admin.eventgallery.edit', ['id' => $gallery->id ]) }}" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i> Edit</a>
						<a href="#" class="btn btn-danger btn-xs" onclick="event.preventDefault(); confirmDelete({{ $gallery->id }})"><i class="fa fa-trash"></i> Delete</a>
						{!! Form::open( [ 'method' => 'DELETE', 'route' => [ 'admin.assets.destroy', $gallery->id ], 'id' => 'form-asset-delete-' . $gallery->id ] ) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
		@endif
	@endforeach
</div>

@include('modal/video_content')


@push('styles')
<style>
a.magnific-popup {
	cursor: zoom-in;
}
</style>
@endpush

@push('scripts')
<script>

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-asset-delete-' + id).submit();
	}
}

$(function() {
	$('input[type="checkbox"].minimal').iCheck({
		checkboxClass: 'icheckbox_minimal-blue'
	});

})

$('.magnific-popup').magnificPopup({type:'image'});
$('.preview-video').click(function(e) {
	e.preventDefault();
	var source = "{{ asset('uploads/event_gallery') }}/" + $(this).data('source');
	$('#modal-video-content').modal();
	$('#video-container').get(0).pause();
    $('#video-source').attr('src', source);
    $('#video-container').get(0).load();
});
$('.magnific-popup-player').magnificPopup({
	items: {
		//src: '<video class="white-popup" src="http://localhost/extoolkit/public/uploads/event_assets/KyxFUW7amq953NFxkAgW.mp4"></video>',
		src: '<video id="video-container" controls><source id="video-source" src="/extoolkit/public/uploads/event_gallery/KyxFUW7amq953NFxkAgW.mp4" type="video/mp4"> Your browser does not support the video tag.</video>',
		type: 'inline'
	}
});
$('.magnific-iframe').magnificPopup({
	type: 'iframe'
});
$('.magnific-video').magnificPopup({
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: false,
    iframe: {
        markup: '<div class="mfp-iframe-scaler">'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
              '</div>',

        srcAction: 'iframe_src',
        }
});


</script>
@endpush

@endsection
