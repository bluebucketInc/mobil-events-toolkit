@extends('admin.layouts.main')

@section('page-title', 'Customers')

@section('breadcrumb')
<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active">Customers</li>
@endsection

@section('content')
<div class="row">
	<!--
	<div class="col-md-3">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Filter</h3>
			</div>
			<div class="box-body">
				<form role="form">
					<div class="form-group">
						<label>Distributor</label>
						<select id="distributor" class="form-control">
							<option value=""></option>
							@foreach ($users as $user)
							<option value="{{ $user->id }}">{{ $user->first_name . ' ' . $user->last_name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label>Sector</label>
					<select id="sector" class="form-control">
							<option value=""></option>
							@foreach ($sectors as $sector)
							<option value="{{ $sector->id }}">{{ $sector->name }}</option>
							@endforeach
						</select>
					</div>
				</form>
			</div>
		</div>
	</div>
	-->
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Customer List</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="dataTables_wrapper form-inline dt-bootstrap">
					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped" id="table-customerList">
								<thead>
									<tr>
										<th>Id</th>
										<th>Name</th>
										<th>Email</th>
										<th>Company</th>
										<th>Sector</th>
										<th>Distributor</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
	</div>
</div>
@endsection

@push('scripts')
<script>

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-customer-delete-' + id).submit();
	}
}

$(function() {
    $('#table-customerList').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.customers.datatable') !!}',
        columns: [
			{ data: 'id', name: 'id' },
			{ data: 'first_name', name: 'first_name', orderable: true, searchable: true, render: function(data, type, row) {
					return row.first_name + ' ' + row.last_name;
				} 
			},
			{ data: 'email', name: 'email' },
			{ data: 'company', name: 'company' },
			{ data: 'sector.name', name: 'sector' },
			{ data: 'null', name: 'distributor', render: function(data, type, row) {
					return row.user ? row.user.first_name : '';
				}
			},
            { 
            	data: 'null', 
            	name: 'action', 
            	orderable: false, 
            	searchable: false,
            	render: function(data, type, row) {
            		var html = '';
            		html += '<a href="{{ route('admin.customers.show', ['id' => ':id']) }}" class="btn btn-xs btn-default btn-flat"><i class="fa fa-eye"></i> View</a>';
					@if (\Auth::guard('admin')->user()->role == 'Super Admin')
					html += '<a href="#" class="btn btn-danger btn-flat btn-xs" onclick="event.preventDefault(); confirmDelete(:id)"><i class="fa fa-trash"></i> Delete</a>';
					html += '{!! Form::open( [ 'method' => 'DELETE', 'route' => [ 'admin.customers.destroy', ':id' ], 'id' => 'form-customer-delete-:id' ] ) !!}';
					html += '{!! Form::close() !!}';
					@endif
            		return html.replace(/:id/g, row.id);
            	}
        	}
        ]
    });

	$('#distributor').select2({
		placeholder: 'All distributors',
		allowClear: true,
	});

	$('#sector').select2({
		placeholder: 'All sectors',
		allowClear: true,
	});

	$('#distributor').on('change', function(e) {
		console.log(this.value);
	});

	$('#sector').on('change', function(e) {
		console.log(this.value);
	});

});
</script>
@endpush