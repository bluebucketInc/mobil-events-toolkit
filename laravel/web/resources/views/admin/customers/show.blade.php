@extends('admin.layouts.main')

@section('page-title', 'Customers')

@section('content')

<div class="row">
	<div class="col-md-6">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Show Customer</h3>
			</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('first_name') !!}
								{!! Form::text('first_name', $customer->first_name, ['class' => 'form-control', 'maxlength' => '50', 'readonly']) !!}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('last_name') !!}
								{!! Form::text('last_name', $customer->last_name, ['class' => 'form-control', 'maxlength' => '191', 'readonly']) !!}
							</div>
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('email') !!}
						{!! Form::email('email', $customer->email, ['class' => 'form-control', 'maxlength' => '191', 'readonly']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('company') !!}
						{!! Form::text('company', $customer->company, ['class' => 'form-control', 'maxlength' => '100', 'readonly']) !!}
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="sector_id">Sector</label>
								<select class="form-control select2" id="sector_id" name="sector_id">
									@foreach ($sectors as $sector)
									<option value="{{ $sector->id }}" {{ $sector->id == $customer->sector_id ? 'selected' : '' }}>{{ $sector->name }}</option>
									@endforeach
								</select>
							</div>

						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="job_title">Job title</label>
								<select class="form-control select2" id="job_title" name="job_title">
									@foreach ($jobTitles as $jobTitle)
									<option value="{{ $jobTitle->id }}" {{ $jobTitle->id == $customer->job_title_id ? 'selected' : '' }}>{{ $jobTitle->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
									<div class="checkbox">
											<label><input {{ $customer->check_receive == 1? 'checked':'' }} name="check_receive" type="checkbox" value="1"> I agree to receiving periodic communications and updates about Mobil products and services from ExxonMobil</label>
									</div>
							</div>
						</div>
					</div>

				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<a href="{{ route('admin.customers.index') }}" class="btn btn-default">Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
