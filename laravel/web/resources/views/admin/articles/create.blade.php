@extends('admin.layouts.main')

@section('page-title', 'Articles')

@section('content')

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Create New Article</h3>
	</div>
	{!! Form::open(['route' => 'admin.articles.store', 'files' => true]) !!}
		<div class="box-body">
			<div class="row">
				<div class="col-md-3 form-group">
					{!! Form::label('published') !!}
					{!! Form::select('published', [ 0 => 'No', 1 => 'Yes' ], 0, ['class' => 'form-control']) !!}
				</div>
				<div class="col-md-3 form-group">
					{!! Form::label('date_published') !!}
					{!! Form::date('date_published', \Carbon\Carbon::now(), ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('title') !!}
				{!! Form::text('title', '', ['class' => 'form-control', 'maxlength' => '60']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('excerpt') !!}
				{!! Form::textarea('excerpt', '', ['class' => 'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('content') !!}
				{!! Form::textarea('content', '', ['class' => 'form-control tinymce']) !!}
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('youtube_id') !!}
						{!! Form::text('youtube_id', '', ['class' => 'form-control', 'maxlength' => '20']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('image_preview_filename') !!}
						{!! Form::file('image_preview_filename', ['class' => 'form-control', 'accept' => 'image/x-png,image/gif,image/jpeg']) !!}
					</div>
				</div>
			</div>
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
			<a href="{{ route('admin.articles.index') }}" class="btn btn-default">Cancel</a>
		</div>
	{!! Form::close() !!}
</div>

@endsection
