@extends('admin.layouts.main')

@section('page-title', 'Dashboard')

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Dashboard</li>
@endsection

@section('content')

<div class="row">

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-users"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Administrators</span>
                <span class="info-box-number">{{ $admins->count() }}</span>
                <span class="info-see-details"><a href="{{ route('admin.admins.index') }}">See details</a></span>
            </div>
            <!-- /.info-box-content -->
        </div>        
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    @if (\Session::get('region_id', \Auth::guard('admin')->user()->region_id) != 1)
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Distributors</span>
                <span class="info-box-number">{{ $users->count() }}</span>
                <span class="info-see-details"><a href="{{ route('admin.users.index') }}">See details</a></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div> <!-- /.col -->
    

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-address-card"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Customers</span>
                <span class="info-box-number">{{ $customers->count() }}</span>
                <span class="info-see-details"><a href="{{ route('admin.customers.index') }}">See details</a></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div> <!-- /.col -->
    @endif

</div> <!-- /.row -->

<div class="row">

    <!--
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-calendar"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Events</span>
                <span class="info-box-number">34</span>
                <span class="info-see-details"><a href="#">See details</a></span>
            </div>
        </div>        
    </div>
    -->

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-file-o"></i></span>    
            <div class="info-box-content">
                <span class="info-box-text">Assets</span>
                <span class="info-box-number">{{ $assets->count() }}</span>
                <span class="info-see-details"><a href="{{ route('admin.assets.browse') }}">See details</a></span>
            </div>          
            <!-- /.info-box-content -->   
        </div>
        <!-- /.info-box -->
    </div> 
    <!-- /.col -->

</div>
<!-- /.row -->

@endsection