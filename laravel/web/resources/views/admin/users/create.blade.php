@extends('admin.layouts.main')

@section('page-title', 'Distributors')

@section('content')

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Create New Distributor</h3>
	</div>
	{!! Form::open(['route' => 'admin.users.store']) !!}
		<div class="box-body">
			<div class="row">
				<div class="form-group col-md-6 {{ $errors->has('first_name') ? 'has-error' : '' }}">
					{!! Form::label('first_name') !!}
					{!! Form::text('first_name', old('first_name'), ['class' => 'form-control', 'maxlength' => '50']) !!}
					<span class="help-block">{{ $errors->first('first_name', '') }}</span>
				</div>
				<div class="form-group col-md-6 {{ $errors->has('last_name') ? 'has-error' : '' }}">
					{!! Form::label('last_name') !!}
					{!! Form::text('last_name', old('last_name'), ['class' => 'form-control', 'maxlength' => '50']) !!}
					<span class="help-block">{{ $errors->first('last_name', '') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-6 {{ $errors->has('company') ? 'has-error' : '' }}">
					{!! Form::label('company') !!}
					{!! Form::text('company', old('company'), ['class' => 'form-control', 'maxlength' => '50']) !!}
					<span class="help-block">{{ $errors->first('company', '') }}</span>
				</div>
				<!--
				<div class="form-group col-md-6 {{ $errors->has('job_title_id') ? 'has-error' : '' }}">
					<label for="job_title_id">Job title</label>
					<select class="form-control" data-placeholder="Choose sector" id="job_title" name="job_title_id">
						@foreach ($jobTitles as $jobTitle)
						<option value="{{ $jobTitle->id }}">{{ $jobTitle->name }}</option>
						@endforeach
					</select>
					<span class="help-block">{{ $errors->first('job_title', '') }}</span>
				</div>
				-->
			</div>
			<div class="form-group {{ $errors->has('region_id') ? 'has-error' : '' }}">
				<label for="region_id">Region</label>
				<select class="form-control" id="region_id" name="region_id">
					@foreach ($regions as $region)
						<option value="{{ $region->id }}" {{ $region->id == 0 ? 'selected' : '' }}>{{ $region->name }}</option>
					@endforeach
				</select>
				<span class="help-block">{{ $errors->first('region_id', '') }}</span>
			</div>
			<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
				{!! Form::label('email') !!}
				{!! Form::email('email', '', ['class' => 'form-control', 'maxlength' => '191']) !!}
				<span class="help-block">{{ $errors->first('email', '') }}</span>
			</div>
			<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
				{!! Form::label('password') !!}
				{!! Form::password('password', ['class' => 'form-control', 'maxlength' => '191']) !!}
				<span class="help-block">{{ $errors->first('password', '') }}</span>
			</div>
			<div class="form-group">
				{!! Form::label('password_confirmation') !!}
				{!! Form::password('password_confirmation', ['class' => 'form-control', 'maxlength' => '191']) !!}
			</div>
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
			<a href="{{ route('admin.users.index') }}" class="btn btn-default">Cancel</a>
		</div>
	{!! Form::close() !!}
</div>

@endsection
