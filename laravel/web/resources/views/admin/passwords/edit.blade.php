@extends('admin.layouts.main')

@section('page-title', 'Change Password')

@section('content')
	
<div class="row">
	<div class="col-md-4">

		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Change my password</h3>
			</div>
		
			<div class="box-body">
				
				{!! Form::open(['route' => 'admin.password.update', 'method' => 'PUT']) !!}
				<div class="form-group has-feedback">
				<div class="form-group has-feedback">
					{!! Form::password('old_password', [ 'class' => 'form-control', 'placeholder' => 'Current Password', 'maxlength' => '60' ]) !!}
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					{!! Form::password('new_password', [ 'class' => 'form-control', 'placeholder' => 'New Password', 'maxlength' => '60' ]) !!}
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					{!! Form::password('confirm_password', [ 'class' => 'form-control', 'placeholder' => 'Confirm Password', 'maxlength' => '60' ]) !!}
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-4">
						<a href="{{ url('admin') }}" class="btn btn-default btn-block btn-flat">Cancel</a>
					</div>
		
					<div class="col-xs-4"></div>
		
					<div class="col-xs-4">
						{!! Form::submit('Change', [ 'class' => 'btn btn-primary btn-block btn-flat' ]) !!}	
					</div>
				</div>
			{!! Form::close() !!}
		
			</div>
		</div>
	</div>

</div>

@endsection