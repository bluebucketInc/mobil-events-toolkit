@extends('admin.layouts.main')

@section('page-title', 'Pages')

@section('content')

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Create New Page</h3>
	</div>
	{!! Form::open(['route' => 'admin.pages.store', 'files' => true]) !!}
		<div class="box-body">
			<div class="row">
				<div class="col-md-3 form-group">
					{!! Form::label('published') !!}
					{!! Form::select('published', [ 0 => 'No', 1 => 'Yes' ], 0, ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('slug') !!}
				{!! Form::text('slug', '', ['class' => 'form-control', 'maxlength' => '100']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('title') !!}
				{!! Form::text('title', '', ['class' => 'form-control', 'maxlength' => '50']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('description') !!}
				{!! Form::textarea('description', '', ['class' => 'form-control', 'maxlength' => '320']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('content') !!}
				{!! Form::textarea('content', '', ['class' => 'form-control tinymce']) !!}
			</div>
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
			<a href="{{ route('admin.pages.index') }}" class="btn btn-default">Cancel</a>
		</div>
	{!! Form::close() !!}
</div>

@endsection
