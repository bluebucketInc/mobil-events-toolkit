@extends('admin.layouts.main')

@section('page-title', 'Admins')

@section('content')

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Create New Admin</h3>
	</div>
	{!! Form::open(['route' => 'admin.admins.store']) !!}
		<div class="box-body">
			<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
				{!! Form::label('name') !!}
				{!! Form::text('name', '', ['class' => 'form-control', 'maxlength' => '50']) !!}
				<span class="help-block">{{ $errors->first('name', '') }}</span>
			</div>
			<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
				{!! Form::label('email') !!}
				{!! Form::email('email', '', ['class' => 'form-control', 'maxlength' => '191']) !!}
				<span class="help-block">{{ $errors->first('email', '') }}</span>
			</div>
			<div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">
				{!! Form::label('role') !!}
				{!! Form::select('role', ['Super Admin' => 'Super Admin', 'Normal Admin' => 'Normal Admin'], 'Normal Admin', ['class' => 'form-control']) !!}
				<span class="help-block">{{ $errors->first('role', '') }}</span>
			</div>
			<div class="form-group {{ $errors->has('region_id') ? 'has-error' : '' }}">
				<label for="region_id">Region</label>
				<select class="form-control" id="region_id" name="region_id">
					@foreach ($regions as $region)
						<option value="{{ $region->id }}" {{ $region->id == 0 ? 'selected' : '' }}>{{ $region->name }}</option>
					@endforeach
				</select>
				<span class="help-block">{{ $errors->first('region_id', '') }}</span>
			</div>
			<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
				{!! Form::label('password') !!}
				{!! Form::password('password', ['class' => 'form-control', 'maxlength' => '191']) !!}
				<span class="help-block">{{ $errors->first('password', '') }}</span>
			</div>
			<div class="form-group">
				{!! Form::label('password_confirmation') !!}
				{!! Form::password('password_confirmation', ['class' => 'form-control', 'maxlength' => '191']) !!}
			</div>
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
			<a href="{{ route('admin.admins.index') }}" class="btn btn-default">Cancel</a>
		</div>
	{!! Form::close() !!}
</div>

@endsection
