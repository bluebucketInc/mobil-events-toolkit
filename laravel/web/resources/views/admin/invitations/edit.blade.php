@extends('admin.layouts.main')

@section('page-title', 'Contacts')

@section('content')

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Edit Contact</h3>
	</div>
	{!! Form::open(['method' => 'PUT', 'route' => ['admin.contacts.update', $contact->id], 'files' => true]) !!}
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('email') !!}
						{!! Form::email('email', $contact->email, ['class' => 'form-control', 'maxlength' => '191']) !!}
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('first_name') !!}
								{!! Form::text('first_name', $contact->first_name, ['class' => 'form-control', 'maxlength' => '50']) !!}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('last_name') !!}
								{!! Form::text('last_name', $contact->last_name, ['class' => 'form-control', 'maxlength' => '191']) !!}
							</div>
						</div>
					</div>				
				</div>
				<!-- /.col-md-6 -->
				<div class="col-md-6"></div>
			</div>
			
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
			<a href="{{ route('admin.contacts.index') }}" class="btn btn-default">Cancel</a>
		</div>
	{!! Form::close() !!}
</div>

@endsection