@extends('admin.layouts.main')

@section('page-title', 'Event Gallery')

@section('breadcrumb')
<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="{{ route('admin.eventgallery.list') }}">Event Galleries</a></li>
<li class="active">Add</li>
@endsection

@section('content')

<div class="row">
	<div class="col-md-6">

		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Add New Event Gallery</h3>
			</div> <!-- /.box-header -->
			{!! Form::open(['route' => 'admin.eventgallery.store', 'files' => true]) !!}
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group {{ $errors->has('sector_id') ? 'has-error' : '' }}">
							<label for="sector_id">Events</label>
							<select class="form-control select2" data-placeholder="Choose event" id="event_id" name="event_id" onchange="selectEvent(this);">
								<option value=""></option>
								@foreach ($events as $event)
									<option event-date="{{$event->date." ".$event->time}}" value="{{ $event->id }}" {{ $event->id == $event_id ? 'selected' : '' }}>{{ $event->name }}</option>
								@endforeach
							</select>
							<span class="help-block">{{ $errors->first('$event_id', '') }}</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group {{ $errors->has('sector_id') ? 'has-error' : '' }}">
							<label for="sector_id">Events Date</label>

							{!! Form::text('gallerytype_id', $event_date, ['id'=>'eventDateId', 'class' => 'form-control', 'disabled'=>'disabled']) !!}

						</div>
					</div>
				</div>

				{!! Form::hidden('gallerytype_id', '2', ['class' => 'form-control']) !!}
			  {!! Form::hidden('name', '', ['class' => 'form-control', 'maxlength' => '50', 'type'=>'hidden']) !!}
				<div class="form-group {{ $errors->has('asset') ? 'has-error' : '' }}">
					{!! Form::label('asset', 'Gallery file') !!}
					{!! Form::file('asset[]', ['class' => 'form-control', 'multiple', 'accept' => 'image/x-png,image/gif,image/jpeg,image/tif,image/tiff,video/mp4,application/zip,application/pdf,application/vnd.openxmlformats-officedocument.presentationml.presentation']) !!}
					<span class="help-block">{{ $errors->first('asset', '') }}</span>
				</div>
			</div> <!-- /.box-body -->

			<div class="box-footer">
				{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
				<a href="{{ route('admin.eventgallery.list', [$event_id]) }}" class="btn btn-default">Cancel</a>
			</div>
			{!! Form::close() !!}
		</div>
	</div> <!-- /.col-md-6 -->

</div>

@endsection

@push('scripts')
<script>
	$('.select2').select2({allowClear: true});

	function selectEvent(sel) {
		option = $('option:selected', sel).attr('event-date');
		$("#eventDateId").val(option);

	}
</script>
@endpush
