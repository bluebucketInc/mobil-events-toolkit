@extends('admin.layouts.main')

@section('page-title', 'Event Gallery')

@section('breadcrumb')
<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active">Event Galleries</li>
@endsection

@section('content')

<div class="col-md-12">

	<div class="box">
		<div class="box-header with-border">
      <div class="btn-group pull-right">
  			<a href="{{ route('admin.eventgallery.create', []) }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add new gallery</a>
  		</div>
		</div>

			<div class="box-body">
        <div class="row asset-grid">
          @if (count($tmpEvents) == 0)
          <h2>Please create your first event.</2>
          @endif
        	@foreach ($tmpEvents as $event)
        		<div class="col-md-3 col-sm-6">
        			<div class="box asset-grid-item">
        				<div class="box-head">
                  <a href="{{ route('admin.eventgallery.details', ['id' => $event->id ])}}">
                    @if($event->ext == "image")
                    <img src="{{ asset('uploads/event_gallery/400x200-' . $event->file_name) }}" class="img-responsive">
                    @elseif($event->ext == "video")
                      <img alt="{{$event->name}}" src="{{ asset('assets/images/video-icon.jpg') }}" class="img-responsive">
                    @elseif($event->ext == "pdf")
                      <img alt="{{$event->name}}" src="{{ asset('assets/images/pdf-icon.jpg') }}" class="img-responsive">
                    @else
                      <img alt="{{$event->name}}" src="{{ asset('assets/images/placeholder-image.jpg') }}" class="img-responsive">
                    @endif
                  </a>
        				</div>
        				<div class="box-footer">
        					<a href="{{ route('admin.eventgallery.details', ['id' => $event->id ]) }}">{{ $event->name}}</a>
                  <br/>{{$event->date}}
                  <h5>{{$event->galleries->count()}} Galleries</h5>

        				</div>
        			</div>
        		</div>
        	@endforeach
        </div>


			</div> <!-- /.box-body -->

	</div> <!-- /.box -->
</div> <!-- /.col-md-6 -->



@endsection

@push('scripts')
<script>
	$('.select2').select2();
	$('.magnific-iframe').magnificPopup({
		type: 'iframe'
	});
</script>
@endpush
