@extends('admin.layouts.main')

@section('page-title', 'Assets')

@section('breadcrumb')
<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="{{ route('admin.assets.browse', [ ]) }}">Event Galleries</a></li>
<li><a href="{{ route('admin.assets.browse', [ ]) }}">Event Detail</a></li>
<li class="active">Edit</li>
@endsection

@section('content')

<div class="col-md-6">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Edit Asset</h3>
		</div>
		{!! Form::open(['method' => 'PUT', 'route' => ['admin.eventgallery.update', $gallery->id], 'files' => true]) !!}
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
            <div class="form-group {{ $errors->has('sector_id') ? 'has-error' : '' }}">
							<label for="sector_id">Events</label>
							<select class="form-control select2" data-placeholder="Choose event" id="event_id" name="event_id">
								<option value=""></option>
								@foreach ($events as $event)
									<option value="{{ $event->id }}" {{ $event->id == $gallery->event_id ? 'selected' : '' }}>{{ $event->name }}</option>
								@endforeach
							</select>
							<span class="help-block">{{ $errors->first('$event_id', '') }}</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
            <div class="form-group {{ $errors->has('sector_id') ? 'has-error' : '' }}">
							<label for="sector_id">Gallery Type</label>
							<select class="form-control select2" data-placeholder="Choose event" id="gallerytype_id" name="gallerytype_id">
								@if ($gallery->gallery_type == 1)
								<option value="1">Events Set-up Assets</option>
								<option value="2">Event Photos</option>
								@else
								<option value="1">Events Set-up Assets</option>
								<option value="2" selected>Event Photos</option>
								@endif
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
							{!! Form::label('name') !!}
							{!! Form::text('name', $gallery->name, ['class' => 'form-control', 'maxlength' => '50']) !!}
							<span class="help-block">{{ $errors->first('name', '') }}</span>
						</div>
					</div>
				</div>

        <div class="form-group {{ $errors->has('asset') ? 'has-error' : '' }}">
					{!! Form::label('asset', 'Gallery file') !!}
					{!! Form::file('asset', ['class' => 'form-control', 'accept' => 'image/x-png,image/gif,image/jpeg,image/tif,image/tiff,video/mp4,application/zip,application/pdf,application/vnd.openxmlformats-officedocument.presentationml.presentation']) !!}
					<span class="help-block">{{ $errors->first('asset', '') }}</span>
				</div>
				<div class="form-group {{ $errors->has('preview') ? 'has-error' : '' }}">
					{!! Form::label('preview', 'Please upload a screenshot of your document for thumbnail preview.') !!}
					{!! Form::file('preview', ['class' => 'form-control', 'accept' => 'image/x-png,image/gif,image/jpeg']) !!}
					<span class="help-block">{{ $errors->first('preview', '') }}</span>
				</div>
			</div> <!-- /.box-body -->

			<div class="box-footer">
				{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
				<a href="{{ route('admin.eventgallery.list', [ 'event_id' => $gallery->event_id] ) }}" class="btn btn-default">Cancel</a>
			</div>
		{!! Form::close() !!}
	</div> <!-- /.box -->
</div> <!-- /.col-md-6 -->

<div class="col-md-6">
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">Gallery Media</h3>
		</div>
		<div class="box-body">
			@if ($gallery->file_extension == 'mp4')
			<video width="320" height="240" controls>
				<source src="{{ url('uploads/event_gallery/' . $gallery->filename) }}" type="video/mp4">
				Your browser does not support the video tag.
			</video>
			@elseif ($gallery->file_extension == 'pdf')
			<a href="{{ url('uploads/event_gallery/' . $gallery->filename) }}" class="magnific-iframe">
				<img src="{{ asset('uploads/event_gallery/400x400-' . $gallery->preview_filename) }}" class="img-responsive">
			</a>
			@else
			<img src="{{ asset('uploads/event_gallery/' . $gallery->filename) }}" class="img-responsive">
			@endif
		</div>
	</div> <!-- /.box -->
</div>

@endsection

@push('scripts')
<script>
	$('.select2').select2();
	$('.magnific-iframe').magnificPopup({
		type: 'iframe'
	});
</script>
@endpush
