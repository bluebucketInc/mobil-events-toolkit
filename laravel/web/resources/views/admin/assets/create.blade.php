@extends('admin.layouts.main')

@section('page-title', 'Assets')

@section('breadcrumb')
<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="{{ route('admin.assets.browse') }}">Assets</a></li>
<li class="active">Add</li>
@endsection

@section('content')

<div class="row">
	<div class="col-md-6">

		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Add New Asset</h3>
			</div> <!-- /.box-header -->
			{!! Form::open(['route' => 'admin.assets.store', 'files' => true]) !!}
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('sector_id') ? 'has-error' : '' }}">
							<label for="sector_id">Sector</label>
							<select class="form-control select2" data-placeholder="Choose sector" id="sector_id" name="sector_id">
								<option value=""></option>
								@foreach ($sectors as $sector)
									<option value="{{ $sector->id }}" {{ $sector->id == $sector_id ? 'selected' : '' }}>{{ $sector->name }}</option>
								@endforeach
							</select>
							<span class="help-block">{{ $errors->first('sector_id', '') }}</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
							<label for="category_id">Category</label>
							<select class="form-control select2" data-placeholder="Choose category" id="category_id" name="category_id">
								<option value=""></option>
								@foreach ($categories as $category)
									<option value="{{ $category->id }}" {{ $category->id == $category_id ? 'selected' : '' }}>{{ $category->name }}</option>
								@endforeach
							</select>
							<span class="help-block">{{ $errors->first('category_id', '') }}</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
							{!! Form::label('name') !!}
							{!! Form::text('name', '', ['class' => 'form-control', 'maxlength' => '50']) !!}
							<span class="help-block">{{ $errors->first('name', '') }}</span>
						</div>
					</div>
				</div>
				<div class="form-group {{ $errors->has('asset') ? 'has-error' : '' }}">
					{!! Form::label('asset', 'Asset file') !!}
					{!! Form::file('asset', ['class' => 'form-control', 'accept' => 'image/x-png,image/gif,image/jpeg,image/tif,image/tiff,video/mp4,application/zip,application/pdf,application/vnd.openxmlformats-officedocument.presentationml.presentation']) !!}
					<span class="help-block">{{ $errors->first('asset', '') }}</span>
				</div>
				<div class="form-group {{ $errors->has('preview') ? 'has-error' : '' }}">
					{!! Form::label('preview', 'Please upload a screenshot of your document for thumbnail preview.') !!}
					{!! Form::file('preview', ['class' => 'form-control', 'accept' => 'image/x-png,image/gif,image/jpeg']) !!}
					<span class="help-block">{{ $errors->first('preview', '') }}</span>
				</div>
			</div> <!-- /.box-body -->

			<div class="box-footer">
				{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
				<a href="{{ route('admin.assets.browse') }}" class="btn btn-default">Cancel</a>
			</div>
			{!! Form::close() !!}
		</div>
	</div> <!-- /.col-md-6 -->
	
</div>

@endsection

@push('scripts')
<script>
	$('.select2').select2({allowClear: true});
</script>
@endpush