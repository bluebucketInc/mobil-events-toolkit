@extends('admin.layouts.main')

@section('content')

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Edit Value</h3>
	</div>
	{!! Form::open(['method' => 'PUT', 'route' => ['admin.values.update', $value->id]]) !!}
		<div class="box-body">
			<div class="form-group">
				{!! Form::label('name') !!}
				{!! Form::text('name', $value->name, ['class' => 'form-control', 'maxlength' => '50']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('value') !!}
				{!! Form::textarea('value', $value->value, ['class' => 'form-control', 'maxlength' => '191']) !!}
			</div>
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
			<a href="{{ route('admin.values.index') }}" class="btn btn-default">Cancel</a>
		</div>
	{!! Form::close() !!}
</div>

@endsection