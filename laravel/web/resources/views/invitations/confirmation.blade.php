@extends('layouts/main')

@section('content')
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li><a href="{{ route('events') }}">Events</a></li>
            <li><a href="{{ route('events.show', $invitation->event->id) }}">{{ $invitation->event->name }}</a></li>
            <li class="active">Invitation - Confirmation</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-mobil">
                    <div class="panel-heading">
                        <h3 class="panel-title">Confirm Before Sending</h3>
                    </div>
                    <div class="panel-body">
                        <p>Please review your email to confirm that all input fields are accurate before your confirmation to send.</p>
                    </div>
                    <div class="panel-footer">
                        <div class="pull-right">
                            <a href="#" id="test" data-toggle="modal" data-target="#modal-test-email" class="btn btn-secondary">Send test email</a>
                            <a href="#" id="send" class="btn btn-primary">Send</a>
                        </div>
                        <div class="pull-left">
                            <a href="{{ route('invitations.customize', $invitation->id) }}" class="btn btn-default">Back</a>
                            <a href="{{ route('customers', ['fc' => 'a', 'fs' => $invitation->event->sector_id ]) }}" class="btn btn-default" target="_blank">Customer List</a>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                        <ul>
                            <li>Click <b>Customer List</b> to view and edit recipients in new tab.</li>
                            <!--<li>Please note: To send test email, you have to register your email address under customer list of the same sector</li>-->
                            <li>Please note that your sent confirmation will be logged in your main events tab upon clicking send</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="panel panel-mobil">
                    <div class="panel-heading">
                        <p><b>From:</b> {{ config('mail.from.name') }} &lt;{{ config('mail.from.address') }} &gt;<br><b>Subject:</b> {{ $invitation->email_template->subject }}</p>
                    </div>
                    <div class="edm-html-wrapper">
                        {!! $invitation->html_content !!}
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /.container -->
</section>
@endsection

@include('modal.test_email')

@push('scripts')
<script>


    $('#send').click(function(e) {
        e.preventDefault();

        var sendUrl = "{{ route('ajax.invitations.send', $invitation->id) }}";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            type: 'POST',
            url: sendUrl,
            datatype: 'json',
            success: function (data) { 
                window.location = "{{ route('events.show', $invitation->event_id) }}";
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    });
    
     $('#test').click(function(e) {
        e.preventDefault();

        var saveUrl = "{{ route('ajax.invitations.test', $invitation->id) }}";
        var saveData = {
            text_block_1: $('#text_block_1').val()
        };

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            type: 'POST',
            url: saveUrl,
            data: saveData,
            datatype: 'json',
            success: function (data) { 
                window.location = "{{ route('invitations.confirmation', $invitation->id) }}";
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    });
       
</script>
@endpush