<div id="modal-delete-gallery" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      {!! Form::open(['route' => 'eventgallery.destroy']) !!}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                <h4 class="modal-title">Confirm Delete Gallery</h4>
            </div>
            {!! Form::hidden('event_id', $event_id) !!}
            {!! Form::hidden('id', '', ['id'=>'gallery_delete_id']) !!}
            <div class="modal-body">
                <p>Cautions</p>
                <p>You are deleting the event gallery, this is not recoverable.</p>
            </div>
            <div class="modal-footer">
              {!! Form::submit('Delete', ['class' => 'btn', 'style'=>'color:white; background-color: #bd330d']) !!}
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
        {!! Form::close() !!}
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
