<div id="modal-upload-galleries" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      {!! Form::open(['route' => 'eventgallery.store', 'files' => true]) !!}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                <h4 class="modal-title">Upload Event Gallery</h4>
            </div>
            <div class="modal-body">
                <p>

                  <div class="form-group {{ $errors->has('asset') ? 'has-error' : '' }}">
                    {!! Form::hidden('event_id', $event_id) !!}
                    {!! Form::label('asset', 'Gallery file') !!}
          					{!! Form::file('asset[]', ['class' => 'form-control', 'multiple', 'accept' => 'image/x-png,image/gif,image/jpeg,image/tif,image/tiff,video/mp4,application/zip,application/pdf,application/vnd.openxmlformats-officedocument.presentationml.presentation']) !!}
          					<span class="help-block">{{ $errors->first('asset', '') }}</span>
          				</div>
                </p>
            </div>
            <div class="modal-footer">
              {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
        {!! Form::close() !!}
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
