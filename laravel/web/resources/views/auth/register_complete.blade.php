@extends('layouts/front/main')

@section('content')
<section class="padding section-500" style="background: url('{{ asset('assets/images/bg-01.jpg') }}') no-repeat center center;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-mobil no-margin">
                    <div class="panel-body">
                        <h4><b>Confirmed!</b></h4>
                        <p>Thank you for registering. Please look out for the notification email confirming account activation.</p>
                        <hr>
                        <div class="row margin-top">
                            <div class="col-md-6">
                                Go back to <a href="{{ route('login') }}">login page</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
@endsection