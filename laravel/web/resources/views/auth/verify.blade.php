@extends('layouts/front/main')

@section('content')
<section class="padding section-500" style="background: url('{{ asset('assets/images/bg-01.jpg') }}') no-repeat center center;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-mobil no-margin">
                    <div class="panel-body">
                        <h4><b>Verify Your Email Address</b></h4>
                        @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                        @endif
                        <p>Before proceeding, please check your email for a verification link.</p>
                        <p>If you did not receive the email, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.</p>
                        <hr>
                        <div class="row margin-top">
                            <div class="col-md-6">
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                Go back to <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">login page</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
@endsection