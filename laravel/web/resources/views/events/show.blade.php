@extends('layouts/main')

@section('content')
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li><a href="{{ route('events') }}">Events</a></li>
            <li class="active">{{ $event->name }}</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-mobil">
                    <div class="panel-heading">
                        <h3 class="panel-title">Email Invitations</h3>
                        <div class="panel-tools pull-right">
                            <a href="{{ route('events.collect_invitations_stats', $event->id) }}" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Refresh</a>
                            <a href="{{ route('events.create_invitation', $event->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Email communication</a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>Created date</th>
                                <!--<th>Recipients</th>-->
                                <th>Sent date</th>
                                <th>Sents</th>
                                <th>Opens</th>
                                <th>Clicks</th>
                                <th>Bounce</th>
                                <th></th>
                            <tr>
                            @foreach ($event->invitations as $invitation)
                            <tr>
                                <td>{{ date('j M y h:i A', strtotime($invitation->created_at)) }}</td>
                                <!--<td><a href="{{ route('customers', ['fc' => 'a', 'fs' => $event->sector_id ]) }}" target="_blank">{{ $invitation->stat_recipients > 0 ? $invitation->stat_recipients : '' }}</a></td>-->
                                <td>{{ $invitation->sent_at ? date('j M y h:i A', strtotime($invitation->sent_at)) : '' }}</td>
                                <td>{{ $invitation->stat_sents > 0 ? $invitation->stat_sents : '' }}</td>
                                <td>{{ $invitation->stat_opens > 1 ? $invitation->stat_opens : ''  }}</td>
                                <td>{{ $invitation->stat_clicks > 0 ? $invitation->stat_clicks : ''  }}</td>
                                <td>{{ $invitation->stat_softbounces > 0 ? $invitation->stat_softbounces : ''  }}</td>
                                <td>
                                    @if($invitation->sent)
                                    <!--<a href="#" class="btn btn-default btn-xs btn-flat">View</a>-->
                                    @else
                                    <a href="{{ route('invitations.template', $invitation->id) }}" class="btn btn-default btn-xs btn-flat">Edit</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div> <!-- ./panel-body -->
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-mobil">
                    <div class="panel-heading">
                        <h3 class="panel-title">Event Information</h3>
                        <div class="panel-tools pull-right">
                            <a href="{{ route('events.edit', ['id' => $event->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-edit"></i> Edit</a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-tight table-noborder">
                            <tr>
                                <td>Name</td>
                                <td>:</td>
                                <td>{{ $event->name }}</td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>:</td>
                                <td>{{ $event->date }}</td>
                            </tr>
                            <tr>
                                <td>Time</td>
                                <td>:</td>
                                <td>{{ $event->time }}</td>
                            </tr>
                            <tr>
                                <td>Location</td>
                                <td>:</td>
                                <td>{{ $event->location }}</td>
                            </tr>
                            <tr>
                                <td>Sector</td>
                                <td>:</td>
                                <td><a href="{{ route('customers', ['fc' => 'a', 'fs' => $event->sector_id ]) }}" target="_blank">{{ $event->sector->name }}</a></td>
                            </tr>
                        </table>
                    </div> <!-- ./panel-body -->
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@include('modal.customer_consent')

@push('scripts')
<script>
$('.select2').select2();
</script>
@endpush