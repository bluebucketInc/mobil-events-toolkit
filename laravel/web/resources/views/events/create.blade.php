@extends('layouts/main')

@section('content')
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li><a href="{{ route('events') }}">Events</a></li>
            <li class="active">Add new event</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-mobil">
                    {!! Form::open(['route' => 'events.store']) !!}
                    <div class="panel-heading">
                        <h3 class="panel-title"><b>Event Form</b></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group col-md-12 {{ $errors->has('name') ? 'has-error' : '' }}">
                                {!! Form::label('name') !!}
                                {!! Form::text('name', old('name'), ['class' => 'form-control', 'maxlength' => '191']) !!}
                                <span class="help-block">{{ $errors->first('name', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 {{ $errors->has('date') ? 'has-error' : '' }}">
                                {!! Form::label('date') !!}
                                 <div class='input-group date' id='datepicker'>
                                    <input name="date" type='text' class="form-control" value="{{ old('date') }}" />
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                                <span class="help-block">{{ $errors->first('date', '') }}</span>
                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('time') ? 'has-error' : '' }}">
                                {!! Form::label('time') !!}
                                 <div class='input-group date' id='timepicker'>
                                    <input name="time" type='text' class="form-control" value="{{ old('time') }}" />
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                                <span class="help-block">{{ $errors->first('time', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 {{ $errors->has('location') ? 'has-error' : '' }}">
                                {!! Form::label('location') !!}
                                {!! Form::text('location', old('location'), ['class' => 'form-control', 'maxlength' => '191']) !!}
                                <span class="help-block">{{ $errors->first('location', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 {{ $errors->has('sector_id') ? 'has-error' : '' }}">
                                <label for="sector_id">Sector</label>
                                <select class="form-control select2" data-placeholder="Choose sector" id="sector_id" name="sector_id">
                                    <option></option>
                                    @foreach ($sectors as $sector)
                                    <option value="{{ $sector->id }}" {{ old('sector_id') == $sector->id ? ' selected' : '' }}>{{ $sector->name }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block">{{ $errors->first('sector_id', '') }}</span>
                            </div>
                        </div>
                    </div> <!-- ./panel-body -->
                    
                    <div class="panel-footer with-border">
                        <div class="pull-right">
			                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
$('#datepicker').datetimepicker({
    format: 'YYYY-MM-DD'
});
$('#timepicker').datetimepicker({
    format: 'LT'
});
$('.select2').select2();
</script>
@endpush