@extends('layouts/main')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-mobil">
                    {!! Form::open(['route' => 'user.update_profile']) !!}
                    <div class="panel-heading">
                        <h3 class="panel-title"><b>Update Your Profile</b></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group col-md-6 {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                {!! Form::label('first_name') !!}
                                {!! Form::text('first_name', $user->first_name, ['class' => 'form-control', 'maxlength' => '191']) !!}
                                <span class="help-block">{{ $errors->first('first_name', '') }}</span>
                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                {!! Form::label('last_name') !!}
                                {!! Form::text('last_name', $user->last_name, ['class' => 'form-control', 'maxlength' => '191']) !!}
                                <span class="help-block">{{ $errors->first('last_name', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 {{ $errors->has('company') ? 'has-error' : '' }}">
                                {!! Form::label('company') !!}
                                {!! Form::text('company', $user->company, ['class' => 'form-control', 'maxlength' => '191']) !!}
                                <span class="help-block">{{ $errors->first('company', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="job_title_id">Job title</label>
                                <select class="form-control select2" data-placeholder="Choose job title" id="job_title_id" name="job_title_id">
                                    @foreach ($jobTitles as $jobTitle)
                                    <option value="{{ $jobTitle->id }}" {{ ($jobTitle->id == $user->job_title_id) ? 'selected' : ''  }}>{{ $jobTitle->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> <!-- ./panel-body -->
                    
                    <div class="panel-footer">
                        <div class="pull-right">
			                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
@endsection