@extends('layouts/main')

@section('content')
<section class="padding" style="background: url('{{ asset('assets/images/bg-04.jpg') }}') no-repeat center center">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-mobil no-margin">
                    <div class="panel-body">
                        <p style="font-size: 24px;">Events Gallery</p>
                        <p>View and download photos from other events</p>
                        <!--<button id="events-checklist" data-toggle="modal" data-target="#modal-event-checklist"  class="btn btn-primary"><i class="fa fa-check-square-o"></i> Events Checklist</button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">Event Galleries</li>
        </ol>
    </div>
</section>

<section class="padding-bottom">
    <div class="container">
        <div class="bar bluebar">
            <div class="container-fluid">
                <div class="form-group searchbox pull-left">
                    <input type="text" class="form-control" id="search" placeholder="Search for files here" aria-describedby="inputSuccess2Status" value="">
                </div>
                <div class="pull-left">
                    <a id="search-icon" class="btn" href="#"><i class="glyphicon glyphicon-search" aria-hidden="true"></i></a>
                </div>

                <div class="dropdown pull-right">
                    <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-link">
                        Region: {{ $currentRegion->name }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">

                        <li><a href="{{ route('event-gallery', ['region_id' => 0]) }}">All</a></li>

                        @foreach ($regions as $region)
                          @if($region->id != 1)
                            <li><a href="{{ route('event-gallery', ['region_id' => $region->id]) }}">{{ $region->name }}</a></li>
                          @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div> <!-- /.bluebar -->
    </div>

    <div class="container container-sm">
        <h2 class="text-center">Galleries by Events</h2>
        <br><br>
        @php
          $i = 0
        @endphp

        @foreach ($events as $event)
          @if($i%3==0)
            <div class="row">
          @endif
            <div class="col-md-4">
                <div class="panel card">
                    <div>
                      @if ($event->imageGallery->count() > 0)
                        <img class="img-responsive" src="{{ asset('uploads/event_gallery/' . $event->imageGallery[0]->preview_filename) }}">
                      @else
                        <img class="img-responsive" src="{{ asset('assets/images/default.png') }}">
                      @endif
                    </div>
                    <div class="panel-body">
                        <div class="card-info-title">{{ $event->name }}</div>
                        <div>
                            <div class="card-info-media pull-left">
                                <i class="fa fa-picture-o"></i> {{ $event->imageGallery->count() }}
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('eventgallery.browse', ['event_id' => $event->id]) }}" class="btn btn-primary">Browse</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
              </div>
            @php
              $i++;
            @endphp
            @if($i%3==0)
              </div>
            @endif

          @endforeach
          @if($i%3!=0)
            </div>
          @endif

    </div>
</section>
@endsection

@include('modal/event_checklist')

@push('scripts')
<script>
$('#search').on('keyup', function(e) {
    e.preventDefault();
    if (e.keyCode == 13) {
        doSearch();
    }
});
$('#search-icon').click(function(e) {
    e.preventDefault();
    doSearch();
});

function doSearch() {
    var search = $('#search').val();
    search = search.trim();
    if (search.length < 3) return;

    var url = "{!! route('assets.browse', ['sector_slug' => 'all', 'region_id' => 1, 'view' => 'grid', 'sort' => 'newest', 'fc' => 'all', 'search' => '_search_']) !!}";
    url = url.replace('_search_', search);
    window.location.href = url;
}
</script>
@endpush
