@extends('layouts/main')

@section('content')
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li><a href="{{ route('customers') }}">Customers</a></li>
            <li class="active">Import customers</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-mobil">
                    {!! Form::open(['route' => 'customers.upload_excel', 'files' => true]) !!}
                    <div class="panel-heading">
                        <h3 class="panel-title"><b>Import Customers</b></h3>
                    </div>
                    <div class="panel-body">
                        <p><i>Please upload excel file by using this <a href="{{ asset('assets/templates/customers-import-template.xlsx') }}">template</a></p>
                        <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                            {!! Form::label('file', 'Select file to upload') !!}
                            {!! Form::file('file', ['class' => 'form-control', 'accept' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel']) !!}
                            <span class="help-block">{{ $errors->first('file', '') }}</span>
                        </div>
                    </div> <!-- ./panel-body -->
                    
                    <div class="panel-footer with-border">
                        <div class="pull-right">
			                {!! Form::submit('Import', ['class' => 'btn btn-primary']) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
@endsection

@include('modal.customer_consent')
