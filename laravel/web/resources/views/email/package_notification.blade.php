<p>Dear Distributor,</p>
<p>Your Event Toolkit Package link is ready: <a href="{{ $link }}" download>{{ $link }}</a></p>
<p>This link will be expired in 7 days.</p>
<img src="{{ asset('assets/images/logo-red.jpg') }}">