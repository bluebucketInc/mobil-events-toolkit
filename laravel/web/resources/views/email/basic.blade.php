@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level === 'error')
# @lang('Whoops!')
@else
# @lang('Hello!')
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
        case 'error':
            $color = $level;
            break;
        default:
            $color = 'primary';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach


{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy')
This events toolkit is managed by BBDO Singapore on behalf of Mobil. For any questions, please contact Janaine Eng &lt;<a href="mailto:janaine.eng@bbdo.com.sg">janaine.eng@bbdo.com.sg</a>&gt; and Kenji Leow &lt;<a href="mailto:kenji.leow@bbdo.com.sg">kenji.leow@bbdo.com.sg</a>&gt;
<br><br>
<span class="last">
@lang(
    "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
    'into your web browser: [:actionURL](:actionURL)',
    [
        'actionText' => $actionText,
        'actionURL' => $actionUrl,
    ]
)
</span>
@endcomponent
@endisset
@endcomponent
