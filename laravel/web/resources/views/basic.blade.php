@extends('layouts.main')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h1 class="text-center">{{ $page->title }}</h1>
                {!! $page->content !!}
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
@endsection