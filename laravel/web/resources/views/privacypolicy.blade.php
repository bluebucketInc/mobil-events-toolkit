@extends('layouts.main')

@section('content')
<section>
    <div class="container">
        <h1 class="text-center">Privacy Policy</h1>
        <p>Personal information is being collected by BBDO Singapore on behalf of ExxonMobil Lubricants Pvt. Ltd. and its affiliated companies.</p>
        <p>This information includes your name, business address, business contact (telephone number and email address), company name and industry, and job title. We also retain information necessary to allow you access to our Application.</p>
        <p>By registering here you confirm that any third party data submitted by you has been collected and processed in compliance with all applicable data privacy laws.</p>
        <p>Based on this notice and consent, the personal information collected will be used for the following purposes:</p>
        <ul>
        <li>to provide the information, products and services you request</li>
        <li>to better understand your needs and interests</li>
        <li>to provide you with a personalized experience when you use this Application</li>
        <li>to provide you with effective customer service</li>
        <li>to improve the content, functionality and usability of this Application</li>
        <li>to contact you with information and notices related to your use of this Application;</li>
        <li>to contact you with special offers and other information we believe will be of interest to you</li>
        <li>to invite you to participate in events, surveys and provide feedback to us</li>
        <li>to improve our products and services</li>
        <li>to improve our marketing and promotional efforts</li>
        <li>for security or fraud prevention purposes; and for any other purpose identified in any other agreement between you and us</li>
        </ul>
        <p>The information will be stored on the Amazon AWS server located in the USA. For the purposes listed in the foregoing, this information will be accessible to all personnel from Exxonmobil South Asia Pacific Finished Lubricants Marketing. In all cases, your personal information will be handled consistent with applicable data privacy laws and ExxonMobil Data Privacy Principles.</p>
    </div>
</section>
@endsection