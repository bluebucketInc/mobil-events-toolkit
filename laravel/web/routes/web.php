<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);
Route::get('register/complete', 'Auth\RegisterController@showCompletePage')->name('register.complete');

/*
Route::get('register/form', 'RegisterController@form')->name('register.form');
Route::post('register/submit', 'RegisterController@register')->name('register.submit');

Route::post('auth/login', 'Auth\LoginController@login')->name('auth.login');
Route::post('auth/logout', 'Auth\LoginController@logout')->name('auth.logout');

Route::get('verification/notice', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('verification/resend', 'Auth\VerificationController@resend')->name('verification.resend');
Route::get('verification/verify', 'Auth\VerificationController@verify')->name('verification.verify');
*/

Route::get('lead/{user_code}/form', 'LeadFormController@form')->name('leadform.form');
Route::post('lead/{user_code}/submit', 'LeadFormController@submit')->name('leadform.submit');

Route::get('privacy-policy', 'PageController@privacyPolicy');
Route::get('contact-us', 'PageController@contactUs');

Route::middleware(['verified', 'approved'])->group(function () {

    Route::get('/', 'OnboardController@index')->name('home');

    Route::get('user/profile', 'UserController@profile')->name('user.profile');
    Route::post('user/update-profile', 'UserController@updateProfile')->name('user.update_profile');
    Route::get('user/change-password', 'UserController@changePassword')->name('user.change_password');
    Route::post('user/update-password', 'UserController@updatePassword')->name('user.update_password');

    Route::get('customers/list/{fc?}/{fs?}', 'CustomerController@index')->name('customers');
    Route::get('customers/datatable/{fc?}/{fs?}', 'CustomerController@datatable')->name('customers.datatable');
    Route::get('customers/create', 'CustomerController@create')->name('customers.create');
    Route::post('customers/store', 'CustomerController@store')->name('customers.store');
    Route::get('customers/{id}/edit', 'CustomerController@edit')->name('customers.edit');
    Route::put('customers/{id}/update', 'CustomerController@update')->name('customers.update');
    Route::delete('customers/{id}/destroy', 'CustomerController@destroy')->name('customers.destroy');
    Route::get('customers/import', 'CustomerController@import')->name('customers.import');
    Route::post('customers/uploadExcel', 'CustomerController@uploadExcel')->name('customers.upload_excel');

    Route::get('events', 'EventController@index')->name('events');
    Route::get('events/create', 'EventController@create')->name('events.create');
    Route::post('events/store', 'EventController@store')->name('events.store');
    Route::get('events/{id}/show', 'EventController@show')->name('events.show');
    Route::get('events/{id}/edit', 'EventController@edit')->name('events.edit');
    Route::put('events/{id}/update', 'EventController@update')->name('events.update');
    Route::delete('events/{id}/destroy', 'EventController@destroy')->name('events.destroy');
    Route::get('events/{id}/create-invitation', 'EventController@createInvitation')->name('events.create_invitation');
    Route::get('events/{id}/collect-invitations-stats', 'EventController@collectInvitationsStats')->name('events.collect_invitations_stats');
    Route::get('events/datatable', 'EventController@datatable')->name('events.datatable');

    Route::get('invitations/{id}/template', 'InvitationController@template')->name('invitations.template');
    Route::get('invitations/{id}/choose-template/{template_id}', 'InvitationController@chooseTemplate')->name('invitations.choose_template');
    Route::get('invitations/{id}/customize', 'InvitationController@customize')->name('invitations.customize');
    Route::get('invitations/{id}/confirmation', 'InvitationController@confirmation')->name('invitations.confirmation');
    Route::post('invitations/{id}/test', 'InvitationController@test')->name('invitations.test');

    Route::post('ajax/invitations/{id}/save', 'InvitationController@ajax_save')->name('ajax.invitations.save');
    Route::post('ajax/invitations/{id}/test', 'InvitationController@ajax_test')->name('ajax.invitations.test');
    Route::post('ajax/invitations/{id}/send', 'InvitationController@ajax_send')->name('ajax.invitations.send');

    Route::get('event-assets', 'AssetController@selectSector')->name('assets');
    Route::get('event-assets/{sector_slug?}/{region_id?}/{view?}/{sort?}/{fc?}/{search?}', 'AssetController@browse')->name('assets.browse');

    Route::get('event-gallery/{region_id?}', 'EventGalleryController@selectSector')->name('event-gallery');
    Route::get('event-gallery-detail/{event_id}/{category?}/{view?}/{sort?}/{fc?}/{search?}', 'EventGalleryController@browse')->name('eventgallery.browse');
    Route::post('event-gallery-store/', 'EventGalleryController@store')->name('eventgallery.store');
    Route::post('event-gallery-destroy/', 'EventGalleryController@destroy')->name('eventgallery.destroy');

    Route::get('new-package/deselect-all', 'NewPackageController@deselectAll')->name('new_package.deselect_all');
    Route::get('new-package/compress', 'NewPackageController@compress')->name('new_package.compress');

    Route::get('gallery', 'GalleryController@index')->name('gallery');

    Route::get('ajax/new-package/add-asset/{asset_id}', 'NewPackageController@addAsset')->name('new_package.add_asset');

    Route::get('ajax/new-package/add-gallery/{gallery_id}', 'NewPackageController@addGallery')->name('new_package.add_gallery');
    Route::get('new-package/deselect-all-gallery', 'NewPackageController@deselectAllGallery')->name('new_package.deselect_all_gallery');
    Route::get('new-package/compress-gallery', 'NewPackageController@compressGallery')->name('new_package.compress_gallery');

    Route::get('new-package/upload-gallery', 'NewPackageController@uploadGallery')->name('new_package.upload_gallery');

});

Route::get('hubspot/test/createcontact/{email}/{firstname}/{lastname}', 'HomeController@hubspot_createContact');
