<?php $__env->startSection('content'); ?>
<section class="padding" style="background: url('<?php echo e(asset('assets/images/bg-04.jpg')); ?>') no-repeat center center">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-mobil no-margin">
                    <div class="panel-body">
                        <p style="font-size: 24px;">Events Gallery</p>
                        <p>View photos from other events.</p>
                        <!--<button id="events-checklist" data-toggle="modal" data-target="#modal-event-checklist" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Events Checklist</button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('home')); ?>">Home</a></li>
            <li><a href="<?php echo e(route('event-gallery')); ?>">Event Galleries</a></li>
            <li class="active"><?php echo e($sector_label); ?></li>
        </ol>
    </div>
</section>

<section class="padding-bottom">
    <div class="container">

        <div class="bar bluebar">
            <div class="container-fluid">

                <div class="pull-left">
                    <a id="search-icon" class="btn" href="#"><i class="glyphicon glyphicon-search" aria-hidden="true"></i></a>
                </div>
                <div class="view-buttons pull-right">
                    <a href="<?php echo e(route('eventgallery.browse', ['event_id' => $event_id, 'category' => $category, 'view' => 'grid'])); ?>" class="btn btn-link <?php echo e($view == 'grid' ? 'active' : ''); ?>"><i class="fa fa-th-large"></i></a>
                    <a href="<?php echo e(route('eventgallery.browse', ['event_id' => $event_id, 'category' => $category, 'view' => 'list'])); ?>" class="btn btn-link <?php echo e($view == 'list' ? 'active' : ''); ?>"><i class="fa fa-list"></i></a>
                </div>

              


            </div>
        </div>

        <div class="bar linebar">
            <div class="pull-left">
                <div class="linebar-info pull-left">
                    <span><span id="selected-asset-count"><?php echo e($newPackage ? $newPackage->galleries->count() : '0'); ?></span> selected</span>
                    <span><a href="<?php echo e(route('new_package.deselect_all_gallery')); ?>">Deselect all</a></span>
                </div>
                <a class="btn btn-primary btn-sm" href="javascript: downloadAssets()" class="btn btn-primary"><i class="fa fa-download"></i> Download Gallery</a>
                <!--<a class="btn btn-secondary btn-sm" href="#" class="btn btn-default" data-toggle="modal" data-target="#modal-booth-mockup">Booth Mockup</a>-->
                <!--
                <a class="btn btn-secondary btn-sm"href="#" class="btn btn-default">Old Packages</a>
                -->
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="row">
            <?php if($view == 'grid'): ?>
            <div class="asset-grid">
                <?php $__currentLoopData = $galleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-3 col-sm-6">
                    <div class="panel card">
                        <div style="position: relative;">
                            <img class="img-responsive" src="<?php echo e(asset('uploads/event_gallery/400x200-' . $gallery->preview_filename)); ?>">
                            <div style="position: absolute; left: 5px; bottom: 5px;"></span></div>
                        </div>
                        <div class="panel-body">
                            <div class="card-info-title asset"><?php echo e($gallery->name); ?></div>
                            <div clsas="card-info-created"><?php echo e($gallery->created_at->format('j F Y \a\t h:i A')); ?></div>
                            <div class="card-info-filesize">
                                <?php if($gallery->filesize < 1024): ?>
                                <?php echo e(round($gallery->filesize, 1)); ?> B
                                <?php elseif($gallery->filesize < 1048576): ?>
                                <?php echo e(round($gallery->filesize / 1024, 1)); ?> KB
                                <?php elseif($gallery->filesize < 1073741824): ?>
                                <?php echo e(round($gallery->filesize / 1048576, 1)); ?> MB
                                <?php else: ?>
                                <?php echo e(round($gallery->filesize / 1073741824, 1)); ?> GB
                                <?php endif; ?>
                            </div>
                            <div class="card-footer-buttons">
                                <?php if($gallery->format == 'image'): ?>
                                <a href="<?php echo e(url('uploads/event_gallery/' . $gallery->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-popup"><i class="fa fa-eye"></i> Preview</a>
                                <?php elseif($gallery->format == 'video'): ?>
                                <a href="<?php echo e(url('uploads/event_gallery/' . $gallery->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview preview-video" data-source="<?php echo e($gallery->filename); ?>"><i class="fa fa-eye"></i> Preview</a>

                                <?php elseif($gallery->format == 'pdf'): ?>
                                <!-- Desktop -->
                                <a href="<?php echo e(url('uploads/event_gallery/' . $gallery->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-iframe hidden-xs hidden-sm hidden-md"><i class="fa fa-eye"></i> Preview</a>
                                <!-- Mobile -->
                                <a href="<?php echo e(url('uploads/event_gallery/' . $gallery->preview_filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-popup hidden-lg hidden-xl"><i class="fa fa-eye"></i> Preview</a>

                                <?php elseif($gallery->format == 'iframe'): ?>
                                <a href="<?php echo e(url('uploads/event_gallery/' . $gallery->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-iframe"><i class="fa fa-eye"></i> Preview</a>
                                <?php endif; ?>
                                <a href="#" data-asset-id="<?php echo e($gallery->id); ?>" class="btn btn-sm btn-primary pull-right download" style="margin-right: 5px;"><i class="fa <?php echo e($newPackage ? ($newPackage->galleries->contains($gallery->id) ? 'fa-check-square-o' : 'fa-square-o') : 'fa-square-o'); ?>"></i> Download</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <?php elseif($view == 'list'): ?>
            <div class="asset-list">
                <?php $__currentLoopData = $galleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-12">
                    <div class="panel panel-default listitem">
                        <div class="listbo">
                            <div class="listcol listcol-left">
                                <img class="img-responsive" src="<?php echo e(asset('uploads/event_gallery/400x200-' . $gallery->preview_filename)); ?>">
                            </div>
                            <div class="listcol listcol-right">

                                <div class="card-info-title asset"><?php echo e($gallery->name); ?></div>
                                <div class="card-info-created"><?php echo e($gallery->created_at->format('j F Y \a\t h:i A')); ?></div>
                                <div class="card-info-filesize">
                                    <?php if($gallery->filesize < 1024): ?>
                                    <?php echo e(round($gallery->filesize, 1)); ?> B
                                    <?php elseif($gallery->filesize < 1048576): ?>
                                    <?php echo e(round($gallery->filesize / 1024, 1)); ?> KB
                                    <?php elseif($gallery->filesize < 1073741824): ?>
                                    <?php echo e(round($gallery->filesize / 1048576, 1)); ?> MB
                                    <?php else: ?>
                                    <?php echo e(round($gallery->filesize / 1073741824, 1)); ?> GB
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="listov">
                            <?php if($gallery->format == 'image'): ?>
                            <a href="<?php echo e(url('uploads/event_gallery/' . $gallery->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-popup"><i class="fa fa-eye"></i> Preview</a>
                            <?php elseif($gallery->format == 'video'): ?>
                            <a href="<?php echo e(url('uploads/event_gallery/' . $gallery->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview preview-video" data-source="<?php echo e($gallery->filename); ?>"><i class="fa fa-eye"></i> Preview</a>

                            <?php elseif($gallery->format == 'pdf'): ?>
                            <!-- Desktop -->
                            <a href="<?php echo e(url('uploads/event_gallery/' . $gallery->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-iframe hidden-xs hidden-sm hidden-md"><i class="fa fa-eye"></i> Preview</a>
                            <!-- Mobile -->
                            <a href="<?php echo e(url('uploads/event_gallery/' . $gallery->preview_filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-popup hidden-lg hidden-xl"><i class="fa fa-eye"></i> Preview</a>

                            <?php elseif($gallery->format == 'iframe'): ?>
                            <a href="<?php echo e(url('uploads/event_gallery/' . $gallery->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-iframe"><i class="fa fa-eye"></i> Preview </a>
                            <?php endif; ?>
                            <a href="#" data-asset-id="<?php echo e($gallery->id); ?>" class="btn btn-sm btn-primary pull-right download"><i class="fa <?php echo e($newPackage ? ($newPackage->galleries->contains($gallery->id) ? 'fa-check-square-o' : 'fa-square-o') : 'fa-square-o'); ?>"></i>  Download</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('modal/event_checklist', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modal/booth_mockup', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modal/compressing_package', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modal/no_assets', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modal/video_content', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->startPush('scripts'); ?>
<script>

function downloadAssets()
{
    var count = parseInt($('#selected-asset-count').html());
    if (count > 0) {
        window.location.href = "<?php echo e(route('new_package.compress_gallery')); ?>";
    } else {
        $('#modal-no-assets').modal();
    }
}

$('.download').click(function(e) {
    e.preventDefault();
    button = $(this);
    assetId = $(this).data('asset-id');
    $.ajax({
        url: "<?php echo e(url('ajax/new-package/add-gallery')); ?>/" + assetId,
        type: 'GET',
        success: function (data) {
            if (data.change == 'add') {
                $(button).find('i.fa').removeClass('fa-square-o').addClass('fa-check-square-o');
            } else {
                $(button).find('i.fa').removeClass('fa-check-square-o').addClass('fa-square-o');
            }
            $('#selected-asset-count').html(data.total);
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
});

$('#search').on('keyup', function(e) {
    e.preventDefault();
    if (e.keyCode == 13) {
        doSearch();
    }
});
$('#search-icon').click(function(e) {
    e.preventDefault();
    doSearch();
});

function doSearch() {
    var search = $('#search').val();
    search = search.trim();
    if (search.length < 3) return;

    var url = "<?php echo route('assets.browse', ['sector_slug' => 'all', 'region_id' => 1, 'view' => 'grid', 'sort' => 'newest', 'fc' => 'all', 'search' => '_search_']); ?>";
    url = url.replace('_search_', search);
    window.location.href = url;
}

<?php if(Session::has('compress')): ?>
console.log('compressing');
$('#modal-compressing-package').modal();
<?php endif; ?>

$('.magnific-popup').magnificPopup({type:'image'});
$('.preview-video').click(function(e) {
	e.preventDefault();
	var source = "<?php echo e(asset('uploads/event_assets')); ?>/" + $(this).data('source');
	$('#modal-video-content').modal();
	$('#video-container').get(0).pause();
    $('#video-source').attr('src', source);
    $('#video-container').get(0).load();
});
$('.magnific-iframe').magnificPopup({
	type: 'iframe'
});

</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>