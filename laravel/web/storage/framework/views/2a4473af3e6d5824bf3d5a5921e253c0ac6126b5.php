<?php $__env->startSection('page-title', 'Assets'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="<?php echo e(route('admin.assets.browse', [ 'sector_id' => $asset->sector_id, 'category_id' => $asset->category_id ])); ?>">Assets</a></li>
<li class="active">Edit</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="col-md-6">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Edit Asset</h3>
		</div>
		<?php echo Form::open(['method' => 'PUT', 'route' => ['admin.assets.update', $asset->id], 'files' => true]); ?>

			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group <?php echo e($errors->has('sector_id') ? 'has-error' : ''); ?>">
							<label for="sector_id">Sector</label>
							<select class="form-control select2" data-placeholder="Choose sector" id="sector_id" name="sector_id">
								<?php $__currentLoopData = $sectors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sector): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($sector->id); ?>" <?php echo e($sector->id == $asset->sector_id ? 'selected' : ''); ?>><?php echo e($sector->name); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
							<span class="help-block"><?php echo e($errors->first('sector_id', '')); ?></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group <?php echo e($errors->has('category_id') ? 'has-error' : ''); ?>">
							<label for="category_id">Category</label>
							<select class="form-control select2" data-placeholder="Choose category" id="category_id" name="category_id">
								<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($category->id); ?>" <?php echo e($category->id == $asset->category_id ? 'selected' : ''); ?>><?php echo e($category->name); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
							<span class="help-block"><?php echo e($errors->first('category_id', '')); ?></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
							<?php echo Form::label('name'); ?>

							<?php echo Form::text('name', $asset->name, ['class' => 'form-control', 'maxlength' => '50']); ?>

							<span class="help-block"><?php echo e($errors->first('name', '')); ?></span>
						</div>
					</div>
				</div>
				<div class="form-group <?php echo e($errors->has('asset') ? 'has-error' : ''); ?>">
					<div class="form-group">
						<?php echo Form::label('asset', 'Asset file'); ?>

						<?php echo Form::file('asset', ['class' => 'form-control', 'accept' => 'image/x-png,image/gif,image/jpeg,image/tif,image/tiff,video/mp4,application/zip,application/pdf,application/vnd.openxmlformats-officedocument.presentationml.presentation']); ?>

						<span class="help-block"><?php echo e($errors->first('asset', '')); ?></span>
					</div>
				</div>
				<div class="form-group">
					<div class="form-group <?php echo e($errors->has('preview') ? 'has-error' : ''); ?>">
						<?php echo Form::label('preview', 'Please upload a screenshot of your document for thumbnail preview.'); ?>

						<?php echo Form::file('preview', ['class' => 'form-control', 'accept' => 'image/x-png,image/gif,image/jpeg']); ?>

						<span class="help-block"><?php echo e($errors->first('preview', '')); ?></span>
					</div>
				</div>
			</div> <!-- /.box-body -->

			<div class="box-footer">
				<?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

				<a href="<?php echo e(route('admin.assets.browse', [ 'sector_id' => $asset->sector_id, 'category_id' => $asset->category_id ] )); ?>" class="btn btn-default">Cancel</a>
			</div>
		<?php echo Form::close(); ?>

	</div> <!-- /.box -->
</div> <!-- /.col-md-6 -->

<div class="col-md-6">
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">Asset Media</h3>
		</div>
		<div class="box-body">
			<?php if($asset->file_extension == 'mp4'): ?>
			<video width="320" height="240" controls>
				<source src="<?php echo e(url('uploads/event_assets/' . $asset->filename)); ?>" type="video/mp4">
				Your browser does not support the video tag.
			</video>
			<?php elseif($asset->file_extension == 'pdf'): ?>
			<a href="<?php echo e(url('uploads/event_assets/' . $asset->filename)); ?>" class="magnific-iframe">
				<img src="<?php echo e(asset('uploads/event_assets/400x400-' . $asset->preview_filename)); ?>" class="img-responsive">
			</a>
			<?php else: ?>
			<img src="<?php echo e(asset('uploads/event_assets/' . $asset->filename)); ?>" class="img-responsive">
			<?php endif; ?>
		</div>
	</div> <!-- /.box -->
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
	$('.select2').select2();
	$('.magnific-iframe').magnificPopup({
		type: 'iframe'
	});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>