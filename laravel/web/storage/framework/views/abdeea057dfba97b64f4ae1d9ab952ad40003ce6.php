<?php $__env->startSection('content'); ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h1 class="text-center"><?php echo e($page->title); ?></h1>
                <?php echo $page->content; ?>

            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>