<?php $__env->startSection('content'); ?>
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('home')); ?>">Home</a></li>
            <li class="active">Customers</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="panel panel-mobil panel-filter">
                    <div class="panel-heading with-border">
                        <h3 class="panel-title">Filter</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="sector_id">Company</label>
                            <select class="form-control select2" data-placeholder="Choose company" id="company_name">
                                <option value="a" <?php echo e($fc == 'a' ? 'selected' : ''); ?>>All</option>
                                <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($company->name); ?>" <?php echo e($fc == $company->name ? 'selected' : ''); ?>><?php echo e($company->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="sector_id">Sector</label>
                            <select class="form-control select2" data-placeholder="Choose sector" id="sector_id">
                                <option value="a" <?php echo e($fs == 'a' ? 'selected' : ''); ?>>All</option>
                                <?php $__currentLoopData = $sectors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sector): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($sector->id); ?>" <?php echo e($fs == $sector->id ? 'selected' : ''); ?>><?php echo e($sector->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <a id="filter" href="#" class="btn btn-secondary btn-sm pull-right">Apply</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="panel panel-mobil">
                    <div class="panel-heading with-border">
                        <h3 class="panel-title">Customer List</h3>
                        <div class="panel-tools pull-right">
                            <a href="<?php echo e(route('customers.create')); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add new contact</a>
                            <a href="<?php echo e(route('leadform.form', ['user_code' => $user->code])); ?>" target="_blank" class="btn btn-secondary btn-sm"><i class="fa fa-link"></i> On-Site Registration Form</a>
                            <a href="<?php echo e(route('customers.import')); ?>" class="btn btn-secondary btn-sm"><i class="fa fa-upload"></i> Import from Excel</a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped" id="table-customers">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Company</th>
                                                <th>Sector</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-customer-delete-' + id).submit();
	}
}

$(function() {
    $('#table-customers').DataTable({
        processing: true,
        serverSide: true,
        ajax: '<?php echo route('customers.datatable', ['fc' => $fc, 'fs' => $fs]); ?>',
        columns: [
			{ data: 'name', name: 'name' },
			{ data: 'email', name: 'email' },
			{ data: 'company', name: 'company' },
			{ data: 'sector', name: 'sector' },
            { 
            	data: 'null', 
            	name: 'action', 
            	orderable: false, 
            	searchable: false,
            	render: function(data, type, row) {
            		var html = '';
            		html += '<a href="<?php echo e(route('customers.edit', ['id' => ':id'])); ?>" class="btn btn-xs btn-default btn-flat"><i class="fa fa-pencil"></i> Edit</a>';
	                html += '<a href="#" class="btn btn-danger btn-flat btn-xs" onclick="event.preventDefault(); confirmDelete(:id)"><i class="fa fa-trash"></i> Delete</a>';
	                html += '<?php echo Form::open( [ 'method' => 'DELETE', 'route' => [ 'customers.destroy', ':id' ], 'id' => 'form-customer-delete-:id' ] ); ?>';
	                html += '<?php echo Form::close(); ?>';
            		return html.replace(/:id/g, row.id);
            	}
        	}
        ]
    });

    $('#filter').click(function(e) {
        e.preventDefault();
        var fc = $('#company_name').val();
        var fs = $('#sector_id').val();
        window.location.href = "<?php echo e(route('customers')); ?>/" + fc + "/" + fs;
    });
});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>