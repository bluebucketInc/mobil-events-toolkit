<?php $__env->startSection('content'); ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-mobil">
                    <?php echo Form::open(['route' => 'user.update_password']); ?>

                    <div class="panel-heading">
                        <h3 class="panel-title"><b>Change Password</b></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('old_password') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('old_password'); ?>

                                <?php echo Form::password('old_password', ['class' => 'form-control', 'maxlength' => '191']); ?>

                                <span class="help-block"><?php echo e($errors->first('old_password', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('password') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('password', 'New password'); ?>

                                <?php echo Form::password('password', ['class' => 'form-control', 'maxlength' => '191']); ?>

                                <span class="help-block"><?php echo e($errors->first('password', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <?php echo Form::label('password_confirmation', 'Confirm new password'); ?>

                                <?php echo Form::password('password_confirmation', ['class' => 'form-control', 'maxlength' => '191']); ?>

                            </div>
                        </div>
                    </div> <!-- ./panel-body -->
                    
                    <div class="panel-footer">
                        <div class="pull-right">
			                <?php echo Form::submit('Update', ['class' => 'btn btn-primary']); ?>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>