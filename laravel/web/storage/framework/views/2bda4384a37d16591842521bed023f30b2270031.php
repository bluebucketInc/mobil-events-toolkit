<?php $__env->startSection('page-title', 'Event Gallery'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active">Event Galleries</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="col-md-12">

	<div class="box">
		<div class="box-header with-border">
      <div class="btn-group pull-right">
  			<a href="<?php echo e(route('admin.eventgallery.create', [])); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add new gallery</a>
  		</div>
		</div>

			<div class="box-body">
        <div class="row asset-grid">
          <?php if(count($tmpEvents) == 0): ?>
          <h2>Please create your first event.</2>
          <?php endif; ?>
        	<?php $__currentLoopData = $tmpEvents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        		<div class="col-md-3 col-sm-6">
        			<div class="box asset-grid-item">
        				<div class="box-head">
                  <a href="<?php echo e(route('admin.eventgallery.details', ['id' => $event->id ])); ?>">
                    <?php if($event->ext == "image"): ?>
                    <img src="<?php echo e(asset('uploads/event_gallery/400x200-' . $event->file_name)); ?>" class="img-responsive">
                    <?php elseif($event->ext == "video"): ?>
                      <img alt="<?php echo e($event->name); ?>" src="<?php echo e(asset('assets/images/video-icon.jpg')); ?>" class="img-responsive">
                    <?php elseif($event->ext == "pdf"): ?>
                      <img alt="<?php echo e($event->name); ?>" src="<?php echo e(asset('assets/images/pdf-icon.jpg')); ?>" class="img-responsive">
                    <?php else: ?>
                      <img alt="<?php echo e($event->name); ?>" src="<?php echo e(asset('assets/images/placeholder-image.jpg')); ?>" class="img-responsive">
                    <?php endif; ?>
                  </a>
        				</div>
        				<div class="box-footer">
        					<a href="<?php echo e(route('admin.eventgallery.details', ['id' => $event->id ])); ?>"><?php echo e($event->name); ?></a>
                  <br/><?php echo e($event->date); ?>

                  <h5><?php echo e($event->galleries->count()); ?> Galleries</h5>

        				</div>
        			</div>
        		</div>
        	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>


			</div> <!-- /.box-body -->

	</div> <!-- /.box -->
</div> <!-- /.col-md-6 -->



<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
	$('.select2').select2();
	$('.magnific-iframe').magnificPopup({
		type: 'iframe'
	});
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>