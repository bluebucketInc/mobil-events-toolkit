<?php $__env->startSection('content'); ?>
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('home')); ?>">Home</a></li>
            <li><a href="<?php echo e(route('events')); ?>">Events</a></li>
            <li><a href="<?php echo e(route('events.show', $invitation->event->id)); ?>"><?php echo e($invitation->event->name); ?></a></li>
            <li class="active">Invitation - Customize</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-mobil">
                    <div class="panel-heading">
                        <h3 class="panel-title">Customize your eDMs</h3>
                    </div>
                    <div class="panel-body">
                        <p>If you wish to create custom content, place it in the field below</p>
                        <div class="form-group">
                            <label>Distributor company name</label>
                            <?php echo Form::text('distributor_company_name', isset($invitation->json_data) && isset($invitation->json_data->distributor_company_name) ? $invitation->json_data->distributor_company_name : '', ['id' => 'distributor_company_name', 'class' => 'form-control', 'placeholder' => 'insert distributor company name here']); ?>

                        </div>
                        <div class="form-group">
                            <label>Contact details</label>
                            <?php echo Form::text('contact_details', isset($invitation->json_data) && isset($invitation->json_data->contact_details) ? $invitation->json_data->contact_details : '', ['id' => 'contact_details', 'class' => 'form-control', 'placeholder' => 'insert contact details here']); ?>

                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="pull-right">
                            <a href="#" id="save" class="btn btn-primary">Save</a>
                            <a href="#" id="next" class="btn btn-secondary">Save &amp; Next</a>
                        </div>
                        <div clas="pull-left">
                            <a href="<?php echo e(route('invitations.template', $invitation->id)); ?>" class="btn btn-default">Back</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="panel panel-mobil">
                    <div class="panel-heading">
                        <p><b>From:</b> <?php echo e(config('mail.from.name')); ?> &lt;<?php echo e(config('mail.from.address')); ?> &gt;<br><b>Subject:</b> <?php echo e($invitation->email_template->subject); ?></p>
                    </div>
                    <div class="edm-html-wrapper">
                        <?php echo $invitation->html_content; ?>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /.container -->
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>


    $('#save').click(function(e) {
        e.preventDefault();

        var saveUrl = "<?php echo e(route('ajax.invitations.save', $invitation->id)); ?>";
        var saveData = {
            distributor_company_name: $('#distributor_company_name').val(),
            contact_details: $('#contact_details').val()
        };

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            type: 'POST',
            url: saveUrl,
            data: saveData,
            datatype: 'json',
            success: function (data) { 
                window.location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    });
    
     $('#next').click(function(e) {
        e.preventDefault();

        var saveUrl = "<?php echo e(route('ajax.invitations.save', $invitation->id)); ?>";
        var saveData = {
            distributor_company_name: $('#distributor_company_name').val(),
            contact_details: $('#contact_details').val()
        };

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            type: 'POST',
            url: saveUrl,
            data: saveData,
            datatype: 'json',
            success: function (data) { 
                window.location = "<?php echo e(route('invitations.confirmation', $invitation->id)); ?>";
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    });
       
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>