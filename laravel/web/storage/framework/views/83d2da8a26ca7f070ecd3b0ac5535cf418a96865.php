<?php $__env->startSection('content'); ?>
<section class="padding" style="background: url('<?php echo e(asset('assets/images/bg-04.jpg')); ?>') no-repeat center center">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-mobil no-margin">
                    <div class="panel-body">
                        <p style="font-size: 24px;">Download event support material</p>
                        <p>Access and download relevant marketing materials for your event execution needs.</p>
                        <!--<button id="events-checklist" data-toggle="modal" data-target="#modal-event-checklist"  class="btn btn-primary"><i class="fa fa-check-square-o"></i> Events Checklist</button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('home')); ?>">Home</a></li>
            <li class="active">Assets</li>
        </ol>
    </div>
</section>

<section class="padding-bottom">
    <div class="container container-sm">
        <h2 class="text-center">Assets by Sector</h2>
        <br><br>
        <div class="row">
            <?php $__currentLoopData = $sectors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sector): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-4">
                <div class="panel card">
                    <div>
                        <img class="img-responsive" src="<?php echo e(asset('uploads/sectors/' . $sector->image_path)); ?>">
                    </div>
                    <div class="panel-body">
                        <div class="card-info-title"><?php echo e($sector->name); ?></div>
                        <div>
                            <div class="card-info-media pull-left">
                                <i class="fa fa-picture-o"></i> <?php echo e($sector->assets->count()); ?>

                            </div>
                            <div class="pull-right">
                                <a href="<?php echo e(route('assets.browse', ['sector_slug' => $sector->slug])); ?>" class="btn btn-primary">Browse</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('modal/event_checklist', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>