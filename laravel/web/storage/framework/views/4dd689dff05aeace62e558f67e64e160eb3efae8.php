<?php $__env->startSection('page-title', 'Dashboard'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li><i class="fa fa-dashboard"></i> Dashboard</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-users"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Administrators</span>
                <span class="info-box-number"><?php echo e($admins->count()); ?></span>
                <span class="info-see-details"><a href="<?php echo e(route('admin.admins.index')); ?>">See details</a></span>
            </div>
            <!-- /.info-box-content -->
        </div>        
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <?php if(\Session::get('region_id', \Auth::guard('admin')->user()->region_id) != 1): ?>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Distributors</span>
                <span class="info-box-number"><?php echo e($users->count()); ?></span>
                <span class="info-see-details"><a href="<?php echo e(route('admin.users.index')); ?>">See details</a></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div> <!-- /.col -->
    

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-address-card"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Customers</span>
                <span class="info-box-number"><?php echo e($customers->count()); ?></span>
                <span class="info-see-details"><a href="<?php echo e(route('admin.customers.index')); ?>">See details</a></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div> <!-- /.col -->
    <?php endif; ?>

</div> <!-- /.row -->

<div class="row">

    <!--
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-calendar"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Events</span>
                <span class="info-box-number">34</span>
                <span class="info-see-details"><a href="#">See details</a></span>
            </div>
        </div>        
    </div>
    -->

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-file-o"></i></span>    
            <div class="info-box-content">
                <span class="info-box-text">Assets</span>
                <span class="info-box-number"><?php echo e($assets->count()); ?></span>
                <span class="info-see-details"><a href="<?php echo e(route('admin.assets.browse')); ?>">See details</a></span>
            </div>          
            <!-- /.info-box-content -->   
        </div>
        <!-- /.info-box -->
    </div> 
    <!-- /.col -->

</div>
<!-- /.row -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>