<?php $__env->startSection('page-title', 'Articles'); ?>

<?php $__env->startSection('content'); ?>

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Edit Article</h3>
	</div>
	<?php echo Form::open(['method' => 'PUT', 'route' => ['admin.articles.update', $article->id], 'files' => true]); ?>

		<div class="box-body">
			<div class="row">
				<div class="col-md-3 form-group">
					<?php echo Form::label('published'); ?>

					<?php echo Form::select('published', [ 0 => 'No', 1 => 'Yes' ], $article->published, ['class' => 'form-control']); ?>

				</div>
				<div class="col-md-3 form-group">
					<?php echo Form::label('date_published'); ?>

					<?php echo Form::date('date_published', $article->date_published, ['class' => 'form-control']); ?>

				</div>
			</div>
			<div class="form-group">
				<?php echo Form::label('title'); ?>

				<?php echo Form::text('title', $article->title, ['class' => 'form-control', 'maxlength' => '60']); ?>

			</div>
			<div class="form-group">
				<?php echo Form::label('excerpt'); ?>

				<?php echo Form::textarea('excerpt', $article->excerpt, ['class' => 'form-control']); ?>

			</div>
			<div class="form-group">
				<?php echo Form::label('content'); ?>

				<?php echo Form::textarea('content', $article->content, ['class' => 'form-control tinymce']); ?>

			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<?php echo Form::label('youtube_id'); ?>

						<?php echo Form::text('youtube_id', $article->youtube_id, ['class' => 'form-control', 'maxlength' => '20']); ?>

					</div>
					<div class="form-group">
						<?php echo Form::label('image'); ?>

						<?php echo Form::file('image', ['class' => 'form-control', 'accept' => 'image/x-png,image/gif,image/jpeg']); ?>

						<img src="<?php echo e(asset('uploads/articles/t-'.$article->image_path)); ?>" class="img-thumbnail">
					</div>
				</div>
			</div>
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			<?php echo Form::submit('Submit', ['class' => 'btn btn-primary']); ?>

			<a href="<?php echo e(route('admin.articles.index')); ?>" class="btn btn-default">Cancel</a>
		</div>
	<?php echo Form::close(); ?>

</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>