<?php $__env->startSection('content'); ?>
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('home')); ?>">Home</a></li>
            <li class="active">Events</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="panel panel-mobil panel-filter">
                    <div class="panel-heading with-border">
                        <h3 class="panel-title">Filter</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="from_date">From date</label>
                            <div class='input-group date' id='datepicker1'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="to_date">To date</label>
                            <div class='input-group date' id='datepicker2'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sector_id">Sector</label>
                            <select class="form-control select2" data-placeholder="Choose sector" id="sector_id">
                                <option value="a" <?php echo e($fs == 'a' ? 'selected' : ''); ?>>All</option>
                                <?php $__currentLoopData = $sectors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sector): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($sector->id); ?>" <?php echo e($fs == $sector->id ? 'selected' : ''); ?>><?php echo e($sector->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <a id="filter" href="#" class="btn btn-secondary btn-sm pull-right">Apply</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="panel panel-mobil">
                    <div class="panel-heading with-border">
                        <h3 class="panel-title">Event List</h3>
                        <div class="panel-tools pull-right">
                            <a href="<?php echo e(route('events.create')); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add new event</a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped" id="table-events">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Sector</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-event-delete-' + id).submit();
	}
}

$(function() {
    $('#table-events').DataTable({
        processing: true,
        serverSide: true,
        ajax: '<?php echo route('events.datatable', ['fd1' => $fd1, 'fd2' => $fd2, 'fs' => $fs, 'fst' => $fst]); ?>',
        columns: [
			{ data: 'name', name: 'name' },
			{ data: 'date', name: 'date' },
			{ data: 'sector', name: 'sector' },
            { 
            	data: 'null', 
            	name: 'action', 
            	orderable: false, 
            	searchable: false,
            	render: function(data, type, row) {
            		var html = '';
            		html += '<a href="<?php echo e(route('events.show', ['id' => ':id'])); ?>" class="btn btn-xs btn-default btn-flat"><i class="fa fa-eye"></i> View</a>';
	                html += '<a href="#" class="btn btn-danger btn-flat btn-xs" onclick="event.preventDefault(); confirmDelete(:id)"><i class="fa fa-trash"></i> Delete</a>';
	                html += '<?php echo Form::open( [ 'method' => 'DELETE', 'route' => [ 'events.destroy', ':id' ], 'id' => 'form-event-delete-:id' ] ); ?>';
	                html += '<?php echo Form::close(); ?>';
            		return html.replace(/:id/g, row.id);
            	}
        	}
        ]
    });

    $('#filter').click(function(e) {
        e.preventDefault();
        var fd1 = $('#datepicker1').find("input").val();
        var fd2 = $('#datepicker2').find("input").val();
        var fs = $('#sector_id').val();
        url = "<?php echo route('events', ['fd1' => '_fd1_', 'fd2' => '_fd2_', 'fs' => '_fs_']); ?>";
        url = url.replace('_fd1_', fd1);
        url = url.replace('_fd2_', fd2);
        url = url.replace('_fs_', fs);
        //console.log(url);
        window.location.href = url;
    });

    $('#datepicker1').datetimepicker({
        format: 'YYYY-MM-DD',
        date: '<?php echo e($fd1); ?>',
        showClear: true,
    });
    $('#datepicker2').datetimepicker({
        format: 'YYYY-MM-DD',
        date: '<?php echo e($fd2); ?>',
        showClear: true,
    });
});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>