<?php $__env->startSection('page-title', 'Events'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active">Events</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="box">
	<div class="box-header">
		<h3 class="box-title">Event List</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="dataTables_wrapper form-inline dt-bootstrap">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped" id="table-exhibitionList">
				        <thead>
				            <tr>
								<th>Id</th>
								<th>Event name</th>
								<th>Created by</th>
								<th>Sector</th>
								<th>Event date</th>
								<th>Invitations</th>
				                <th>Action</th>
				            </tr>
				        </thead>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.box-body -->
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-exhibition-delete-' + id).submit();
	}
}

$(function() {
    $('#table-exhibitionList').DataTable({
        processing: true,
        serverSide: true,
        ajax: "<?php echo e(url('admin/events/datatable')); ?>",
        columns: [
			{ data: 'id', name: 'id' },
			{ data: 'name', name: 'name' },
			{ data: 'distributor_name', name: 'distributor_name' },
			{ data: 'sector_name', name: 'sector_name' },
			{ data: 'date', name: 'date' },
			{ data: 'invitations_count', name: 'invitations_count' },
            { 
            	data: 'null', 
            	name: 'action', 
            	orderable: false, 
            	searchable: false,
            	render: function(data, type, row) {
            		var html = '';
            		html += '<a href="<?php echo e(route('admin.events.show', ['id' => ':id'])); ?>" class="btn btn-xs btn-default btn-flat"><i class="fa fa-eye"></i> View</a>';
            		return html.replace(/:id/g, row.id);
            	}
        	}
        ]
    });
});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>