<?php $__env->startSection('page-title', 'Regions'); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
	<div class="col-md-6">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Region</h3>
			</div>
			<?php echo Form::open(['method' => 'PUT', 'route' => ['admin.regions.update', $region->id], 'files' => true]); ?>

				<div class="box-body">
					
					<div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
						<?php echo Form::label('name'); ?>

						<?php echo Form::text('name', $region->name, ['class' => 'form-control', 'maxlength' => '50']); ?>

						<span class="help-block"><?php echo e($errors->first('name', '')); ?></span>
					</div>
					<div class="form-group <?php echo e($errors->has('order') ? 'has-error' : ''); ?>">
						<?php echo Form::label('order'); ?>

						<?php echo Form::number('order', $region->order, ['class' => 'form-control']); ?>

						<span class="help-block"><?php echo e($errors->first('order', '')); ?></span>
					</div>
					<div class="form-group <?php echo e($errors->has('order') ? 'has-error' : ''); ?>">
						<?php echo Form::label('active'); ?>

						<?php echo Form::select('active', [ 0 => 'No', 1 => 'Yes' ], $region->active, ['class' => 'form-control']); ?>

						<span class="help-block"><?php echo e($errors->first('active', '')); ?></span>
					</div>

				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<?php echo Form::submit('Submit', ['class' => 'btn btn-primary']); ?>

					<a href="<?php echo e(route('admin.regions.index')); ?>" class="btn btn-default">Cancel</a>
				</div>
			<?php echo Form::close(); ?>

		</div>
		<!-- /.box -->
	</div>
	<!-- /.col-md-6 -->
</div>
<!-- /.row -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>