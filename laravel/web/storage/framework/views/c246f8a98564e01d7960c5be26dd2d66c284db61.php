<?php $__env->startSection('page-title', 'Distributors'); ?>

<?php $__env->startSection('content'); ?>

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Edit Distributor</h3>
	</div>
	<?php echo Form::open(['method' => 'PUT', 'route' => ['admin.users.update', $user->id]]); ?>

		<div class="box-body">
			<div class="row">
				<div class="form-group col-md-6 <?php echo e($errors->has('first_name') ? 'has-error' : ''); ?>">
					<?php echo Form::label('first_name'); ?>

					<?php echo Form::text('first_name', $user->first_name, ['class' => 'form-control', 'maxlength' => '50']); ?>

					<span class="help-block"><?php echo e($errors->first('first_name', '')); ?></span>
				</div>
				<div class="form-group col-md-6 <?php echo e($errors->has('last_name') ? 'has-error' : ''); ?>">
					<?php echo Form::label('last_name'); ?>

					<?php echo Form::text('last_name', $user->last_name, ['class' => 'form-control', 'maxlength' => '50']); ?>

					<span class="help-block"><?php echo e($errors->first('last_name', '')); ?></span>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-6 <?php echo e($errors->has('company') ? 'has-error' : ''); ?>">
					<?php echo Form::label('company'); ?>

					<?php echo Form::text('company', $user->company, ['class' => 'form-control', 'maxlength' => '50']); ?>

					<span class="help-block"><?php echo e($errors->first('company', '')); ?></span>
				</div>
				<!--
				<div class="form-group col-md-6 <?php echo e($errors->has('job_title_id') ? 'has-error' : ''); ?>">
					<label for="job_title_id">Job title</label>
					<select class="form-control" data-placeholder="Choose sector" id="job_title" name="job_title_id">
						<?php $__currentLoopData = $jobTitles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jobTitle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($jobTitle->id); ?>" <?php echo e($jobTitle->id == $user->job_title_id ? 'selected'  : ''); ?>><?php echo e($jobTitle->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>
					<span class="help-block"><?php echo e($errors->first('job_title', '')); ?></span>
				</div>
				-->
			</div>
			<div class="form-group <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
				<?php echo Form::label('email'); ?>

				<?php echo Form::email('email', $user->email, ['class' => 'form-control', 'maxlength' => '191']); ?>

				<span class="help-block"><?php echo e($errors->first('email', '')); ?></span>
			</div>
			<div class="form-group <?php echo e($errors->has('region_id') ? 'has-error' : ''); ?>">
				<label for="region_id">Region</label>
				<select class="form-control" id="region_id" name="region_id">
					<?php $__currentLoopData = $regions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $region): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($region->id); ?>" <?php echo e($region->id == $user->region_id ? 'selected' : ''); ?>><?php echo e($region->name); ?></option>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
				<span class="help-block"><?php echo e($errors->first('region_id', '')); ?></span>
			</div>
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			<?php echo Form::submit('Submit', ['class' => 'btn btn-primary']); ?>

			<a href="<?php echo e(route('admin.users.index')); ?>" class="btn btn-default">Cancel</a>
		</div>
	<?php echo Form::close(); ?>

</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>