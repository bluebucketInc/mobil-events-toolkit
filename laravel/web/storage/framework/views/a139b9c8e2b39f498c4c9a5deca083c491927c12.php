<?php $__env->startSection('content'); ?>
<section class="padding" style="background: url('<?php echo e(asset('assets/images/bg-04.jpg')); ?>') no-repeat center center">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-mobil no-margin">
                    <div class="panel-body">
                        <p style="font-size: 24px;">Download event support material</p>
                        <p>Access and download relevant marketing materials for your event execution needs.</p>
                        <!--<button id="events-checklist" data-toggle="modal" data-target="#modal-event-checklist" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Events Checklist</button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('home')); ?>">Home</a></li>
            <li><a href="<?php echo e(route('assets')); ?>">Assets</a></li>
            <li class="active"><?php echo e($sector_label); ?></li>
        </ol>
    </div>
</section>

<section class="padding-bottom">
    <div class="container">

        <div class="bar bluebar">
            <div class="container-fluid">
                <div class="form-group searchbox pull-left">
                    <input type="text" class="form-control" id="search" placeholder="Search for files here" aria-describedby="inputSuccess2Status" value="<?php echo e($search); ?>">
                </div>
                <div class="pull-left">
                    <a id="search-icon" class="btn" href="#"><i class="glyphicon glyphicon-search" aria-hidden="true"></i></a>
                </div>
                <div class="view-buttons pull-right">
                    <a href="<?php echo e(route('assets.browse', ['sector_slug' => $sector->slug, 'region_id' => $region_id, 'view' => 'grid', 'sort' => $sort, 'fc' => $fc ])); ?>" class="btn btn-link <?php echo e($view == 'grid' ? 'active' : ''); ?>"><i class="fa fa-th-large"></i></a>
                    <a href="<?php echo e(route('assets.browse', ['sector_slug' => $sector->slug, 'region_id' => $region_id, 'view' => 'list', 'sort' => $sort, 'fc' => $fc ])); ?>" class="btn btn-link <?php echo e($view == 'list' ? 'active' : ''); ?>"><i class="fa fa-list"></i></a>
                </div>
                <div class="dropdown pull-right">
                    <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-link">
                        Filter <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><a href="<?php echo e(route('assets.browse', ['sector_slug' => $sector->slug, 'region_id' => $region_id, 'view' => $view, 'sort' => $sort, 'fc' => $category->slug ])); ?>"><span class="label" style="display: block; text-align: left; background-color: <?php echo e($category->label_color); ?>"><i class="fa <?php echo e($category->slug == $fc ? 'fa-check-square-o' : 'fa-square-o'); ?>"></i> <?php echo e($category->name); ?></span></a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <li class="divider"></li>
                        <li><a href="<?php echo e(route('assets.browse', ['sector_slug' => $sector->slug, 'region_id' => $region_id, 'view' => $view, 'sort' => $sort, 'fc' => 'all' ])); ?>">No filter</a></li>
                    </ul>
                </div>
                <div class="dropdown pull-right">
                    <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-link">
                        Sort <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li><a href="<?php echo e(route('assets.browse', ['sector_slug' => $sector->slug, 'region_id' => $region_id, 'view' => $view, 'sort' => 'category', 'fc' => $fc ])); ?>">Sort by Category</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?php echo e(route('assets.browse', ['sector_slug' => $sector->slug, 'region_id' => $region_id, 'view' => $view, 'sort' => 'newest', 'fc' => $fc ])); ?>">Sort by Newest</a></li>
                        <li><a href="<?php echo e(route('assets.browse', ['sector_slug' => $sector->slug, 'region_id' => $region_id, 'view' => $view, 'sort' => 'oldest', 'fc' => $fc ])); ?>">Sort by Oldest</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?php echo e(route('assets.browse', ['sector_slug' => $sector->slug, 'region_id' => $region_id, 'view' => $view, 'sort' => 'largest', 'fc' => $fc ])); ?>">Sort by Largest</a></li>
                        <li><a href="<?php echo e(route('assets.browse', ['sector_slug' => $sector->slug, 'region_id' => $region_id, 'view' => $view, 'sort' => 'smallest', 'fc' => $fc ])); ?>">Sort by Smallest</a></li>
                    </ul>
                </div>
                <div class="dropdown pull-right">
                    <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-link">
                        Region: <?php echo e($currentRegion->name); ?> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php $__currentLoopData = $regions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $region): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><a href="<?php echo e(route('assets.browse', ['sector_slug' => $sector->slug, 'region_id' => $region->id, 'view' => $view, 'sort' => $sort, 'fc' => $fc ])); ?>"><?php echo e($region->name); ?></a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>

            </div>
        </div>

        <div class="bar linebar">
            <div class="pull-left">
                <div class="linebar-info pull-left">
                    <span><span id="selected-asset-count"><?php echo e($newPackage ? $newPackage->assets->count() : '0'); ?></span> selected</span>
                    <span><a href="<?php echo e(route('new_package.deselect_all')); ?>">Deselect all</a></span>
                </div>
                <a class="btn btn-primary btn-sm" href="javascript: downloadAssets()" class="btn btn-primary"><i class="fa fa-download"></i> Download Assets</a>
                <!--<a class="btn btn-secondary btn-sm" href="#" class="btn btn-default" data-toggle="modal" data-target="#modal-booth-mockup">Booth Mockup</a>-->
                <!--
                <a class="btn btn-secondary btn-sm"href="#" class="btn btn-default">Old Packages</a>
                -->
            </div>
            <div class="pull-right"><?php echo e($assets->links()); ?></div>
            <div class="clearfix"></div>
        </div>

        <div class="row">
            <?php if($view == 'grid'): ?>
            <div class="asset-grid">
                <?php $__currentLoopData = $assets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $asset): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-3 col-sm-6">
                    <div class="panel card">
                        <div style="position: relative;">
                            <img class="img-responsive" src="<?php echo e(asset('uploads/event_assets/400x200-' . $asset->preview_filename)); ?>">
                            <div style="position: absolute; left: 5px; bottom: 5px;"><span class="label" style="background-color: <?php echo e($asset->category->label_color); ?>"><?php echo e($asset->category->name); ?></span></div>
                        </div>
                        <div class="panel-body">
                            <div class="card-info-title asset"><?php echo e($asset->name); ?></div>
                            <div clsas="card-info-created"><?php echo e($asset->created_at->format('j F Y \a\t h:i A')); ?></div>
                            <div class="card-info-filesize">
                                <?php if($asset->filesize < 1024): ?>
                                <?php echo e(round($asset->filesize, 1)); ?> B
                                <?php elseif($asset->filesize < 1048576): ?>
                                <?php echo e(round($asset->filesize / 1024, 1)); ?> KB
                                <?php elseif($asset->filesize < 1073741824): ?>
                                <?php echo e(round($asset->filesize / 1048576, 1)); ?> MB
                                <?php else: ?>
                                <?php echo e(round($asset->filesize / 1073741824, 1)); ?> GB
                                <?php endif; ?>
                            </div>
                            <div class="card-footer-buttons">
                                <?php if($asset->format == 'image'): ?>
                                <a href="<?php echo e(url('uploads/event_assets/' . $asset->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-popup"><i class="fa fa-eye"></i> Preview</a>
                                <?php elseif($asset->format == 'video'): ?>
                                <a href="<?php echo e(url('uploads/event_assets/' . $asset->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview preview-video" data-source="<?php echo e($asset->filename); ?>"><i class="fa fa-eye"></i> Preview</a>
                                
                                <?php elseif($asset->format == 'pdf'): ?>
                                <!-- Desktop -->
                                <a href="<?php echo e(url('uploads/event_assets/' . $asset->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-iframe hidden-xs hidden-sm hidden-md"><i class="fa fa-eye"></i> Preview</a>
                                <!-- Mobile -->
                                <a href="<?php echo e(url('uploads/event_assets/' . $asset->preview_filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-popup hidden-lg hidden-xl"><i class="fa fa-eye"></i> Preview</a>
                                
                                <?php elseif($asset->format == 'iframe'): ?>
                                <a href="<?php echo e(url('uploads/event_assets/' . $asset->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-iframe"><i class="fa fa-eye"></i> Preview</a>
                                <?php endif; ?>
                                <a href="#" data-asset-id="<?php echo e($asset->id); ?>" class="btn btn-sm btn-primary pull-right download" style="margin-right: 5px;"><i class="fa <?php echo e($newPackage ? ($newPackage->assets->contains($asset->id) ? 'fa-check-square-o' : 'fa-square-o') : 'fa-square-o'); ?>"></i> Download</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <?php elseif($view == 'list'): ?>
            <div class="asset-list">
                <?php $__currentLoopData = $assets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $asset): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-12">
                    <div class="panel panel-default listitem">
                        <div class="listbo">
                            <div class="listcol listcol-left">
                                <img class="img-responsive" src="<?php echo e(asset('uploads/event_assets/400x200-' . $asset->preview_filename)); ?>">
                            </div>
                            <div class="listcol listcol-right">
                                <span class="label" style="background-color: <?php echo e($asset->category->label_color); ?>"><?php echo e($asset->category->name); ?></span>
                                <div class="card-info-title asset"><?php echo e($asset->name); ?></div>
                                <div class="card-info-created"><?php echo e($asset->created_at->format('j F Y \a\t h:i A')); ?></div>
                                <div class="card-info-filesize">
                                    <?php if($asset->filesize < 1024): ?>
                                    <?php echo e(round($asset->filesize, 1)); ?> B
                                    <?php elseif($asset->filesize < 1048576): ?>
                                    <?php echo e(round($asset->filesize / 1024, 1)); ?> KB
                                    <?php elseif($asset->filesize < 1073741824): ?>
                                    <?php echo e(round($asset->filesize / 1048576, 1)); ?> MB
                                    <?php else: ?>
                                    <?php echo e(round($asset->filesize / 1073741824, 1)); ?> GB
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="listov">
                            <?php if($asset->format == 'image'): ?>
                            <a href="<?php echo e(url('uploads/event_assets/' . $asset->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-popup"><i class="fa fa-eye"></i> Preview</a>
                            <?php elseif($asset->format == 'video'): ?>
                            <a href="<?php echo e(url('uploads/event_assets/' . $asset->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview preview-video" data-source="<?php echo e($asset->filename); ?>"><i class="fa fa-eye"></i> Preview</a>

                            <?php elseif($asset->format == 'pdf'): ?>
                            <!-- Desktop -->
                            <a href="<?php echo e(url('uploads/event_assets/' . $asset->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-iframe hidden-xs hidden-sm hidden-md"><i class="fa fa-eye"></i> Preview</a>
                            <!-- Mobile -->
                            <a href="<?php echo e(url('uploads/event_assets/' . $asset->preview_filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-popup hidden-lg hidden-xl"><i class="fa fa-eye"></i> Preview</a>
                            
                            <?php elseif($asset->format == 'iframe'): ?>
                            <a href="<?php echo e(url('uploads/event_assets/' . $asset->filename)); ?>" class="btn btn-sm btn-secondary pull-right preview magnific-iframe"><i class="fa fa-eye"></i> Preview</a>
                            <?php endif; ?>
                            <a href="#" data-asset-id="<?php echo e($asset->id); ?>" class="btn btn-sm btn-primary pull-right download"><i class="fa <?php echo e($newPackage ? ($newPackage->assets->contains($asset->id) ? 'fa-check-square-o' : 'fa-square-o') : 'fa-square-o'); ?>"></i> Download</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('modal/event_checklist', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modal/booth_mockup', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modal/compressing_package', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modal/no_assets', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modal/video_content', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->startPush('scripts'); ?>
<script>

function downloadAssets()
{
    var count = parseInt($('#selected-asset-count').html());
    if (count > 0) {
        window.location.href = "<?php echo e(route('new_package.compress')); ?>";
    } else {
        $('#modal-no-assets').modal();
    }
}

$('.download').click(function(e) {
    e.preventDefault();
    button = $(this);
    assetId = $(this).data('asset-id');
    $.ajax({
        url: "<?php echo e(url('ajax/new-package/add-asset')); ?>/" + assetId,
        type: 'GET',
        success: function (data) { 
            if (data.change == 'add') {
                $(button).find('i.fa').removeClass('fa-square-o').addClass('fa-check-square-o');
            } else {
                $(button).find('i.fa').removeClass('fa-check-square-o').addClass('fa-square-o');
            }
            $('#selected-asset-count').html(data.total);
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
});

$('#search').on('keyup', function(e) {
    e.preventDefault();
    if (e.keyCode == 13) {
        doSearch();
    }
});
$('#search-icon').click(function(e) {
    e.preventDefault();
    doSearch();
});

function doSearch() {
    var search = $('#search').val();
    search = search.trim();
    if (search.length < 3) return;

    var url = "<?php echo route('assets.browse', ['sector_slug' => 'all', 'region_id' => 1, 'view' => 'grid', 'sort' => 'newest', 'fc' => 'all', 'search' => '_search_']); ?>";
    url = url.replace('_search_', search);
    window.location.href = url;
}

<?php if(Session::has('compress')): ?>
console.log('compressing');
$('#modal-compressing-package').modal();
<?php endif; ?>

$('.magnific-popup').magnificPopup({type:'image'});
$('.preview-video').click(function(e) {
	e.preventDefault();
	var source = "<?php echo e(asset('uploads/event_assets')); ?>/" + $(this).data('source');
	$('#modal-video-content').modal();
	$('#video-container').get(0).pause();
    $('#video-source').attr('src', source);
    $('#video-container').get(0).load();
});
$('.magnific-iframe').magnificPopup({
	type: 'iframe'
});

</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>