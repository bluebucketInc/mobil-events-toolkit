<?php $__env->startSection('content'); ?>
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('home')); ?>">Home</a></li>
            <li><a href="<?php echo e(route('events')); ?>">Events</a></li>
            <li><a href="<?php echo e(route('events.show', $invitation->event->id)); ?>"><?php echo e($invitation->event->name); ?></a></li>
            <li class="active">Invitation - Choose template</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="panel panel-mobil">
            <div class="panel-body">
                <div class="row">
                    <?php $__currentLoopData = $templates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $template): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-2">
                            <div class="box">
                                <div class="box-head">
                                    <a href="#" data-id="<?php echo e($template->id); ?>" class="template template-popup <?php echo e($invitation->email_template_id == $template->id ? 'selected' : ''); ?>">
                                        <img src="<?php echo e(asset('uploads/email_templates/t-' . $template->image_preview_filename)); ?>" class="img-responsive">
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <div class="panel-footer">
                <div class="pull-right">
                    <a href="#" id="next" class="btn btn-primary">Next</a>
                </div>
                <div clas="pull-left">
                    <a href="<?php echo e(route('events.show', $invitation->event_id)); ?>" class="btn btn-default">Back</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div> <!-- /.container -->
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>

$('.template').click(function(e) {
    e.preventDefault();
    $('.template').removeClass('selected');
    $(this).addClass('selected');
});

$('#next').click(function(e) {
    e.preventDefault();
    var templateId = $('.template.selected').data('id');
    var url = "<?php echo e(route('invitations.choose_template', ['id' => '_id_', 'template_id' => '_template_id_'])); ?>";
    console.log(url);
    url = url.replace('_id_', <?php echo e($invitation->id); ?>);
    url = url.replace('_template_id_', templateId);
    window.location = url;
});

</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>