<?php $__env->startSection('page-title', 'Sectors'); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
	<div class="col-md-6">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Create New Sector</h3>
			</div>
			<?php echo Form::open(['route' => 'admin.sectors.store', 'files' => true]); ?>

				<div class="box-body">
					
					<div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
						<?php echo Form::label('name'); ?>

						<?php echo Form::text('name', '', ['class' => 'form-control', 'maxlength' => '50']); ?>

						<span class="help-block"><?php echo e($errors->first('name', '')); ?></span>
					</div>
					<div class="form-group <?php echo e($errors->has('slug') ? 'has-error' : ''); ?>">
						<?php echo Form::label('slug'); ?>

						<?php echo Form::text('slug', '', ['class' => 'form-control', 'maxlength' => '50']); ?>

						<span class="help-block"><?php echo e($errors->first('slug', '')); ?></span>
					</div>
					<div class="form-group <?php echo e($errors->has('order') ? 'has-error' : ''); ?>">
						<?php echo Form::label('order'); ?>

						<?php echo Form::number('order', $new_order, ['class' => 'form-control']); ?>

						<span class="help-block"><?php echo e($errors->first('order', '')); ?></span>
					</div>
					<div class="form-group <?php echo e($errors->has('image') ? 'has-error' : ''); ?>">
						<?php echo Form::label('image', 'Image file'); ?>

						<?php echo Form::file('image', ['class' => 'form-control']); ?>

						<span class="help-block"><?php echo e($errors->first('image', '')); ?></span>
					</div>

				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<?php echo Form::submit('Submit', ['class' => 'btn btn-primary']); ?>

					<a href="<?php echo e(route('admin.sectors.index')); ?>" class="btn btn-default">Cancel</a>
				</div>
			<?php echo Form::close(); ?>

		</div>
		<!-- /.box -->
	</div>
	<!-- /.col-md-6 -->
</div>
<!-- /.row -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>