<?php $__env->startSection('content'); ?>
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('home')); ?>">Home</a></li>
            <li><a href="<?php echo e(route('events')); ?>">Events</a></li>
            <li><a href="<?php echo e(route('events.show', $invitation->event->id)); ?>"><?php echo e($invitation->event->name); ?></a></li>
            <li class="active">Invitation - Confirmation</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-mobil">
                    <div class="panel-heading">
                        <h3 class="panel-title">Confirm Before Sending</h3>
                    </div>
                    <div class="panel-body">
                        <p>Please ensure that the email you will be sending away is right!</p>
                    </div>
                    <div class="panel-footer">
                        <div class="pull-right">
                            <a href="#" id="test" data-toggle="modal" data-target="#modal-test-email" class="btn btn-secondary">Send test email</a>
                            <a href="#" id="send" class="btn btn-primary">Send</a>
                        </div>
                        <div class="pull-left">
                            <a href="<?php echo e(route('invitations.customize', $invitation->id)); ?>" class="btn btn-default">Back</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="panel panel-mobil">
                    <div class="edm-html-wrapper">
                        <?php echo $invitation->html_content; ?>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /.container -->
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('modal.test_email', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->startPush('scripts'); ?>
<script>


    $('#send').click(function(e) {
        e.preventDefault();

        var sendUrl = "<?php echo e(route('ajax.invitations.send', $invitation->id)); ?>";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            type: 'POST',
            url: sendUrl,
            datatype: 'json',
            success: function (data) { 
                window.location = "<?php echo e(route('events.show', $invitation->event_id)); ?>";
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    });
    
     $('#test').click(function(e) {
        e.preventDefault();

        var saveUrl = "<?php echo e(route('ajax.invitations.test', $invitation->id)); ?>";
        var saveData = {
            text_block_1: $('#text_block_1').val()
        };

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            type: 'POST',
            url: saveUrl,
            data: saveData,
            datatype: 'json',
            success: function (data) { 
                window.location = "<?php echo e(route('invitations.confirmation', $invitation->id)); ?>";
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    });
       
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>