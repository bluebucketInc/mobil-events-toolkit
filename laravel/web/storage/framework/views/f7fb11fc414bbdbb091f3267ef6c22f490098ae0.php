<?php $__env->startSection('page-title', 'Asset Categories'); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
	<div class="col-md-6">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Add New Asset Category</h3>
			</div>
			<?php echo Form::open(['route' => 'admin.assetcategories.store']); ?>

				<div class="box-body">
					
					<div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
						<?php echo Form::label('name'); ?>

						<?php echo Form::text('name', old('name'), ['class' => 'form-control', 'maxlength' => '50']); ?>

						<span class="help-block"><?php echo e($errors->first('name', '')); ?></span>
					</div>
					<div class="form-group <?php echo e($errors->has('slug') ? 'has-error' : ''); ?>">
						<?php echo Form::label('slug'); ?>

						<?php echo Form::text('slug', old('slug'), ['class' => 'form-control', 'maxlength' => '50']); ?>

						<span class="help-block"><?php echo e($errors->first('slug', '')); ?></span>
					</div>
					<div class="form-group <?php echo e($errors->has('order') ? 'has-error' : ''); ?>">
						<?php echo Form::label('order'); ?>

						<?php echo Form::number('order', $new_order, ['class' => 'form-control']); ?>

						<span class="help-block"><?php echo e($errors->first('order', '')); ?></span>
					</div>
					<div class="form-group <?php echo e($errors->has('label_color') ? 'has-error' : ''); ?>">
						<?php echo Form::label('label_color'); ?>

						<?php echo Form::text('label_color', old('label_color'), ['class' => 'form-control']); ?>

						<span class="help-block"><?php echo e($errors->first('label_color', '')); ?></span>
					</div>

				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<?php echo Form::submit('Submit', ['class' => 'btn btn-primary']); ?>

					<a href="<?php echo e(route('admin.assetcategories.index')); ?>" class="btn btn-default">Cancel</a>
				</div>
			<?php echo Form::close(); ?>

		</div>
		<!-- /.box -->
	</div>
	<!-- /.col-md-6 -->
</div>
<!-- /.row -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>