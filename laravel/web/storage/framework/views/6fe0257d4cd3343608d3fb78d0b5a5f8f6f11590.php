<?php $__env->startSection('page-title', 'Assets'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="<?php echo e(route('admin.assets.browse', [ ])); ?>">Event Galleries</a></li>
<li><a href="<?php echo e(route('admin.assets.browse', [ ])); ?>">Event Detail</a></li>
<li class="active">Edit</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="col-md-6">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Edit Asset</h3>
		</div>
		<?php echo Form::open(['method' => 'PUT', 'route' => ['admin.eventgallery.update', $gallery->id], 'files' => true]); ?>

			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
            <div class="form-group <?php echo e($errors->has('sector_id') ? 'has-error' : ''); ?>">
							<label for="sector_id">Events</label>
							<select class="form-control select2" data-placeholder="Choose event" id="event_id" name="event_id" onchange="selectEvent(this);">
								<option value=""></option>
								<?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option  event-date="<?php echo e($event->date." ".$event->time); ?>"  value="<?php echo e($event->id); ?>" <?php echo e($event->id == $gallery->event_id ? 'selected' : ''); ?>><?php echo e($event->name); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
							<span class="help-block"><?php echo e($errors->first('$event_id', '')); ?></span>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group <?php echo e($errors->has('sector_id') ? 'has-error' : ''); ?>">
							<label for="sector_id">Events Date</label>
							<?php echo Form::text('gallerytype_id', $gallery->event()->date.' '.$events[0]->time, ['id'=>'eventDateId', 'class' => 'form-control', 'disabled'=>'disabled']); ?>

						</div>
					</div>
				</div>

				<?php echo Form::hidden('gallerytype_id', '2', ['class' => 'form-control']); ?>

			  <?php echo Form::hidden('name', '', ['class' => 'form-control', 'maxlength' => '50', 'type'=>'hidden']); ?>


        <div class="form-group <?php echo e($errors->has('asset') ? 'has-error' : ''); ?>">
					<?php echo Form::label('asset', 'Gallery file'); ?>

					<?php echo Form::file('asset', ['class' => 'form-control', 'accept' => 'image/x-png,image/gif,image/jpeg,image/tif,image/tiff,video/mp4,application/zip,application/pdf,application/vnd.openxmlformats-officedocument.presentationml.presentation']); ?>

					<span class="help-block"><?php echo e($errors->first('asset', '')); ?></span>
				</div>
			</div> <!-- /.box-body -->

			<div class="box-footer">
				<?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

				<a href="<?php echo e(route('admin.eventgallery.list', [ 'event_id' => $gallery->event_id] )); ?>" class="btn btn-default">Cancel</a>
			</div>
		<?php echo Form::close(); ?>

	</div> <!-- /.box -->
</div> <!-- /.col-md-6 -->

<div class="col-md-6">
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">Gallery Media</h3>
		</div>
		<div class="box-body">
			<?php if($gallery->format == 'mp4' || $gallery->format == 'video'): ?>
			<video width="320" height="240" controls>
				<source src="<?php echo e(url('uploads/event_gallery/' . $gallery->filename)); ?>" type="video/mp4">
				Your browser does not support the video tag.
			</video>
			<?php elseif($gallery->format == 'pdf'): ?>dd
			<a href="<?php echo e(url('uploads/event_gallery/' . $gallery->filename)); ?>" class="magnific-iframe"> <?php echo e($gallery->file_extension); ?>dd
				<img alt="<?php echo e($event->name); ?>" src="<?php echo e(asset('assets/images/pdf-icon.jpg')); ?>" class="img-responsive">
			</a>
			<?php elseif($gallery->format == 'image'): ?>
			<a href="<?php echo e(url('uploads/event_gallery/' . $gallery->filename)); ?>" class="magnific-iframe">
				<img src="<?php echo e(asset('uploads/event_gallery/400x200-' . $gallery->preview_filename)); ?>" class="img-responsive">
			</a>
			<?php else: ?>
			<a href="<?php echo e(url('uploads/event_gallery/' . $gallery->filename)); ?>" class="magnific-iframe">
				<img alt="<?php echo e($event->name); ?>" src="<?php echo e(asset('assets/images/pdf-icon.jpg')); ?>" class="img-responsive">
			</a>

			<?php endif; ?>
		</div>
	</div> <!-- /.box -->
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
	$('.select2').select2();
	$('.magnific-iframe').magnificPopup({
		type: 'iframe'
	});

	function selectEvent(sel) {
		option = $('option:selected', sel).attr('event-date');
		$("#eventDateId").val(option);

	}
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>