
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<!-- Main Footer -->
		<footer class="main-footer">
			<!-- To the right -->
			<div class="pull-right hidden-xs">
				<?php echo e(env('APP_NAME')); ?>

			</div>
			<!-- Default to the left -->
			<strong>Copyright &copy; 2018 <a href="<?php echo e(url('/')); ?>"><?php echo e(env('APP_NAME')); ?></a>.</strong> All rights reserved.
		</footer>


	</div>
	<!-- ./wrapper -->

	<!-- REQUIRED JS SCRIPTS -->

	<script src="<?php echo e(asset('assets/vendor/jquery/jquery-3.3.1.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/jquery-ui/jquery-ui.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/jquery-magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/moment/min/moment.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/tinymce/tinymce.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/datatables/jquery.dataTables.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/datatables/dataTables.bootstrap.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/iCheck/icheck.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/vendor/select2/select2.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/adminlte/js/adminlte.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/js/admin.js')); ?>"></script>

	<?php echo $__env->yieldPushContent('scripts'); ?>

</body>
</html>