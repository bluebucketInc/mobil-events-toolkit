<?php $__env->startSection('content'); ?>
<section class="padding section-500" style="background: url('<?php echo e(asset('assets/images/bg-01.jpg')); ?>') no-repeat center center;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-mobil no-margin">
                    <div class="panel-body">
                        <h4><b>Verify Your Email Address</b></h4>
                        <?php if(session('resent')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(__('A fresh verification link has been sent to your email address.')); ?>

                        </div>
                        <?php endif; ?>
                        <p>Before proceeding, please check your email for a verification link.</p>
                        <p>If you did not receive the email, <a href="<?php echo e(route('verification.resend')); ?>"><?php echo e(__('click here to request another')); ?></a>.</p>
                        <hr>
                        <div class="row margin-top">
                            <div class="col-md-6">
                                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                    <?php echo csrf_field(); ?>
                                </form>
                                Go back to <a class="dropdown-item" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">login page</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/front/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>