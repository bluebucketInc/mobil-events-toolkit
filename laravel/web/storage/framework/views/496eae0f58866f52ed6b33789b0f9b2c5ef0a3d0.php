<?php $__env->startSection('page-title', 'Logs'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active">Logs</li>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="box">
	<div class="box-header">
		<h3 class="box-title">Logs</h3>
        <div class="box-tools">
        </div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="dataTables_wrapper form-inline dt-bootstrap">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped" id="table-pageList">
				        <thead>
				            <tr>
				                <th>Id</th>
				                <th>Time</th>
				                <th>Description</th>
				            </tr>
				        </thead>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.box-body -->
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>

$(function() {
    $('#table-pageList').DataTable({
        processing: true,
        serverSide: true,
        ajax: '<?php echo route('admin.logs.datatable'); ?>',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'created_at', name: 'created_at' },
            { data: 'description', name: 'description' },
        ]
    });
});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>