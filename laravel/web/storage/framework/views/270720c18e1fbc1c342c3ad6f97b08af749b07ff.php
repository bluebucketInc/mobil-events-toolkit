<?php $__env->startSection('content'); ?>
<section class="padding section-500" style="background: url('<?php echo e(asset('assets/images/bg-01.jpg')); ?>') no-repeat center center;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-mobil no-margin">
                    <?php echo Form::open(['route' => 'login']); ?>

                    <div class="panel-body">
                        <h1>Mobil Events Toolkit</h1>


                        <input id="recaptchaResponse" name="recaptcha_response" type="hidden">

                        <p><i>Set the stage for better business</i></p>
                        <br>
                        <p>From assets to CRM tools, the Mobil Events Toolkit is designed to help you create the perfect event, seamlessly.</p>
                        <p>Please sign in to get started.</p>
                        <p>Don't have an account? <a href="<?php echo e(route('register')); ?>">Register here</a>.</p>
                        <p>If you are an EM administrator, please click <a href="<?php echo e(route('admin.auth.login')); ?>">here</a> to access the Admin Portal.</p>
                        <br>
                        </p>
                        <div class="form-group <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('email'); ?>

                            <?php echo Form::email('email', old('email'), [ 'class' => 'form-control', 'placeholder' => 'Your email', 'required' => 'required', 'autofocus' => 'autofocus' ]); ?>

                            <span class="help-block"><?php echo e($errors->first('email', '')); ?></span>
                        </div>
                        <div class="form-group <?php echo e($errors->has('password') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('password'); ?>

                            <?php echo Form::password('password', [ 'class' => 'form-control', 'placeholder' => 'Your password', 'required' => 'required' ]); ?>

                            <span class="help-block"><?php echo e($errors->first('password', '')); ?></span>
                        </div>
                        <?php echo Form::submit('Submit', ['class' => 'btn btn-primary btn-block']); ?>

                        <div class="row margin-top">
                            <div class="col-md-6">
                                <a href="<?php echo e(route('password.request')); ?>">Forgot password?</a>
                            </div>
                            <div class="col-md-6 text-right">
                                <!--<a href="#">Legal notice</a>-->
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $__env->make('modal.register_consent', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php if(env('SITE_KEY')): ?>
    <script src='https://www.google.com/recaptcha/api.js?render=<?php echo e(env('SITE_KEY')); ?>&onload=onloadCallback&render=explicit'></script>
   	<script>
        function onloadCallback() {
	      grecaptcha.ready(function() {
	        grecaptcha.execute('<?php echo e(env('SITE_KEY')); ?>', {
	          action: 'login',
	        }).then(function (token) {
	        	var recaptchaResponse = document.getElementById('recaptchaResponse');
	        	recaptchaResponse.value = token;
	        });
	      });
	    }
    </script>
<?php endif; ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/front/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>