<?php $__env->startSection('content'); ?>
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('home')); ?>">Home</a></li>
            <li><a href="<?php echo e(route('events')); ?>">Events</a></li>
            <li class="active">Add new event</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-mobil">
                    <?php echo Form::open(['route' => 'events.store']); ?>

                    <div class="panel-heading">
                        <h3 class="panel-title"><b>Event Form</b></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('name'); ?>

                                <?php echo Form::text('name', old('name'), ['class' => 'form-control', 'maxlength' => '191']); ?>

                                <span class="help-block"><?php echo e($errors->first('name', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 <?php echo e($errors->has('date') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('date'); ?>

                                 <div class='input-group date' id='datepicker'>
                                    <input name="date" type='text' class="form-control" value="<?php echo e(old('date')); ?>" />
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                                <span class="help-block"><?php echo e($errors->first('date', '')); ?></span>
                            </div>
                            <div class="form-group col-md-6 <?php echo e($errors->has('time') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('time'); ?>

                                 <div class='input-group date' id='timepicker'>
                                    <input name="time" type='text' class="form-control" value="<?php echo e(old('time')); ?>" />
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                                <span class="help-block"><?php echo e($errors->first('time', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('location') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('location'); ?>

                                <?php echo Form::text('location', old('location'), ['class' => 'form-control', 'maxlength' => '191']); ?>

                                <span class="help-block"><?php echo e($errors->first('location', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 <?php echo e($errors->has('sector_id') ? 'has-error' : ''); ?>">
                                <label for="sector_id">Sector</label>
                                <select class="form-control select2" data-placeholder="Choose sector" id="sector_id" name="sector_id">
                                    <option></option>
                                    <?php $__currentLoopData = $sectors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sector): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($sector->id); ?>" <?php echo e(old('sector_id') == $sector->id ? ' selected' : ''); ?>><?php echo e($sector->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <span class="help-block"><?php echo e($errors->first('sector_id', '')); ?></span>
                            </div>
                        </div>
                    </div> <!-- ./panel-body -->
                    
                    <div class="panel-footer with-border">
                        <div class="pull-right">
			                <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
$('#datepicker').datetimepicker({
    format: 'YYYY-MM-DD'
});
$('#timepicker').datetimepicker({
    format: 'LT'
});
$('.select2').select2();
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>