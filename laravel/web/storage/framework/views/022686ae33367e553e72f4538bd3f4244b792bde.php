<?php $__env->startSection('content'); ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h1 class="text-center">Contact Us</h1>
                <p>For any general questions regarding the toolkit, or website technical matters, please contact<br />Janaine Natalie Eng: <a href="mailto:janaine.eng@bbdo.com.sg">janaine.eng@bbdo.com.sg</a>&nbsp;<br />Kenji Leow: <a href="mailto:kenji.leow@bbdo.com.sg">kenji.leow@bbdo.com.sg</a></p>
                <p>&nbsp;</p>
                <p>For distributors, please kindly email your respective ExxonMobil field marketing contacts as follows.&nbsp;&nbsp;</p>
                <p><span style="text-decoration: underline;"><strong>India</strong></span></p>
                <p>Imtiaz Sayed: <a href="mailto:imtiaz.ahmed@exxonmobil.com">imtiaz.ahmed@exxonmobil.com</a><a href="mailto:imtiaz.ahmed@exxonmobil.com"><br /></a>Sayed Aweze: <a href="mailto:sayed.aweze@exxonmobil.com">sayed.aweze@exxonmobil.com</a><a href="mailto:sayed.aweze@exxonmobil.com"><br /></a>Smita Mane: <a href="mailto:smita.mane@exxonmobil.com">smita.mane@exxonmobil.com</a></p>
                <p><span style="text-decoration: underline;"><strong>FDS</strong></span></p>
                <p>Desmond Chan: <a href="mailto:desmond-benghui.chan@exxonmobil.com">desmond-benghui.chan@exxonmobil.com</a><a href="mailto:desmond-benghui.chan@exxonmobil.com"><br /></a>Amber Lim: <a href="mailto:amber.lim@exxonmobil.com">amber.lim@exxonmobil.com<br /></a>Cheryl Kok: <a href="mailto:cheryl.kok@exxonmobil.com">cheryl.kok@exxonmobil.com</a><a href="mailto:cheryl.kok@exxonmobil.com"><br /></a>Shaun Ramadan: <a href="mailto:shaun.ramadan@exxonmobil.com">shaun.ramadan@exxonmobil.com</a></p>
                <p><span style="text-decoration: underline;"><strong>Thailand</strong></span>&nbsp;</p>
                <p>Amran Ma: <a href="mailto:amran.ma@exxonmobil.com">amran.ma@exxonmobil.com</a><a href="mailto:amran.ma@exxonmobil.com"><br /></a>Kitphinyochai, Walynntip: <a href="mailto:walynntip.kitphinyochai@exxonmobil.com">walynntip.kitphinyochai@exxonmobil.com</a><a href="mailto:walynntip.kitphinyochai@exxonmobil.com"><br /></a>Kantharak, Piyaratwadee: <a href="mailto:piyaratwadee.kantharak@exxonmobil.com">piyaratwadee.kantharak@exxonmobil.com</a><a href="mailto:piyaratwadee.kantharak@exxonmobil.com"><br /></a>Philippa Penson: <a href="mailto:philippa.penson@exxonmobil.com">philippa.penson@exxonmobil.com</a></p>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>