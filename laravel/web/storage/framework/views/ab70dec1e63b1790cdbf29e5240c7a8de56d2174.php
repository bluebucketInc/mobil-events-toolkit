<?php $__env->startSection('content'); ?>
<section class="padding" style="background: url('<?php echo e(asset('assets/images/bg-04.jpg')); ?>') no-repeat center center">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-mobil no-margin">
                    <div class="panel-body">
                        <p style="font-size: 24px;">Hi <strong><?php echo e(\Auth::user()->name()); ?></strong>,</p>
                        <p>Everything you need to set up and run a successful event is now at your fingertips. Connect with your customers, ensure your event is on track and download tradeshow brand materials here.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="padding">
    <div class="container container-sm">
        <h2 class="text-center">What would you like to do?</h2>
        <br><br>
        <div class="row">
            <div class="col-md-3 menu-column text-center">
                <a href="<?php echo e(route('customers')); ?>">
                    <img class="img-responsive img-circle" src="<?php echo e(asset('assets/images/img-customers.jpg')); ?>">
                    <h3>Manage Your Customers</h3>
                </a>
                <hr>
                <p>Access and manage customer registrations from your events.</p>
            </div>
            <div class="col-md-3 menu-column text-center">
                <a href="<?php echo e(route('events')); ?>">
                    <img style="width:100%;" class="img-responsive img-circle" src="<?php echo e(asset('assets/images/img-events.jpg')); ?>">
                    <h3>Organize Your Events</h3>
                </a>
                <hr>
                <p>Organise and track your list of events. Also, keep customers informed about upcoming events using our email templates.</p>
            </div>
            <div class="col-md-3 menu-column text-center">
                <a href="<?php echo e(route('assets')); ?>">
                    <img class="img-responsive img-circle" src="<?php echo e(asset('assets/images/img-assets.jpg')); ?>">
                    <h3>Download Event Assets</h3>
                </a>
                <hr>
                <p>Access and download relevant marketing materials for your event execution needs.</p>
            </div>
            <div class="col-md-3 menu-column text-center">
                <a href="<?php echo e(route('event-gallery')); ?>">
                    <img class="img-responsive img-circle" src="<?php echo e(asset('assets/images/img-gallery.jpg')); ?>">
                    <h3>&nbsp; &nbsp;&nbsp;Events Gallery &nbsp;&nbsp;&nbsp;</h3>
                </a>
                <hr>
                <p>View photos from other events.</p>
            </div>

        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>