<?php $__env->startSection('page-title', 'Customers'); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
	<div class="col-md-6">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Show Customer</h3>
			</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<?php echo Form::label('first_name'); ?>

								<?php echo Form::text('first_name', $customer->first_name, ['class' => 'form-control', 'maxlength' => '50', 'readonly']); ?>

							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<?php echo Form::label('last_name'); ?>

								<?php echo Form::text('last_name', $customer->last_name, ['class' => 'form-control', 'maxlength' => '191', 'readonly']); ?>

							</div>
						</div>
					</div>
					<div class="form-group">
						<?php echo Form::label('email'); ?>

						<?php echo Form::email('email', $customer->email, ['class' => 'form-control', 'maxlength' => '191', 'readonly']); ?>

					</div>
					<div class="form-group">
						<?php echo Form::label('company'); ?>

						<?php echo Form::text('company', $customer->company, ['class' => 'form-control', 'maxlength' => '100', 'readonly']); ?>

					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="sector_id">Sector</label>
								<select class="form-control select2" id="sector_id" name="sector_id">
									<?php $__currentLoopData = $sectors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sector): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($sector->id); ?>" <?php echo e($sector->id == $customer->sector_id ? 'selected' : ''); ?>><?php echo e($sector->name); ?></option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
							</div>
							
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="job_title">Job title</label>
								<select class="form-control select2" id="job_title" name="job_title">
									<?php $__currentLoopData = $jobTitles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jobTitle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($jobTitle->id); ?>" <?php echo e($jobTitle->id == $customer->job_title_id ? 'selected' : ''); ?>><?php echo e($jobTitle->name); ?></option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<a href="<?php echo e(route('admin.customers.index')); ?>" class="btn btn-default">Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>