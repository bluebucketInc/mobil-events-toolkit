<?php $__env->startSection('content'); ?>
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('home')); ?>">Home</a></li>
            <li><a href="<?php echo e(route('customers')); ?>">Customers</a></li>
            <li class="active">Add new customer</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-mobil">
                    <?php echo Form::open(['route' => 'customers.store']); ?>

                    <div class="panel-heading">
                        <h3 class="panel-title"><b>Customer Contact Form</b></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group col-md-6 <?php echo e($errors->has('first_name') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('first_name'); ?>

                                <?php echo Form::text('first_name', old('first_name'), ['class' => 'form-control', 'maxlength' => '191']); ?>

                                <span class="help-block"><?php echo e($errors->first('first_name', '')); ?></span>
                            </div>
                            <div class="form-group col-md-6 <?php echo e($errors->has('last_name]') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('last_name'); ?>

                                <?php echo Form::text('last_name', old('last_name'), ['class' => 'form-control', 'maxlength' => '191']); ?>

                                <span class="help-block"><?php echo e($errors->first('last_name', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('email'); ?>

                                <?php echo Form::text('email', old('email'), ['class' => 'form-control', 'maxlength' => '191']); ?>

                                <span class="help-block"><?php echo e($errors->first('email', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('company') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('company'); ?>

                                <?php echo Form::text('company', old('company'), ['class' => 'form-control', 'maxlength' => '191']); ?>

                                <span class="help-block"><?php echo e($errors->first('company', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 <?php echo e($errors->has('sector_id') ? 'has-error' : ''); ?>">
                                <label for="sector_id">Sector</label>
                                <select class="form-control select2" data-placeholder="Choose sector" id="sector_id" name="sector_id">
                                    <option></option>
                                    <?php $__currentLoopData = $sectors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sector): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($sector->id); ?>" <?php echo e(old('sector_id') == $sector->id ? ' selected' : ''); ?>><?php echo e($sector->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <span class="help-block"><?php echo e($errors->first('sector_id', '')); ?></span>
                            </div>
                            <div class="form-group col-md-6 <?php echo e($errors->has('job_title_id') ? 'has-error' : ''); ?>">
                                <label for="job_title_id">Job title</label>
                                <select class="form-control select2" data-placeholder="Choose job title" id="job_title_id" name="job_title_id">
                                    <option></option>
                                    <?php $__currentLoopData = $jobTitles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jobTitle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($jobTitle->id); ?>" <?php echo e(old('job_title_id') == $jobTitle->id ? ' selected' : ''); ?>><?php echo e($jobTitle->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <span class="help-block"><?php echo e($errors->first('job_title_id', '')); ?></span>
                            </div>
                        </div>
                        <!--
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('check_permission') ? 'has-error' : ''); ?>">
                                <div class="checkbox">
                                    <label><input name="check_permission" type="checkbox"> This person agrees to receive marketing emails from ExxonMobil from time to time.</label>
                                    <a href="#" data-toggle="modal" data-target="#modal-customer-consent">What's this?</a>
                                    <span class="help-block"><?php echo e($errors->first('check_permission', '')); ?></span>
                                </div>
                            </div>
                        </div>
                        -->
                        <div class="row">
                            <ul>
                                <li>This form is meant for <b>Distributors</b> to fill up only.</li>
                                <li>For Customer's On-site Registration, please use the <a href="<?php echo e(route('leadform.form', ['user_code' => $user->code])); ?>" target="_blank">On-site Registration Form</a></li>
                            </ul>
                        </div>
                    </div> <!-- ./panel-body -->
                    
                    <div class="panel-footer with-border">
                        <div class="pull-right">
			                <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('modal.customer_consent', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
$('.select2').select2();
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>