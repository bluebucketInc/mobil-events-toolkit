<?php $__env->startSection('content'); ?>
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('home')); ?>">Home</a></li>
            <li><a href="<?php echo e(route('customers')); ?>">Customers</a></li>
            <li class="active">Import customers</li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-mobil">
                    <?php echo Form::open(['route' => 'customers.upload_excel', 'files' => true]); ?>

                    <div class="panel-heading">
                        <h3 class="panel-title"><b>Import Customers</b></h3>
                    </div>
                    <div class="panel-body">
                        <p><i>Please upload excel file by using this <a href="<?php echo e(asset('assets/templates/customers-import-template.xlsx')); ?>">template</a></p>
                        <div class="form-group <?php echo e($errors->has('file') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('file', 'Select file to upload'); ?>

                            <?php echo Form::file('file', ['class' => 'form-control', 'accept' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel']); ?>

                            <span class="help-block"><?php echo e($errors->first('file', '')); ?></span>
                        </div>
                    </div> <!-- ./panel-body -->
                    
                    <div class="panel-footer with-border">
                        <div class="pull-right">
			                <?php echo Form::submit('Import', ['class' => 'btn btn-primary']); ?>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('modal.customer_consent', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>