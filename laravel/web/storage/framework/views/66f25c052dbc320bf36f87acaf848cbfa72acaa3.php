<?php $__env->startSection('page-title', 'Invitations'); ?>

<?php $__env->startSection('content'); ?>

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Create New Invitation</h3>
	</div>
	<?php echo Form::open(['route' => 'admin.invitations.store', 'files' => true]); ?>

		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="city_id">City</label>
						<select class="form-control select2" data-placeholder="Choose city" id="email_template_id" name="email_template_id">
							<?php $__currentLoopData = $templates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $template): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($template->id); ?>" <?php if(old('email_template_id') == $template->id): ?> <?php echo e('selected'); ?> <?php endif; ?>><?php echo e($template->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>
				</div>
				<!-- /.col-md-6 -->
				<div class="col-md-6"></div>
			</div>
			
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			<?php echo Form::submit('Submit', ['class' => 'btn btn-primary']); ?>

			<a href="<?php echo e(route('admin.invitations.index')); ?>" class="btn btn-default">Cancel</a>
		</div>
	<?php echo Form::close(); ?>

</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>