<?php $__env->startSection('page-title', 'Change Password'); ?>

<?php $__env->startSection('content'); ?>
	
<div class="row">
	<div class="col-md-4">

		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Change my password</h3>
			</div>
		
			<div class="box-body">
				
				<?php echo Form::open(['route' => 'admin.password.update', 'method' => 'PUT']); ?>

				<div class="form-group has-feedback">
				<div class="form-group has-feedback">
					<?php echo Form::password('old_password', [ 'class' => 'form-control', 'placeholder' => 'Current Password', 'maxlength' => '60' ]); ?>

					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<?php echo Form::password('new_password', [ 'class' => 'form-control', 'placeholder' => 'New Password', 'maxlength' => '60' ]); ?>

					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<?php echo Form::password('confirm_password', [ 'class' => 'form-control', 'placeholder' => 'Confirm Password', 'maxlength' => '60' ]); ?>

					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-4">
						<a href="<?php echo e(url('admin')); ?>" class="btn btn-default btn-block btn-flat">Cancel</a>
					</div>
		
					<div class="col-xs-4"></div>
		
					<div class="col-xs-4">
						<?php echo Form::submit('Change', [ 'class' => 'btn btn-primary btn-block btn-flat' ]); ?>	
					</div>
				</div>
			<?php echo Form::close(); ?>

		
			</div>
		</div>
	</div>

</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>