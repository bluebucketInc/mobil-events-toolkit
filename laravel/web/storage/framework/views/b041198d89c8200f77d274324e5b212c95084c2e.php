<?php $__env->startSection('content'); ?>
<section style="background: url(<?php echo e(asset('assets/images/bg-03.jpg')); ?>) no-repeat center center; background-size: cover;">
    <div class="container" style="padding-top: 20px;">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-mobil">
                    <?php echo Form::open(['route' => ['leadform.submit', $user->code]]); ?>

                    <div class="panel-heading">
                        <h3 class="panel-title"><b>On-Site Registration Form</b></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group col-md-6 <?php echo e($errors->has('first_name') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('first_name'); ?>

                                <?php echo Form::text('first_name', old('first_name'), ['class' => 'form-control', 'maxlength' => '191']); ?>

                                <span class="help-block"><?php echo e($errors->first('first_name', '')); ?></span>
                            </div>
                            <div class="form-group col-md-6 <?php echo e($errors->has('last_name') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('last_name'); ?>

                                <?php echo Form::text('last_name', old('last_name'), ['class' => 'form-control', 'maxlength' => '191']); ?>

                                <span class="help-block"><?php echo e($errors->first('first_name', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('email'); ?>

                                <?php echo Form::text('email', old('email'), ['class' => 'form-control', 'maxlength' => '191']); ?>

                                <span class="help-block"><?php echo e($errors->first('first_name', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 <?php echo e($errors->has('company') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('company'); ?>

                                <?php echo Form::text('company', old('company'), ['class' => 'form-control', 'maxlength' => '191']); ?>

                                <span class="help-block"><?php echo e($errors->first('first_name', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 <?php echo e($errors->has('sector_id') ? 'has-error' : ''); ?>">
                                <label for="sector_id">Sector</label>
                                <select class="form-control select2" data-placeholder="Choose sector" id="sector_id" name="sector_id">
                                    <option></option>
                                    <?php $__currentLoopData = $sectors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sector): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($sector->id); ?>" <?php echo e(old('sector_id') == $sector->id ? 'selected' : ''); ?>><?php echo e($sector->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <span class="help-block"><?php echo e($errors->first('sector_id', '')); ?></span>
                            </div>
                            <div class="form-group col-md-6 <?php echo e($errors->has('job_title_id') ? 'has-error' : ''); ?>">
                                <label for="job_title_id">Job title</label>
                                <select class="form-control select2" data-placeholder="Choose job title" id="job_title_id" name="job_title_id">
                                    <option></option>
                                    <?php $__currentLoopData = $jobTitles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jobTitle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($jobTitle->id); ?>" <?php echo e(old('job_title_id') == $jobTitle->id ? 'selected' : ''); ?>><?php echo e($jobTitle->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <span class="help-block"><?php echo e($errors->first('job_title_id', '')); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <?php echo e($errors->has('check_permission') ? 'has-error' : ''); ?>">
                                    <div class="checkbox">
                                        <label><input name="check_permission" type="checkbox"> I have read and acknowledged Mobil’s <a href="<?php echo e(url('privacy-policy')); ?>" target="_blank">Privacy Policy</a> statement</label>
                                    </div>
                                    <span class="help-block"><?php echo e($errors->first('check_permission', '')); ?></span>
                                </div>
                                <div class="form-group <?php echo e($errors->has('check_receive') ? 'has-error' : ''); ?>">
                                    <div class="checkbox">
                                        <label><input name="check_receive" type="checkbox"> I agree to receiving periodic communications from ExxonMobil</label>
                                    </div>
                                    <span class="help-block"><?php echo e($errors->first('check_receive', '')); ?></span>
                                </div>
                            </div>
                        </div>
                    </div> <!-- ./panel-body -->
                    
                    <div class="panel-footer with-border">
                        <div class="pull-right">
			                <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
            <div class="col-md-6"></div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('modal.customer_consent', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.front.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>