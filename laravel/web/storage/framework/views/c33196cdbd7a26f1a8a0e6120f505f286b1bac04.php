<?php $__env->startSection('page-title', 'Users'); ?>

<?php $__env->startSection('content'); ?>

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Edit Admin</h3>
	</div>
	<?php echo Form::open(['method' => 'PUT', 'route' => ['admin.admins.update', $admin->id]]); ?>

		<div class="box-body">
			<div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
				<?php echo Form::label('name'); ?>

				<?php echo Form::text('name', $admin->name, ['class' => 'form-control', 'maxlength' => '50']); ?>

				<span class="help-block"><?php echo e($errors->first('name', '')); ?></span>
			</div>
			<div class="form-group <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
				<?php echo Form::label('email'); ?>

				<?php echo Form::email('email', $admin->email, ['class' => 'form-control', 'maxlength' => '191']); ?>

				<span class="help-block"><?php echo e($errors->first('email', '')); ?></span>
			</div>
			<div class="form-group <?php echo e($errors->has('role') ? 'has-error' : ''); ?>">
				<?php echo Form::label('role'); ?>

				<?php echo Form::select('role', ['Super Admin' => 'Super Admin', 'Normal Admin' => 'Normal Admin'], $admin->role, ['class' => 'form-control']); ?>

				<span class="help-block"><?php echo e($errors->first('role', '')); ?></span>
			</div>
			<div class="form-group <?php echo e($errors->has('region_id') ? 'has-error' : ''); ?>">
				<label for="region_id">Region</label>
				<select class="form-control" id="region_id" name="region_id">
					<?php $__currentLoopData = $regions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $region): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($region->id); ?>" <?php echo e($region->id == $admin->region_id ? 'selected' : ''); ?>><?php echo e($region->name); ?></option>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
				<span class="help-block"><?php echo e($errors->first('region_id', '')); ?></span>
			</div>
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			<?php echo Form::submit('Submit', ['class' => 'btn btn-primary']); ?>

			<a href="<?php echo e(route('admin.admins.index')); ?>" class="btn btn-default">Cancel</a>
		</div>
	<?php echo Form::close(); ?>

</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>