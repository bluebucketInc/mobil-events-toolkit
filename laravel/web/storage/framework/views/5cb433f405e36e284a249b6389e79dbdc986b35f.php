<?php $__env->startSection('page-title', 'Events'); ?>
<?php $__env->startSection('breadcrumb'); ?>
<li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="<?php echo e(route('admin.eventgallery.list')); ?>">Event Galleries</a></li>
<li class="active">Galleries</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-body">
				<table class="table table-tight table-noborder">
					<tr>
						<td>Name</td>
						<td>:</td>
						<td><?php echo e($event->name); ?></td>
					</tr>
					<tr>
						<td>Date</td>
						<td>:</td>
						<td><?php echo e($event->date); ?></td>
					</tr>
					<tr>
						<td>Time</td>
						<td>:</td>
						<td><?php echo e($event->time); ?></td>
					</tr>
					<tr>
						<td>Location</td>
						<td>:</td>
						<td><?php echo e($event->location); ?></td>
					</tr>
					<tr>
						<td>Sector</td>
						<td>:</td>
						<td><?php echo e($event->sector->name); ?></td>
					</tr>
				</table>
			</div> <!-- ./box-body -->
		</div>
	</div>
</div>

<h3 class="box-title">Event Photos <a href="<?php echo e(route('admin.eventgallery.create', [$event->id])); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add new gallery</a></h3>


<div class="row asset-grid">
	<?php $__currentLoopData = $galleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php if($gallery->gallery_type == 2): ?>
		<div class="col-md-3 col-sm-6">
			<div class="box asset-grid-item">
				<div class="box-head">
					<?php if($gallery->format == 'image'): ?>
					<a href="<?php echo e(url('uploads/event_gallery/' . $gallery->filename)); ?>" class="magnific-popup">
						<img src="<?php echo e(asset('uploads/event_gallery/400x200-' . $gallery->preview_filename)); ?>" class="img-responsive">
					</a>
					<?php elseif($gallery->format == 'video'): ?>
					<a href="#" data-source="<?php echo e($gallery->filename); ?>" class="preview-video">
						<img src="<?php echo e(asset('assets/images/video-icon.jpg')); ?>" class="img-responsive">
					</a>
					<?php elseif($gallery->format == 'iframe'): ?>
					<a href="<?php echo e(url('uploads/event_gallery/' . $gallery->filename)); ?>" class="magnific-iframe">
						<img src="<?php echo e(asset('assets/images/pdf-icon.jpg')); ?>" class="img-responsive">
					</a>
					<?php endif; ?>
				</div>
				<div class="box-footer">
					<h5><?php echo e($gallery->name); ?></h5>
					<div class="btn-group pull-right">
						<a href="<?php echo e(route('admin.eventgallery.edit', ['id' => $gallery->id ])); ?>" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i> Edit</a>
						<a href="#" class="btn btn-danger btn-xs" onclick="event.preventDefault(); confirmDelete(<?php echo e($gallery->id); ?>)"><i class="fa fa-trash"></i> Delete</a>
						<?php echo Form::open( [ 'method' => 'DELETE', 'route' => [ 'admin.eventgallery.destroy', $gallery->id ], 'id' => 'form-asset-delete-' . $gallery->id ] ); ?>

						<?php echo Form::close(); ?>

					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

<?php echo $__env->make('modal/video_content', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<?php $__env->startPush('styles'); ?>
<style>
a.magnific-popup {
	cursor: zoom-in;
}
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script>

function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this record?')) {
		$('#form-asset-delete-' + id).submit();
	}
}

$(function() {
	$('input[type="checkbox"].minimal').iCheck({
		checkboxClass: 'icheckbox_minimal-blue'
	});

})

$('.magnific-popup').magnificPopup({type:'image'});
$('.preview-video').click(function(e) {
	e.preventDefault();
	var source = "<?php echo e(asset('uploads/event_gallery')); ?>/" + $(this).data('source');
	$('#modal-video-content').modal();
	$('#video-container').get(0).pause();
    $('#video-source').attr('src', source);
    $('#video-container').get(0).load();
});
$('.magnific-popup-player').magnificPopup({
	items: {
		//src: '<video class="white-popup" src="http://localhost/extoolkit/public/uploads/event_assets/KyxFUW7amq953NFxkAgW.mp4"></video>',
		src: '<video id="video-container" controls><source id="video-source" src="/extoolkit/public/uploads/event_gallery/KyxFUW7amq953NFxkAgW.mp4" type="video/mp4"> Your browser does not support the video tag.</video>',
		type: 'inline'
	}
});
$('.magnific-iframe').magnificPopup({
	type: 'iframe'
});
$('.magnific-video').magnificPopup({
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: false,
    iframe: {
        markup: '<div class="mfp-iframe-scaler">'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
              '</div>',

        srcAction: 'iframe_src',
        }
});


</script>
<?php $__env->stopPush(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>