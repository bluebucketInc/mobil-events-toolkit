<?php $__env->startSection('content'); ?>
<section>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('home')); ?>">Home</a></li>
            <li><a href="<?php echo e(route('events')); ?>">Events</a></li>
            <li class="active"><?php echo e($event->name); ?></li>
        </ol>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-mobil">
                    <div class="panel-heading">
                        <h3 class="panel-title">Email Invitations</h3>
                        <div class="panel-tools pull-right">
                            <a href="<?php echo e(route('events.collect_invitations_stats', $event->id)); ?>" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Refresh</a>
                            <a href="<?php echo e(route('events.create_invitation', $event->id)); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Email communication</a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>Created date</th>
                                <!--<th>Recipients</th>-->
                                <th>Sent date</th>
                                <th>Sents</th>
                                <th>Opens</th>
                                <th>Clicks</th>
                                <th>Bounce</th>
                                <th></th>
                            <tr>
                            <?php $__currentLoopData = $event->invitations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $invitation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e(date('j M y h:i A', strtotime($invitation->created_at))); ?></td>
                                <!--<td><a href="<?php echo e(route('customers', ['fc' => 'a', 'fs' => $event->sector_id ])); ?>" target="_blank"><?php echo e($invitation->stat_recipients > 0 ? $invitation->stat_recipients : ''); ?></a></td>-->
                                <td><?php echo e($invitation->sent_at ? date('j M y h:i A', strtotime($invitation->sent_at)) : ''); ?></td>
                                <td><?php echo e($invitation->stat_sents > 0 ? $invitation->stat_sents : ''); ?></td>
                                <td><?php echo e($invitation->stat_opens > 1 ? $invitation->stat_opens : ''); ?></td>
                                <td><?php echo e($invitation->stat_clicks > 0 ? $invitation->stat_clicks : ''); ?></td>
                                <td><?php echo e($invitation->stat_softbounces > 0 ? $invitation->stat_softbounces : ''); ?></td>
                                <td>
                                    <?php if($invitation->sent): ?>
                                    <!--<a href="#" class="btn btn-default btn-xs btn-flat">View</a>-->
                                    <?php else: ?>
                                    <a href="<?php echo e(route('invitations.template', $invitation->id)); ?>" class="btn btn-default btn-xs btn-flat">Edit</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </table>
                    </div> <!-- ./panel-body -->
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-mobil">
                    <div class="panel-heading">
                        <h3 class="panel-title">Event Information</h3>
                        <div class="panel-tools pull-right">
                            <a href="<?php echo e(route('events.edit', ['id' => $event->id])); ?>" class="btn btn-default btn-sm"><i class="fa fa-edit"></i> Edit</a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-tight table-noborder">
                            <tr>
                                <td>Name</td>
                                <td>:</td>
                                <td><?php echo e($event->name); ?></td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>:</td>
                                <td><?php echo e($event->date); ?></td>
                            </tr>
                            <tr>
                                <td>Time</td>
                                <td>:</td>
                                <td><?php echo e($event->time); ?></td>
                            </tr>
                            <tr>
                                <td>Location</td>
                                <td>:</td>
                                <td><?php echo e($event->location); ?></td>
                            </tr>
                            <tr>
                                <td>Sector</td>
                                <td>:</td>
                                <td><a href="<?php echo e(route('customers', ['fc' => 'a', 'fs' => $event->sector_id ])); ?>" target="_blank"><?php echo e($event->sector->name); ?></a></td>
                            </tr>
                        </table>
                    </div> <!-- ./panel-body -->
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('modal.customer_consent', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
$('.select2').select2();
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts/main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>