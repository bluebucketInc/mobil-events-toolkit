<div id="modal-video-content" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                <h4 class="modal-title">Preview Video</h4>
            </div>
            <div class="modal-body">
                <video id="video-container" controls style="width: 100%; height: auto">
                    <source id="video-source" src="" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php $__env->startPush('scripts'); ?>
<script>
$('#modal-video-content').on('hidden.bs.modal', function () {
    $('#video-container').trigger('pause');
});
</script>
<?php $__env->stopPush(); ?>