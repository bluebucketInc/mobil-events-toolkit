<?php

use Illuminate\Database\Seeder;

class SectorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sectors')->insert([
            [
                'name' => 'Wind',
                'slug' => 'wind',
                'order' => 0,
                'active' => 1,
            ],
            [
                'name' => 'Mining',
                'slug' => 'mining',
                'order' => 1,
                'active' => 1,
            ],
            [
                'name' => 'Power',
                'slug' => 'power',
                'order' => 2,
                'active' => 1,
            ],
            [
                'name' => 'Machine Shop',
                'slug' => 'machine-shop',
                'order' => 3,
                'active' => 1,
            ],
            [
                'name' => 'Plastics',
                'slug' => 'plastics',
                'order' => 4,
                'active' => 1,
            ],
        ]);
    }
}
