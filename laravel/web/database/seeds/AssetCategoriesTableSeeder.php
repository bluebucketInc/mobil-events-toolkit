<?php

use Illuminate\Database\Seeder;

class AssetCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('asset_categories')->insert([
            [
                'name' => 'Banner',
                'slug' => 'banner',
                'label_color' => '#BA982B',
                'order' => 0,
                'active' => 1,
            ],
            [
                'name' => 'Backdrop',
                'slug' => 'backdrop',
                'label_color' => '#2AB38E',
                'order' => 1,
                'active' => 1,
            ],
            [
                'name' => 'Brochure',
                'slug' => 'brochure',
                'label_color' => '#CE5430',
                'order' => 2,
                'active' => 1,
            ],
            [
                'name' => 'Poster',
                'slug' => 'poster',
                'label_color' => '#682BBA',
                'order' => 3,
                'active' => 1,
            ],
            [
                'name' => 'Video',
                'slug' => 'video',
                'label_color' => '#CE309F',
                'order' => 4,
                'active' => 1,
            ],
        ]);
    }
}
