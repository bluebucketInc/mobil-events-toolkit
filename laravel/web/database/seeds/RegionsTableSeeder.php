<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->insert([
            [
                'name' => 'Singapore',
                'order' => 0,
                'active' => 1,
            ],
            [
                'name' => 'India',
                'order' => 1,
                'active' => 1,
            ],
            [
                'name' => 'Thailand',
                'order' => 2,
                'active' => 1,
            ],
            [
                'name' => 'Asia Pacific HQ (APHQ)',
                'order' => 3,
                'active' => 1,
            ],
            [
                'name' => 'Fully Distributor Served (FDS)',
                'order' => 4,
                'active' => 1,
            ]
        ]);
    }
}
