<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->date('date')->nullable();
            $table->time('time')->nullable();
            $table->string('location', 191)->nullable();
            $table->unsignedInteger('sector_id')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('region_id')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
