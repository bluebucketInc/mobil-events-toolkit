<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('email_template_id')->nullable();
            $table->string('code', 40);
            $table->text('html_content')->nullable();
            $table->text('text_block_1')->nullable();
            $table->text('text_block_2')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('sendinblue_list_id')->nullable();
            $table->unsignedInteger('sendinblue_campaign_id')->nullable();
            $table->boolean('sent')->default(false);
            $table->dateTime('sent_at')->nullable();
            $table->unsignedInteger('stat_sents')->default(0);
            $table->unsignedInteger('stat_opens')->default(0);
            $table->unsignedInteger('stat_clicks')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitations');
    }
}
