<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetCategory extends Model
{
    public $timestamps = false;

    public function assets()
    {
        return $this->hasMany('App\Asset', 'category_id');
    }

    public function region()
    {
        return $this->hasMany('App\Region');
    }
}
