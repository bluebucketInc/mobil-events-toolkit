<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Sector;
use HubSpot;
use Sendinblue;

class Customer extends Model
{
    protected $fillable = ['hubspot_contact_id', 'email', 'first_name', 'last_name', 'company', 'phone_number', 'sector_id', 'job_title_id', 'user_id'];

    public function job_title()
    {
        return $this->belongsTo('App\JobTitle');
    }

    public function sector()
    {
        return $this->belongsTo('App\Sector');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function hubspot_sync($user_id)
    {
        return;
        $sector = Sector::find($this->sector_id);

        $response = HubSpot::contacts()->createOrUpdate($this->email, [
            [
                'property' => 'firstname',
                'value' => $this->first_name,
            ],
            [
                'property' => 'lastname',
                'value' => $this->last_name,
            ],
            [
                'property' => 'sector',
                'value' => $sector ? $sector->name : '',
            ],
            [
                'property' => 'distributor_id',
                'value' => $user_id,
            ]
        ]);
        $this->hubspot_contact_id = $response->data->vid;
        $this->save();
    }

    public static function sendinblue_addContactsIfDoesNotExist($emails)
    {
        $apiInstance = new \SendinBlue\Client\Api\ContactsApi(null, Sendinblue::getConfiguration());

        foreach ($emails as $email) {            
            try {
                $result = $apiInstance->getContactInfo($email);
            } catch (\Exception $e) {
                $createContact = new \SendinBlue\Client\Model\CreateContact();
                $createContact->setEmail($email);
                $createContact->setUpdateEnabled(true);
    
                try {
                    $result = $apiInstance->createContact($createContact);
                } catch (\Exception $e) {
                    dd($e);
                }
            }
        }


        // Add contacts to list
        $listId = 174;
        $contactEmails = new \SendinBlue\Client\Model\AddContactToList(); // \SendinBlue\Client\Model\AddContactToList | Emails addresses of the contacts

        $contactEmails->setEmails($emails);

        try {
            $result = $apiInstance->addContactToList($listId, $contactEmails);
        } catch (\Exception $e) {
            //echo 'Exception when calling ContactsApi->addContactToList: ', $e->getMessage(), PHP_EOL;
        }

    }

    public function sendinblue_sync()
    {
        $apiInstance = new \SendinBlue\Client\Api\ContactsApi(null, Sendinblue::getConfiguration());

        $createContact = new \SendinBlue\Client\Model\CreateContact();
        $createContact->setEmail($this->email);
        $createContact->setAttributes([
            'FIRSTNAME' => $this->first_name,
            'LASTNAME' => $this->last_name,
        ]);
        $createContact->setUpdateEnabled(true);

        try {
            $result = $apiInstance->createContact($createContact);
        } catch (Exception $e) {
            //echo 'Exception when calling ContactsApi->createContact: ', $e->getMessage(), PHP_EOL;
        }
    }

}
