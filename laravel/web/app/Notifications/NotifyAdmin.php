<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotifyAdmin extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->markdown('email.basic')
                    ->subject('Please log in to your admin dashboard to approve an access request to the Mobil Events Toolkit.')
                    ->greeting('Dear EM Administrator,')
                    ->line('A distributor from your market has requested for access to the Mobil Events Toolkit.')
                    ->line('Please log in to your Admin Portal here to approve their access.')
                    ->action('Go to dashboard', route('admin.users.index'))
                    ->line('Please note that you and other fellow designated administrators for your market are able to approve this request. The admin dashboard will indicate who has approved access for each distributor.')
                    ->line('Thank you.')
                    ->line('From the Mobil team.');
        
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
