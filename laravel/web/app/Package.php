<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function assets()
    {
        return $this->belongsToMany('App\Asset');
    }

    public function galleries() {
        return $this->belongsToMany('App\EventGallery');
    }
}
