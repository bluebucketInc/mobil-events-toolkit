<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function privacyPolicy()
    {
        $page = Page::where('slug', 'privacy-policy')->first();
        if (!$page) return redirect('/');
        return view('basic', compact('page'));
    }

    public function contactUs()
    {
        $page = Page::where('slug', 'contact-us')->first();
        if (!$page) return redirect('/');
        return view('basic', compact('page'));
    }
}
