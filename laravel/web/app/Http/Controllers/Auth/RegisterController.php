<?php

namespace App\Http\Controllers\Auth;

use App\JobTitle;
use App\User;
use App\Region;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register/complete';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin');
    }

    public function showRegistrationForm()
    {
        $jobTitles = JobTitle::orderBy('name')->get();
        $regions = Region::where('id', '!=', 1)->orderBy('order')->get();
        return view('auth.register', compact('jobTitles', 'regions'));
    }

    public function showCompletePage()
    {
        return view('auth.register_complete');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required',
            'last_name' => 'required',
            'company' => 'required',
            'region' => 'required',
            //'job_title' => 'required',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'agreed' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'code' => str_random(30),
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'company' => $data['company'],
            'region_id' => $data['region'],
            //'job_title_id' => $data['job_title'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function register(Request $request)
    {
      /**
      Verify reCaptcha ** 20200314
      **/
      if (!$this->isValidCaptcha($request->input('recaptcha_response'))) {

        return redirect()->back()->withErrors([
            'email' => 'Your request is not authorized'
        ])->withInput($request->only('email', 'remember'));
      }

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        if (\Auth::guard('admin')->check()) {
            \Auth::guard('admin')->logout();
        }
        $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    public function isValidCaptcha($recaptcha_response)
    {

    	if ($recaptcha_response == null) return false;

    	$url    = env('VERIFY_URL');
    	$secret = env('SECRET_KEY');
      $score  = env('SCORE');

      $client = new \GuzzleHttp\Client();

    	$response = $client->request('get',
        $url.'?secret='.$secret.'&response='.$recaptcha_response);

    	$json = $response->getBody();
    	$recaptcha = json_decode($json);

    	if (isset($recaptcha->success) && $recaptcha->success == false) return false;

    	return ($recaptcha->score >= $score) ? true : false;
    }

}
