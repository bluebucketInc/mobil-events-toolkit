<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Auth\Events\Verified;
use App\Admin;
use App\User;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be resent if the user did not receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('verify');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function verify(Request $request)
    {
        $id = $request->route('id');
        $user = User::find($id);
        //dd($id);

        if ($user->email_verified_at) {
            return redirect('/');
        }

        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->save();

        // send notification emails to admins in respective region
        $admins = Admin::where('region_id', $user->region_id)->orWhere('be_notified', 1)->get();
        foreach ($admins as $admin) {
            $admin->notify(new \App\Notifications\NotifyAdmin);
        }
        //event(new Verified($user));
        //dd($user);

        //return redirect('/');

        /*if ($request->route('id') != $request->user()->getKey()) {
            throw new AuthorizationException;
        }

        if ($request->user()->hasVerifiedEmail()) {
            return redirect($this->redirectPath());
        }

        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }*/

        //return redirect($this->redirectPath())->with('verified', true);
    }
}
