<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        //$this->middleware('throttle:5,3600');
    }

    public function login(Request $request)
    {
      /**
      Verify reCaptcha ** 20200314
      **/
      if (!$this->isValidCaptcha($request->input('recaptcha_response'))) {

        return redirect()->back()->withErrors([
            'email' => 'Your request is not authorized'
        ])->withInput($request->only('email', 'remember'));
      }


        // Validate the form data

        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        $user = User::where('email', $request->input('email'))->first();
        if (!$user) {
            return redirect()->back()->withErrors([
                'email' => 'Account not found or incorrect password'
            ])->withInput($request->only('email', 'remember'));
        }

        if (!$user->approved_at) {
            return redirect()->back()->withErrors([
                'email' => 'Your account has not been approved yet'
            ])->withInput($request->only('email', 'remember'));
        }

        // Attempt to log the user in
        if (Auth::guard()->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(url('/'));
        }

        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->back()->withErrors([
            'email' => 'Account not found or incorrect password'
        ])->withInput($request->only('email', 'remember'));
    }

    public function logout()
    {
        Auth::guard()->logout();
        return redirect('/');
    }


    public function isValidCaptcha($recaptcha_response)
    {
    	if ($recaptcha_response == null) return false;

    	$url    = env('VERIFY_URL');
    	$secret = env('SECRET_KEY');
      $score  = env('SCORE');

      $client = new \GuzzleHttp\Client();



    	$response = $client->request('get',
        $url.'?secret='.$secret.'&response='.$recaptcha_response);

    	$json = $response->getBody();
    	$recaptcha = json_decode($json);

    	if (isset($recaptcha->success) && $recaptcha->success == false) return false;

    	return ($recaptcha->score >= $score) ? true : false;
    }

}
