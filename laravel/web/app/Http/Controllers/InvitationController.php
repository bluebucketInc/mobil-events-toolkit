<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Customer;
use App\EmailTemplate;
use App\Event;
use App\Invitation;
use App\Sector;
use DB;
use Sendinblue;
use SendinBlue\Client\Model\UpdateEmailCampaignSender;

class InvitationController extends Controller
{

    public function template($id)
    {
        $invitation = Invitation::with('event')->where('id', $id)->first();
        if (!$invitation) return redirect('events')->with('error', 'Invitation ID not found');

        $templates = EmailTemplate::all();
        return view('invitations.template', compact('invitation', 'templates'));
    }

    public function chooseTemplate($id, $template_id)
    {
        $invitation = Invitation::with('event')->where('id', $id)->first();
        if (!$invitation) return redirect('events')->with('error', 'Invitation ID not found');

        $email_template = EmailTemplate::find($template_id);
        
        $invitation->email_template_id = $template_id;

        $html = $email_template->html_content;
        $html = str_replace('[ event name ]', $invitation->event->name, $html);
        $html = str_replace('[ date ]', date('j M y', strtotime($invitation->event->date)), $html);
        $html = str_replace('[ location ]', $invitation->event->location, $html);
        $html = str_replace('[ time ]', $invitation->event->time, $html);
        $json_data = json_decode($invitation->custom_content);
        if (isset($json_data)) {
            if (isset($json_data->distributor_company_name)) {
                $html = str_replace('[ distributor company name ]', $json_data->distributor_company_name, $html);
            }
            if (isset($json_data->contact_details)) {
                $html = str_replace('[ contact details ]', $json_data->contact_details, $html);
            }
        }

        $invitation->html_content = $html;

        $invitation->save();

        return redirect()->route('invitations.customize', $id);
    }

    public function customize($id)
    {
        $invitation = Invitation::with(['email_template', 'event'])->where('id', $id)->first();
        if (!$invitation) return redirect('events')->with('error', 'Invitation ID not found');
        $invitation->json_data = json_decode($invitation->custom_content);

        return view('invitations.customize', compact('invitation'));
    }

    public function confirmation($id)
    {
        $invitation = Invitation::with('email_template')->where('id', $id)->first();
        if (!$invitation) return redirect('events')->with('error', 'Invitation ID not found');
        $invitation->json_data = json_decode($invitation->custom_content);

        return view('invitations.confirmation', compact('invitation'));
    }

    public function test(Request $request, $id)
    {
        $request->validate([
            'emails' => 'required',
        ]);

        
        $invitation = Invitation::with('email_template')->where('id', $id)->first();
        $campaignInstance = new \SendinBlue\Client\Api\EmailCampaignsApi(null, Sendinblue::getConfiguration());
        $campaignId = $invitation->sendinblue_campaign_id;

        // Update the html content first in the Send In Blue Invitation
        $updateCampaign = new \SendinBlue\Client\Model\UpdateEmailCampaign(); // \SendinBlue\Client\Model\UpdateEmailCampaign | Values to update a campaign
        $updateCampaign->setHtmlContent($invitation->html_content);
        $updateCampaign->setSubject($invitation->email_template->subject);
        $updateCampaign->setReplyTo('donotreply@mobil-eventtoolkit.com');
        
        $sender = new UpdateEmailCampaignSender([
            'name' => 'Mobil Event Toolkit',
            'email' => 'admin@mobil-eventtoolkit.com'
        ]);
        $updateCampaign->setSender($sender);

        try {
            $campaignInstance->updateEmailCampaign($campaignId, $updateCampaign);
        } catch (Exception $e) {
            echo 'Exception when calling EmailCampaignsApi->updateEmailCampaign: ', $e->getMessage(), PHP_EOL;
        }

        $emailTo = new \SendinBlue\Client\Model\SendTestEmail(); // \SendinBlue\Client\Model\SendTestEmail | 
        $emails = explode(",", $request->input('emails'));
        Customer::sendinblue_addContactsIfDoesNotExist($emails);
        $emailTo->setEmailTo($emails);

        try {
            $campaignInstance->sendTestEmail($campaignId, $emailTo);
        } catch (\Exception $e) {
            dd($e);
            return redirect()->back()->with('error', 'Email address is not in the Contact List');
        }

        return redirect()->back()->with('success', 'A test email has been sent');
    }

    public function ajax_save(Request $request, $id)
    {
        $invitation = Invitation::with('email_template')->where('id', $id)->first();
        $json_data = new \stdClass;
        $json_data->distributor_company_name = $request->input('distributor_company_name');
        $json_data->contact_details = $request->input('contact_details');
        $invitation->custom_content = json_encode($json_data);

        $html = $invitation->email_template->html_content;
        $html = str_replace('[ event name ]', $invitation->event->name, $html);
        $html = str_replace('[ date ]', date('j M y', strtotime($invitation->event->date)), $html);
        $html = str_replace('[ location ]', $invitation->event->location, $html);
        $html = str_replace('[ time ]', $invitation->event->time, $html);
        if (isset($json_data)) {
            if (isset($json_data->distributor_company_name)) {
                $html = str_replace('[ distributor company name ]', $json_data->distributor_company_name, $html);
            }
            if (isset($json_data->contact_details)) {
                $html = str_replace('[ contact details ]', $json_data->contact_details, $html);
            }
        }
        $invitation->html_content = $html;

        $invitation->save();
        \Session::flash('success', 'Your entry has been successfuly updated');

        return response()->json([
            'status' => 'success',
        ]);
    }

    public function ajax_send(Request $request, $id)
    {
        $invitation = Invitation::with('email_template')->where('id', $id)->first();
        $campaignId = $invitation->sendinblue_campaign_id;

        $campaignInstance = new \SendinBlue\Client\Api\EmailCampaignsApi(null, Sendinblue::getConfiguration());
        
        // Update the html content first in the Send In Blue Invitation
        $updateCampaign = new \SendinBlue\Client\Model\UpdateEmailCampaign(); // \SendinBlue\Client\Model\UpdateEmailCampaign | Values to update a campaign
        $updateCampaign->setHtmlContent($invitation->html_content);
        $updateCampaign->setSubject($invitation->email_template->subject);
        $sender = new UpdateEmailCampaignSender([
            'name' => 'Mobil Event Toolkit',
            'email' => 'admin@mobil-eventtoolkit.com'
        ]);
        $updateCampaign->setSender($sender);
        $updateCampaign->setReplyTo('donotreply@mobil-eventtoolkit.com');

        try {
            $campaignInstance->updateEmailCampaign($campaignId, $updateCampaign);
        } catch (Exception $e) {
            echo 'Exception when calling EmailCampaignsApi->updateEmailCampaign: ', $e->getMessage(), PHP_EOL;
        }

        // Send them invitation
        try {
            $campaignInstance->sendEmailCampaignNow($campaignId);

            $invitation->sent = true;
            $invitation->sent_at = date('Y-m-d H:i:s');
            $invitation->save();

            \Session::flash('success', 'Your email has been sent');
        } catch (Exception $e) {
            echo 'Exception when calling EmailCampaignsApi->sendEmailCampaignNow: ', $e->getMessage(), PHP_EOL;
        }        
    }

}
