<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Event;
use App\Sector;
use App\EventGallery;


use App\Region;
use App\AssetCategory;
use App\Package;
use App\Asset;

use DB;
use Sendinblue;
use File;
use Image;

class EventGalleryController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function selectSector($region_id = 0)
  {
      // $region_id = 1;//\Auth::user()->region_id;
      // $sectors = Sector::with(['assets' => function($q) use ($region_id) {
      //     $q->where('region_id', $region_id);
      // }])->where('active', 1)->orderBy('order')->get();
      $fd1 = \Request::get('fd1') ? \Request::get('fd1') : '1000-01-01';
      $fd2 = \Request::get('fd2') ? \Request::get('fd2') : '9000-12-31';
      $fs  = 'a';
      $fst = 'a';
      $q = Event::with(['sector'])->whereBetween('date', [$fd1, $fd2]);
      // $q = $q->where('user_id', \Auth::user()->id);
      if ($fs != 'a') { $q = $q->where('sector_id', $fs); }
      if ($fst != 'a') { $q = $q->where('status', $fst); }

      if ($region_id != 0) {
        $currentRegion = Region::find($region_id);
        $q->where(['region_id'=>$region_id]);
      } else {
        $currentRegion = new Region;
        $currentRegion->name = "All";
        $currentRegion->id   = 0;
      }


      $events = $q->get();

      //20200325
      $regions = Region::where(['active'=>1])->get();


      return view('gallery.sector', compact('events', 'regions', 'currentRegion'));
  }


  public function browse($event_id = 0, $category=1, $view = 'grid',  $region_id = 1,  $sort = 'newest', $fc = 'all', $search = '')
  {
      $sector_slug = 'all';
      $sector = Sector::first();
      $category = 2;
      $galleries = EventGallery::where('event_id', '=', $event_id)->where('gallery_type', '=', $category)->get();

      foreach ($galleries as $gallery) {
          $arr = explode(".", $gallery->filename);
          $ext = end($arr);
          $gallery->file_extension = $ext;
          if ($ext == 'mp4') {
              $gallery->format = 'video';
          } else if ($ext == 'png' | $ext == 'jpg' | $ext == 'jpeg' | $ext == 'gif') {
              $gallery->format = 'image';
          } else {
              $gallery->format = 'iframe';
          }
          $gallery->magnific_popup_class = $ext == 'mp4' ? 'magnific-popup-player--' : 'magnific-popup';
      }

      //
      // $regions = Region::whereIn('id', [1, \Auth::user()->region_id])->get();
      // $currentRegion = Region::find($region_id);
      //
      // $categories = AssetCategory::with('assets')
      //     ->where('active', 1)
      //     ->orderBy('order')
      //     ->get();
      //
      $newPackage = Package::with('galleries')->where('user_id', \Auth::user()->id)->where('status', 0)->where('package_type', 2)->first();
      $userId = \Auth::user()->id;
      $sector_label   = "Download";
      $category_label = $category == 1? "Events Set-up Assets":"Event Photos";

      return view('gallery.browse', compact(
      'view', 'sector_label', 'category', 'category_label', 'newPackage', 'event_id', 'galleries', 'userId'
    ));
  }


  public function store(Request $request)
  {
    $request->validate([
        'event_id' => 'required',
        'asset' => 'required',
    ]);

    $path = storage_path().'/app/public/event_gallery';
    File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);


    if ($request->hasFile('asset')) {
        $files = $request->file('asset');

        foreach ($files as $file) {
          // code...
          $gallery = new EventGallery;

          $ext = $file->getClientOriginalExtension();

          $filename = str_random(20) . '.' . $ext;

          $file->storeAs('event_gallery', $filename, 'public');

          $gallery->filesize = $file->getSize();
          $gallery->filename = $filename;

          if (strtolower($ext) == 'png' | strtolower($ext) == 'jpg' | strtolower($ext) == 'jpeg' | strtolower($ext) == 'gif') {
            $img = Image::make($file)->orientate();
            $img->fit(400, 400)->save(storage_path('app/public/event_gallery/400x400-' . $filename));
            $img->fit(400, 200)->save(storage_path('app/public/event_gallery/400x200-' . $filename));
            $gallery->preview_filename = $filename;
          } else {
            $gallery->preview_filename = '';
          }

          $gallery->name         = "";
          $gallery->event_id     = $request->input('event_id');
          $gallery->gallery_type = 2;
          $gallery->user_id      = \Auth::user()->id;

          $gallery->save();
        }
    }

    \Session::flash('success', 'You have successfully uploaded a gallery');

    return redirect()->route('eventgallery.browse', ['id' => $request->input('event_id')])->with('success', 'An event gallery was successfully created');


  }

  public function destroy(Request $request)
  {

      $userId = \Auth::user()->id;
      $eventgallery = EventGallery::find($request->input('id'));
      if (!$eventgallery || $eventgallery->user_id != $userId) return redirect()->route('eventgallery.browse', ['event_id'=>$request->input('event_id')])->with('error', 'Event Gallery Photo ID not found');
      $eventgallery->delete();

      return redirect()->back()->with('success', 'An event gallery photo was successfully deleted');;
  }

}
