<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Customer;
use App\Event;
use App\Invitation;
use App\Sector;
use DataTables;
use DB;
use Sendinblue;

class EventController extends Controller
{

    public function index()
    {
        $fd1 = \Request::get('fd1');
        $fd2 = \Request::get('fd2');
        $fs = \Request::get('fs') ? \Request::get('fs') : 'a';
        $fs = \Request::get('fs') ? \Request::get('fs') : 'a';
        $fst = \Request::get('fst') ? \Request::get('fst') : 'a';
        $sectors = Sector::where('active', 1)->orderBy('name', 'ASC')->get();
        return view('events.index', compact('sectors', 'fd1', 'fd2', 'fs', 'fst'));
    }

    public function create()
    {
        $sectors = Sector::where('active', 1)->orderBy('name', 'ASC')->get();
        return view('events.create', compact('sectors'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'date' => 'required',
            'time' => 'required',
            'location' => 'required',
            'sector_id' => 'required',
        ]);

        $event = new Event;
        $event->name = $request->input('name');
        $event->date = $request->input('date');
        $event->time = date('H:i:s', strtotime($request->input('time')));
        $event->location = $request->input('location');
        $event->sector_id = $request->input('sector_id');
        $event->status = 0;
        $event->user_id = \Auth::user()->id;
        $event->region_id = \Auth::user()->region_id;
        $event->save();

        return redirect()->route('events.show', ['id' => $event->id])->with('success', 'An event was successfully created');
    }

    public function show($id)
    {
        $event = Event::with(['sector', 'invitations' => function($q) {
            $q->orderBy('created_at', 'DESC');
        }])->where('id', $id)->first();
        if (!$event) return redirect()->route('events')->with('error', 'Event ID not found');

        return view('events.show', compact('event'));
    }

    public function edit($id)
    {
        $event = Event::find($id);
        if (!$event) return redirect()->route('events')->with('error', 'Event ID not found');
        $event->timef = date('h:i a', strtotime($event->time));

        $sectors = Sector::where('active', 1)->orderBy('name', 'ASC')->get();
        return view('events.edit', compact('event', 'sectors'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'date' => 'required',
            'time' => 'required',
            'location' => 'required',
            'sector_id' => 'required',
        ]);

        $event = Event::find($id);
        $event->name = $request->input('name');
        $event->date = $request->input('date');
        $event->time = date('H:i:s', strtotime($request->input('time')));
        $event->location = $request->input('location');
        $event->sector_id = $request->input('sector_id');
        $event->save();

        return redirect()->route('events.show', ['id' => $event->id])->with('success', 'An event was successfully created');
    }

    public function createInvitation($id)
    {
        $event = Event::find($id);
        if (!$event) return redirect()->route('events')->with('error', 'Event ID not found');

        $user = \Auth::user();
        $customers = Customer::where('user_id', $user->id)->where('sector_id', $event->sector_id)->get();

        if ($customers->count() == 0) {
            return back()->with('error', "You don't have any customers in this sector!");
        }

        $invitation = new Invitation;
        $invitation->event_id = $event->id;
        $invitation->user_id = \Auth::user()->id;
        $invitation->code = Str::random(40);
        $invitation->save();

        $invitation->sendinblue_sync();

        return redirect()->route('invitations.template', $invitation->id)->with('success', 'An invitation successfully created');
    }

    public function collectInvitationsStats($id)
    {
        $event = Event::with(['invitations' => function($q) {
                $q->where('sent', true);
            }])->where('id', $id)->first();
        if (!$event) return redirect()->route('events')->with('error', 'Event ID not found');

        foreach ($event->invitations as $invitation) {
            $campaignInstance = new \SendinBlue\Client\Api\EmailCampaignsApi(null, Sendinblue::getConfiguration());
            $campaignId = $invitation->sendinblue_campaign_id;

            try {
                $result = $campaignInstance->getEmailCampaign($campaignId);
                $stats = $result->getStatistics()['globalStats'];
                //dd($stats);
                $invitation->stat_recipients = $stats->sent;
                $invitation->stat_sents = $stats->delivered;
                $invitation->stat_opens = $stats->viewed;
                $invitation->stat_clicks = $stats->clickers;
                $invitation->stat_softbounces = $stats->softBounces;
                $invitation->save();
            } catch (Exception $e) {
                echo 'Exception when calling EmailCampaignsApi->sendEmailCampaignNow: ', $e->getMessage(), PHP_EOL;
            }
        }

        return redirect()->route('events.show', $id)->with('success', 'All invitations statistics have been recollected');
    }

    public function destroy($id)
    {
        $event = Event::find($id);
        if (!$event) return redirect()->route('events')->with('error', 'Event ID not found');
        $event->delete();

        return redirect()->back()->with('success', 'An event was successfully deleted');;
    }

    public function datatable()
    {
        $fd1 = \Request::get('fd1') ? \Request::get('fd1') : '1000-01-01';
        $fd2 = \Request::get('fd2') ? \Request::get('fd2') : '9000-12-31';
        $fs = \Request::get('fs');
        $fst = \Request::get('fst');

        $q = Event::with(['sector'])->whereBetween('date', [$fd1, $fd2]);
        $q = $q->where('user_id', \Auth::user()->id);
        if ($fs != 'a') { $q = $q->where('sector_id', $fs); }
        if ($fst != 'a') { $q = $q->where('status', $fst); }
        $events = $q->get();

        return DataTables::of($events)
            ->addColumn('sector', function (Event $event) {
                return $event->sector->name;
            })
            ->make(true);
    }

}
