<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\Admin;
use App\AdminLog;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
        //$this->middleware('throttle:5,3600')->except('logout');
    }

    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    public function login(Request $request)
    {

      /**
      Verify reCaptcha ** 20200314
      **/
      if (!$this->isValidCaptcha($request->input('recaptcha_response'))) {

        return redirect()->back()->withErrors([
            'email' => 'Your request is not authorized'
        ])->withInput($request->only('email', 'remember'));
      }


      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);

      $admin = Admin::where('email', $request->input('email'))->first();
      if (!$admin) {
          return redirect()->back()->withErrors([
              'email' => 'Account not found or incorrect password'
          ])->withInput($request->only('email', 'remember'));
      }

      // Attempt to log the user in
      if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
        AdminLog::record('Logged in');
        return redirect()->intended(route('admin.dashboard.index'));
      }

      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withErrors([
        'email' => 'Account not found or incorrect password'
      ])->withInput($request->only('email', 'remember'));
    }

    public function logout()
    {
        AdminLog::record('Logged out');
        Auth::guard('admin')->logout();
        return redirect()->route('admin.auth.login');
    }


    public function isValidCaptcha($recaptcha_response)
    {

    	if ($recaptcha_response == null) return false;

    	$url    = env('VERIFY_URL');
    	$secret = env('SECRET_KEY');
      $score  = env('SCORE');

      $client = new \GuzzleHttp\Client();

    	$response = $client->request('get',
        $url.'?secret='.$secret.'&response='.$recaptcha_response);

    	$json = $response->getBody();
    	$recaptcha = json_decode($json);

    	if (isset($recaptcha->success) && $recaptcha->success == false) return false;

    	return ($recaptcha->score >= $score) ? true : false;
    }
}
