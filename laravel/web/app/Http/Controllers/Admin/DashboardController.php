<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use App\Admin;
use App\AdminLog;
use App\Asset;
use App\Customer;
use App\Event;
use App\User;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $region_id = \Session::get('region_id', \Auth::guard('admin')->user()->region_id);
        $admins = Admin::where('region_id', $region_id)->get();
        $users = User::where('region_id', $region_id)->get();
        $assets = Asset::where('region_id', $region_id)->get();
        $customers = Customer::where('region_id', $region_id)->get();
        $logs = AdminLog::where('admin_id', \Auth::guard('admin')->user()->id)->orderBy('created_at', 'DESC')->take(10)->get();

        return view('admin.dashboard.index', compact('admins', 'assets', 'users', 'customers', 'logs'));
    }

    public function index_bak()
    {
        $host = 'https://api.sendgrid.com/v3/';
        $client = new Client();

        // Add recipients

        $url = $host . 'contactdb/recipients';

        $payload = [
            'headers' => [
                'Authorization' => 'Bearer ' . env('SENDGRID_API_KEY'),
                'Content-Type' => 'application/json',
            ],
            'json' => [
                [
                    'email' => 'gerry.ahmadi@gmail.com',
                    'first_name' => 'Gerry',
                    'last_name' => 'Ahmadi',
                ],
                [
                    'email' => 'ryuga.nukman.nurrahman@gmail.com',
                    'first_name' => 'Ryuga',
                    'last_name' => 'Nukman',
                ],
                [
                    'email' => 'ryuza.adhan.nurrahim@gmail.com',
                    'first_name' => 'Ryuza',
                    'last_name' => 'Adhan',
                ]
            ],
        ];

        $response = $client->request('POST', $url, $payload);
        $json = (json_decode($response->getBody()));
        $persisted_recipients = $json->persisted_recipients;

        // Create list

        $url = $host . 'contactdb/lists';

        $payload = [
            'headers' => [
                'Authorization' => 'Bearer ' . env('SENDGRID_API_KEY'),
                'Content-Type' => 'application/json',
            ],
            'json' => [
                'name' => 'extoolkit_001_001',
            ],
        ];

        $response = $client->request('POST', $url, $payload);
        $json = (json_decode($response->getBody()));
        $list_id = $json->id;

        // Insert contacts to created list
 
        $url = $host . 'contactdb/lists/' . $list_id . '/recipients';

        $payload = [
            'headers' => [
                'Authorization' => 'Bearer ' . env('SENDGRID_API_KEY'),
                'Content-Type' => 'application/json',
            ],
            'json' => $persisted_recipients,
        ];

        $response = $client->request('POST', $url, $payload);
        $json = (json_decode($response->getBody()));
        dd($json);

        return view('admin.dashboard.index');
    }
}
