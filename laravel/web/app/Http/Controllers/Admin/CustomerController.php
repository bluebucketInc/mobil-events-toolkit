<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DataTables;
use File;
use HubSpot;
use Image;

use App\AdminLog;
use App\Customer;
use App\JobTitle;
use App\Sector;
use App\User;

class CustomerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $users = User::orderBy('first_name')->get();
        $sectors = Sector::where('active', 1)->orderBy('order')->get();
        return view('admin.customers.index', compact('users', 'sectors'));
    }

    public function create()
    {
        $sectors = Sector::where('active', 1)->orderBy('order')->get();
        $jobTitles = JobTitle::orderBy('name')->get();
        return view('admin.customers.create', compact('sectors', 'jobTitles'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'company' => 'required',
            'job_title' => 'required',
            'email' => 'required',
        ]);

        $customer = new Customer;
        $customer->email        = $request->input('email');
        $customer->first_name   = $request->input('first_name');
        $customer->last_name    = $request->input('last_name');
        $customer->company      = $request->input('company');
        $customer->job_title_id = $request->input('job_title');
        $customer->sector_id    = $request->input('sector_id');
        $customer->user_id      = null;
        $customer->save();

        $customer->hubspot_sync(\Auth::user()->id);

        AdminLog::record('Created a customer: ' . $customer->name, ['customer_id' => $customer->id]);

        return redirect()->route('admin.customers.index');
    }

    public function show($id)
    {
        $customer = Customer::find($id);
        $sectors = Sector::where('active', 1)->orderBy('order')->get();
        $jobTitles = JobTitle::orderBy('name')->get();

        return view('admin.customers.show', compact('customer', 'sectors', 'jobTitles'));
    }

    public function destroy($id)
    {
        if (\Auth::guard('admin')->user()->role != 'Super Admin') return back();

        $customer = Customer::find($id);
        //HubSpot::contacts()->delete($customer->hubspot_contact_id);
        AdminLog::record('Deleted a customer: ' . $customer->name, ['customer_id' => $customer->id]);
        $customer->delete();
        
        return back();
    }

    public function datatable()
    {
        $admin = \Auth::guard('admin')->user();
        $region_id = \Session::get('region_id', \Auth::guard('admin')->user()->region_id);
        $customers = Customer::with(['sector', 'user'])->where('region_id', $region_id)->get();
        return DataTables::of($customers)
            ->make(true);
    }

}
