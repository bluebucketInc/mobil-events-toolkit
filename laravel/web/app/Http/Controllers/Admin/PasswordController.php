<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class PasswordController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function edit()
    {
        $user = \Auth::user();
        return view('admin.passwords.edit', compact('user'));
    }

    public function update(Request $request)
    {
        $this->validate($request, array(
            'old_password' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|min:6',
        ));

        $user = \Auth::user();

        if (!\Hash::check($request->old_password, $user->password)) {
            return redirect()->back();
        }

        if ($request->new_password != $request->confirm_password) {
            return redirect()->back();
        }

        $user->password = \Hash::make($request->new_password);
        $user->save();

        return redirect()->route('admin.dashboard.index');
    }


}
