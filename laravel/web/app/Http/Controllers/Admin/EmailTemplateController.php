<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use VerumConsilium\Browsershot\Facades\Screenshot;

use File;
use Image;
use Sendinblue;

use App\EmailTemplate;

class EmailTemplateController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $templates = EmailTemplate::orderBy('active', 'DESC')
                        ->orderBy('id')
                        ->get();
        return view('admin.emailtemplates.index', compact('templates'));
    }

    public function edit($id)
    {
        $template = EmailTemplate::find($id);
        return view('admin.emailtemplates.edit', compact('template'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $template = EmailTemplate::find($id);

        if ($request->hasFile('image_preview_filename')) {
            File::delete(storage_path('app/public/email_templates/' . $template->image_preview_filename));

            $file = $request->file('image_preview_filename');
            $ext = $file->getClientOriginalExtension();
            
            $filename = str_random(20) . '.' . $ext;
            $img = Image::make($file)->orientate();
            $img->fit(200, 200)->save(storage_path('app/public/email_templates/' . $filename));
            $template->image_preview_filename = $filename;
        }

        $template->name = $request->input('name');
        $template->save();

        return redirect()->back()->with('success', 'Email template has been updated');
    }

    public function syncFromSendinblue()
    {
        $apiInstance = new \SendinBlue\Client\Api\SMTPApi(null, Sendinblue::getConfiguration());
        
        try {
            $result = $apiInstance->getSmtpTemplates();
            foreach ($result['templates'] as $template) {
                $et = EmailTemplate::find($template['id']);
                if (!$et) {
                    $et = new EmailTemplate;
                    $et->id = $template['id'];
                }

                /*
                if ($et->image_preview_filename) {
                    //delete previous image files
                    File::delete(public_path('uploads/email_templates/' . $et->image_preview_filename));
                    File::delete(public_path('uploads/email_templates/t-'. $et->image_preview_filename));
                }
                */
              
                $et->name = $template['name'];
                $et->subject = $template['subject'];
                $et->html_content = $template['htmlContent'];

                /*
                // Create email template image
                $filename = str_random(25) . '.png';
                Screenshot::loadHtml($et->html_content)
                    ->fullPage()
                    ->save(public_path('uploads/email_templates/' . $filename));
                $et->image_preview_filename = $filename;

                // Create image thumbnail
                Image::make(public_path('uploads/email_templates/' . $filename))
                    ->fit(800, 800, null, 'top')
                    ->save(public_path('uploads/email_templates/t-' . $filename));
                */

                $et->save();
            }
        } catch (Exception $e) {

        }

        return back();
    }

    public function activate(Request $request, $id)
    {
        $template = EmailTemplate::find($id);
        $template->active = 1;
        $template->save();
        return back();
    }

    public function deactivate(Request $request, $id)
    {
        $template = EmailTemplate::find($id);
        $template->active = 0;
        $template->save();
        return back();
    }

}
