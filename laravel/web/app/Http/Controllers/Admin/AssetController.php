<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DataTables;
use File;
use Image;

use App\AdminLog;
use App\Asset;
use App\AssetCategory;
use App\Sector;

class AssetController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function browse($sector_id = null, $category_id = null)
    {
        $region_id = \Session::get('region_id', \Auth::guard('admin')->user()->region_id);

        $sectors = Sector::with(['assets' => function ($query) use ($region_id) {
                        $query->where('region_id', $region_id);
                    }])
                    ->where('active', 1)
                    ->orderBy('order')
                    ->get();
        if (!$sector_id) {
            $sector_id = $sectors[0]->id;
        }

        $categories = AssetCategory::where('active', 1)
                        ->orderBy('order')
                        ->with(['assets' => function ($query) use ($sector_id, $region_id) {
                            $query->where('sector_id', $sector_id);
                            $query->where('region_id', $region_id);
                        }])->get();
        
        $qAssets = Asset::with('category')
                    ->where('sector_id', $sector_id)
                    ->where('region_id', $region_id);
        if ($category_id) {
            $qAssets->where('category_id', $category_id);
        }
        $assets = $qAssets->get();

        foreach ($assets as $asset) {
            $arr = explode(".", $asset->filename);
            $ext = end($arr);
            $asset->file_extension = $ext;
            if ($ext == 'mp4') {
                $asset->format = 'video';
            } else if ($ext == 'png' | $ext == 'jpg' | $ext == 'jpeg' | $ext == 'gif') {
                $asset->format = 'image';
            } else {
                $asset->format = 'iframe';
            }
            $asset->magnific_popup_class = $ext == 'mp4' ? 'magnific-popup-player--' : 'magnific-popup';
        }

        return view('admin.assets.browse', compact('sectors', 'categories', 'assets', 'sector_id', 'category_id'));
    }

    public function create($sector_id = null, $category_id = null)
    {
        $sectors = Sector::where('active', 1)->orderBy('order')->get();
        $categories = AssetCategory::where('active', 1)->orderBy('order')->get();
        return view('admin.assets.create', compact('sectors', 'categories', 'sector_id', 'category_id'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'sector_id' => 'required',
            'asset' => 'required',
        ]);

        //create directory/folder if doesn't exist
        //$path = public_path().'/uploads';
        //File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        $path = storage_path().'/app/public/event_assets';
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

        $asset = new Asset;

        if ($request->hasFile('asset')) {
            $file = $request->file('asset');
            $ext = $file->getClientOriginalExtension();
            
            $filename = str_random(20) . '.' . $ext;
            
            $file->storeAs('event_assets', $filename, 'public');

            $asset->filesize = $file->getSize();
            $asset->filename = $filename;
        }

        if ($request->hasFile('preview')) {
            $file = $request->file('preview');
            $ext = $file->getClientOriginalExtension();
            
            $filename = str_random(20) . '.' . $ext;

            $img = Image::make($file)->orientate();
            $img->fit(800, null)->save(storage_path('app/public/event_assets/' . $filename));
            $img->fit(400, 400)->save(storage_path('app/public/event_assets/400x400-' . $filename));
            $img->fit(400, 200)->save(storage_path('app/public/event_assets/400x200-' . $filename));

            $asset->preview_filename = $filename;
        }

        $asset->name = $request->input('name');
        $asset->sector_id = $request->input('sector_id');
        $asset->category_id = $request->input('category_id');
        $asset->region_id = \Session::get('region_id', \Auth::guard('admin')->user()->region_id);
        
        $asset->save();

        \Session::flash('success', 'You have successfully uploaded an asset');
        AdminLog::record('Created an asset: ' . $asset->name, ['asset_id' => $asset->id]);

        return redirect()->route('admin.assets.browse');
    }

    public function edit($id)
    {
        $sectors = Sector::where('active', 1)->orderBy('order')->get();
        $categories = AssetCategory::where('active', 1)->orderBy('order')->get();
        $asset = Asset::with('sector')
                    ->with('category')
                    ->where('id', $id)
                    ->first();

        $arr = explode(".", $asset->filename);
        $ext = end($arr);
        $asset->file_extension = $ext;

        return view('admin.assets.edit', compact('asset', 'sectors', 'categories'));
    }

    public function update(Request $request, $id)
    {
        //create directory/folder if doesn't exist
        //$path = public_path().'/uploads';
        //File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        $path = storage_path().'/app/public/event_assets';
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

        $asset = Asset::find($id);

        if ($request->hasFile('asset')) {
 
            //delete previous file
            File::delete(storage_path('app/public/event_assets/' . $asset->filename));
 
            $file = $request->file('asset');
            $ext = $file->getClientOriginalExtension();
            
            $filename = str_random(20) . '.' . $ext;
            
            $file->storeAs('event_assets', $filename, 'public');

            $asset->filesize = $file->getSize();
            $asset->filename = $filename;
        }

        if ($request->hasFile('preview')) {
 
            //delete previous file
            File::delete(storage_path('app/public/event_assets/' . $asset->preview_filename));
            File::delete(storage_path('app/public/event_assets/400x400-' . $asset->preview_filename));
            File::delete(storage_path('app/public/event_assets/400-200-' . $asset->preview_filename));

            $file = $request->file('preview');
            $ext = $file->getClientOriginalExtension();
            
            $filename = str_random(20) . '.' . $ext;

            $img = Image::make($file)->orientate();
            $img->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(storage_path('app/public/event_assets/' . $filename));
            $img->fit(400, 400)->save(storage_path('app/public/event_assets/400x400-' . $filename));
            $img->fit(400, 200)->save(storage_path('app/public/event_assets/400x200-' . $filename));

            $asset->preview_filename = $filename;
        }

        $asset->name = $request->input('name');
        $asset->sector_id = $request->input('sector_id');
        $asset->category_id = $request->input('category_id');
        
        $asset->save();

        \Session::flash('success', 'You have successfully updated an asset');
        AdminLog::record('Updated an asset: ' . $asset->name, ['asset_id' => $asset->id]);

        return redirect()->route('admin.assets.browse', ['sector_id' => $asset->sector_id, 'category_id' => $asset->category_id]);
    }

    public function destroy($id)
    {
        $asset = Asset::find($id);
        File::delete(storage_path('app/public/event_assets/' . $asset->filename));
        File::delete(storage_path('app/public/event_assets/' . $asset->preview_filename));
        File::delete(storage_path('app/public/event_assets/400x400-' . $asset->preview_filename));
        File::delete(storage_path('app/public/event_assets/400-200-' . $asset->preview_filename));
        AdminLog::record('Deleted an asset: ' . $asset->name, ['asset_id' => $asset->id]);
        $asset->delete();
        
        return back();
    }

    public function datatable()
    {
        return DataTables::of(Asset::all())
            ->make(true);
    }

}
