<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Event;
use App\EventGallery;
use App\AdminLog;
use DataTables;
use DB;
use File;
use Image;
use Sendinblue;

class EventGalleryController extends Controller
{

    public function index()
    {
        $fd1 = \Request::get('fd1');
        $fd2 = \Request::get('fd2');
        $fs = \Request::get('fs') ? \Request::get('fs') : 'a';
        $fs = \Request::get('fs') ? \Request::get('fs') : 'a';
        $fst = \Request::get('fst') ? \Request::get('fst') : 'a';
        $sectors = Sector::where('active', 1)->orderBy('name', 'ASC')->get();
        return view('events.index', compact('sectors', 'fd1', 'fd2', 'fs', 'fst'));
    }

    public function create($event_id = null)
    {

      $region_id = \Session::get('region_id', \Auth::guard('admin')->user()->region_id);
      $events = Event::with(['user'])->where('region_id', $region_id)->get();
      $event_date = "";
      if ($event_id != null) {
        $event = Event::where(['id'=>$event_id])->first();
        if ($event != null) {
          $event_date = $event->date . " " . $event->time;
        }
      }
      return view('admin.eventgallery.create', compact('events', 'event_id', 'event_date'));
    }

    public function list() {
      $region_id = \Session::get('region_id', \Auth::guard('admin')->user()->region_id);
      $events = Event::where('region_id', $region_id)->get();

      $tmpEvents = array();
      foreach ($events as $event) {
        $file_name = "";
        if ($event->imageGallery->count()==0) {
          if ($event->galleries->count() == 0) {
            $file_name = "other";
          } else {
            $file_name = $event->galleries->first()->filename;
          }
        } else {
          $file_name = $event->imageGallery->first()->filename;
        }

        $arr = explode(".", $file_name);
        $ext = end($arr);
        if ($ext == 'mp4') {
            $ext = 'video';
        } else if (strtolower($ext) == 'png' | strtolower($ext) == 'jpg' | strtolower($ext) == 'jpeg' | strtolower($ext) == 'gif') {
            $ext = 'image';

        } else if (strtolower($ext) == 'pdf') {
            $ext = 'pdf';
        }
        $event->ext = $ext;
        $event->file_name = $file_name;
        array_push($tmpEvents, $event);

      }

      return view('admin.eventgallery.list', compact('tmpEvents'));
    }

    public function detail ($id) {
      $event = Event::where('id', $id)->first();

      $galleries = EventGallery::where('event_id', '=', $event->id)->get();

      foreach ($galleries as $gallery) {
          $arr = explode(".", $gallery->filename);
          $ext = end($arr);
          $gallery->file_extension = $ext;
          if ($ext == 'mp4') {
              $gallery->format = 'video';
          } else if (strtolower($ext) == 'png' | strtolower($ext) == 'jpg' | strtolower($ext) == 'jpeg' | strtolower($ext) == 'gif') {
              $gallery->format = 'image';
          } else {
              $gallery->format = 'iframe';
          }
          $gallery->magnific_popup_class = $ext == 'mp4' ? 'magnific-popup-player--' : 'magnific-popup';
      }

      return view('admin.eventgallery.detail', compact('event', 'galleries'));
    }

    public function store(Request $request)
    {
      $request->validate([
          'event_id' => 'required',
          'asset' => 'required',
      ]);

      //create directory/folder if doesn't exist
      //$path = public_path().'/uploads';
      //File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
      $path = storage_path().'/app/public/event_gallery';
      File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);


      if ($request->hasFile('asset')) {
          $files = $request->file('asset');

          foreach ($files as $file) {
            // code...
            $gallery = new EventGallery;

            $ext = $file->getClientOriginalExtension();

            $filename = str_random(20) . '.' . $ext;

            $file->storeAs('event_gallery', $filename, 'public');

            $gallery->filesize = $file->getSize();
            $gallery->filename = $filename;

            if (strtolower($ext) == 'png' | strtolower($ext) == 'jpg' | strtolower($ext) == 'jpeg' | strtolower($ext) == 'gif') {
              $img = Image::make($file)->orientate();
              $img->fit(400, 400)->save(storage_path('app/public/event_gallery/400x400-' . $filename));
              $img->fit(400, 200)->save(storage_path('app/public/event_gallery/400x200-' . $filename));
              $gallery->preview_filename = $filename;
            } else {
              $gallery->preview_filename = '';
            }

            $gallery->name         = "";
            $gallery->event_id     = $request->input('event_id');
            $gallery->gallery_type = $request->input('gallerytype_id');

            $gallery->save();
          }
      }

      // if ($request->hasFile('preview')) {
      //     $file = $request->file('preview');
      //     $ext = $file->getClientOriginalExtension();
      //
      //     $filename = str_random(20) . '.' . $ext;
      //
      //     $img = Image::make($file)->orientate();
      //     $img->fit(800, null)->save(storage_path('app/public/event_gallery/' . $filename));
      //     $img->fit(400, 400)->save(storage_path('app/public/event_gallery/400x400-' . $filename));
      //     $img->fit(400, 200)->save(storage_path('app/public/event_gallery/400x200-' . $filename));
      //
      //     $gallery->preview_filename = $filename;
      // }



      \Session::flash('success', 'You have successfully uploaded a gallery');
      AdminLog::record('Created a gallery: ' . $gallery->name, ['asset_id' => $gallery->id]);

      return redirect()->route('admin.eventgallery.details', ['id' => $gallery->event_id])->with('success', 'An event gallery was successfully created');

        // return redirect()->route('events.show', ['id' => $event->id])->with('success', 'An event was successfully created');
    }

    public function show($id)
    {

        // $event = Event::with(['sector', 'invitations' => function($q) {
        //     $q->orderBy('created_at', 'DESC');
        // }])->where('id', $id)->first();
        // if (!$event) return redirect()->route('events')->with('error', 'Event ID not found');
        //
        // return view('events.show', compact('event'));
    }

    public function edit($id)
    {

      $region_id = \Session::get('region_id', \Auth::guard('admin')->user()->region_id);
      $events = Event::with(['user'])->where('region_id', $region_id)->get();

      $gallery = EventGallery::where('id', $id)
                  ->first();

      $arr = explode(".", $gallery->filename);
      $ext = end($arr);
      $gallery->file_extension = $ext;

      $gallery->file_extension = $ext;
      if ($ext == 'mp4') {
          $gallery->format = 'video';
      } else if (strtolower($ext) == 'png' | strtolower($ext) == 'jpg' | strtolower($ext) == 'jpeg' | strtolower($ext) == 'gif') {
          $gallery->format = 'image';
      } else {
          $gallery->format = 'iframe';
      }


      return view('admin.eventgallery.edit', compact('gallery', 'events'));

    }

    public function update(Request $request, $id)
    {
      //create directory/folder if doesn't exist
      //$path = public_path().'/uploads';
      //File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
      $path = storage_path().'/app/public/event_gallery';
      File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

      $gallery = EventGallery::find($id);


      if ($request->hasFile('asset')) {

          //delete previous file
          File::delete(storage_path('app/public/event_gallery/' . $gallery->filename));
          File::delete(storage_path('app/public/event_gallery/' . $gallery->preview_filename));
          File::delete(storage_path('app/public/event_gallery/400x400-' . $gallery->preview_filename));
          File::delete(storage_path('app/public/event_gallery/400-200-' . $gallery->preview_filename));

          $file = $request->file('asset');
          $ext = $file->getClientOriginalExtension();

          $filename = str_random(20) . '.' . $ext;

          $file->storeAs('event_gallery', $filename, 'public');

          $img = Image::make($file)->orientate();
          $img->resize(800, null, function ($constraint) {
              $constraint->aspectRatio();
          })->save(storage_path('app/public/event_gallery/' . $filename));
          $img->fit(400, 400)->save(storage_path('app/public/event_gallery/400x400-' . $filename));
          $img->fit(400, 200)->save(storage_path('app/public/event_gallery/400x200-' . $filename));


          $gallery->filesize = $file->getSize();
          $gallery->filename = $filename;
          $gallery->preview_filename = $filename;
      }

      // if ($request->hasFile('preview')) {
      //
      //     //delete previous file
      //     File::delete(storage_path('app/public/event_gallery/' . $gallery->preview_filename));
      //     File::delete(storage_path('app/public/event_gallery/400x400-' . $gallery->preview_filename));
      //     File::delete(storage_path('app/public/event_gallery/400-200-' . $gallery->preview_filename));
      //
      //     $file = $request->file('preview');
      //     $ext = $file->getClientOriginalExtension();
      //
      //     $filename = str_random(20) . '.' . $ext;
      //
          // $img = Image::make($file)->orientate();
          // $img->resize(800, null, function ($constraint) {
          //     $constraint->aspectRatio();
          // })->save(storage_path('app/public/event_gallery/' . $filename));
          // $img->fit(400, 400)->save(storage_path('app/public/event_gallery/400x400-' . $filename));
          // $img->fit(400, 200)->save(storage_path('app/public/event_gallery/400x200-' . $filename));
      //
      //     $gallery->preview_filename = $filename;
      //     $gallery->filename = $filename;
      // }
      $gallery->event_id     = $request->input('event_id');
      $gallery->gallery_type = $request->input('gallerytype_id');

      $gallery->save();

      \Session::flash('success', 'You have successfully updated an event gallery');
      AdminLog::record('Updated an event gallery: ' . $gallery->name, ['gallery_id' => $gallery->id]);

      return redirect()->route('admin.eventgallery.details', ['id' => $gallery->event_id])->with('success', 'The event gallery was successfully updated');
    }


    public function destroy($id)
    {
        $eventgallery = EventGallery::find($id);
        if (!$eventgallery) return redirect()->route('events')->with('error', 'Event Gallery Photo ID not found');
        $eventgallery->delete();

        return redirect()->back()->with('success', 'An event gallery photo was successfully deleted');;
    }

}
