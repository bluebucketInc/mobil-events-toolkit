<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DataTables;
use File;
use Image;

use App\AdminLog;
use App\AssetCategory;

class AssetCategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.assetcategories.index');
    }

    public function create()
    {
        $last_category = AssetCategory::orderBy('order', 'DESC')->first();
        $new_order = $last_category ? $last_category->order + 1 : 0;

        return view('admin.assetcategories.create', compact('new_order'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:asset_categories',
            'slug' => 'required|unique:asset_categories',
            'order' => 'required|numeric',
            'label_color' => 'required',
        ]);

        $category = new AssetCategory;
        $category->name        = $request->input('name');
        $category->slug        = $request->input('slug');
        $category->label_color = $request->input('label_color');
        $category->active      = 1;
        $category->save();

        AdminLog::record('Created an asset category: ' . $category->name, ['asset_category_id' => $category->id]);

        return redirect()->route('admin.assetcategories.index')->with('success', 'A new asset category sucessfully created');
    }

    public function edit($id)
    {
        $assetCategory = AssetCategory::find($id);

        return view('admin.assetcategories.edit', compact('assetCategory'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:asset_categories',
            'slug' => 'required|unique:asset_categories',
            'order' => 'required|numeric',
            'label_color' => 'required',
        ]);

        $category = AssetCategory::find($id);
        $category->name        = $request->input('name');
        $category->slug        = $request->input('slug');
        $category->label_color = $request->input('label_color');
        $category->active      = 1;
        $category->save();

        AdminLog::record('Edited an asset category: ' . $category->name, ['asset_category_id' => $category->id]);

        return redirect()->back()->with('success', 'An asset category sucessfully created');
    }

    public function destroy($id)
    {
        $assetCategory = AssetCategory::find($id);
        AdminLog::record('Deleted an asset category: ' . $assetCategory->name, ['asset_category_id' => $assetCategory->id]);
        if ($assetCategory) $assetCategory->delete();
        
        \Session::flash('success', 'An asset category successfully deleted');

        return back();
    }

    public function datatable()
    {
        return DataTables::of(AssetCategory::all())
            ->make(true);
    }

}
