<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DataTables;
use Mail;

use App\Article;
use App\Contact;
use App\EmailTemplate;
use App\Invitation;
use App\Mail\ExhibitionInvitation;

class InvitationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.invitations.index');
    }

    public function create()
    {
        $templates = EmailTemplate::where('active', true)->orderBy('name')->get();
        return view('admin.invitations.create', compact('templates'));
    }

    public function store(Request $request)
    {
        $template = EmailTemplate::find($request->input('email_template_id'));

        $code = str_random(40);

        $contacts = Contact::where('distributor_id', NULL)->get();
        $this->sendToContacts($contacts, $template->hubspot_template_id, $code);

        $invitation = new Invitation;
        $invitation->email_template_id = $request->input('email_template_id');
        $invitation->user_id = \Auth::user()->id;
        $invitation->sent_at = \Carbon\Carbon::now();
        $invitation->code = $code;
        $invitation->save();

        return redirect()->route('admin.invitations.index');
    }

    public function edit($id)
    {
        $invitation = Invitation::find($id);
        $template = EmailTemplate::find($request->input('email_template_id'));

        return view('admin.invitations.edit', compact('invitation', 'invitation'));
    }

    public function update(Request $request, $id)
    {
        $invitation = Invitation::find($id);
        $invitation->email_template_id = $request->input('email_template_id');
        $invitation->user_id = \Auth::user()->id;
        $invitation->sent_at = \Carbon\Carbon::now();
        $invitation->first_name = $request->input('first_name');
        $invitation->last_name  = $request->input('last_name');
        $invitation->save();

        return redirect()->back();
    }

    public function destroy($id)
    {
        $invitation = Invitation::find($id);
        $invitation->delete();
        
        return back();
    }

    public function datatable()
    {
        return DataTables::of(Invitation::all())
            ->make(true);
    }

    private function sendToContacts($contacts, $template_id, $code)
    {
        $personalizations = [];
        foreach ($contacts as $contact) {
            $personalizations[] = [
                'to' => [
                    'email' => $contact->email,
                    'name'  => $contact->first_name . ' ' . $contact->last_name,
                ],
                'dynamic_template_data' => [
                    'firstName' => $contact->first_name,
                ],
            ];
        }
        Mail::to('gerry.ahmadi@gmail.com', 'Gerry Ahmadi')
            ->send(new ExhibitionInvitation($personalizations, $template_id, $code));
    }

}
