<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DataTables;
use File;
use Image;

use App\Event;
use App\EventGallery;

class EventController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.events.index');
    }

    public function show($id)
    {
        $event = Event::with(['sector', 'invitations' => function($q) {
            $q->orderBy('created_at', 'DESC');
        }])->where('id', $id)->first();

        $galleries = EventGallery::where('event_id', '=', $event->id)->get();

        foreach ($galleries as $gallery) {
            $arr = explode(".", $gallery->filename);
            $ext = end($arr);
            $gallery->file_extension = $ext;
            if ($ext == 'mp4') {
                $gallery->format = 'video';
            } else if ($ext == 'png' | $ext == 'jpg' | $ext == 'jpeg' | $ext == 'gif') {
                $gallery->format = 'image';
            } else {
                $gallery->format = 'iframe';
            }
            $gallery->magnific_popup_class = $ext == 'mp4' ? 'magnific-popup-player--' : 'magnific-popup';
        }

        return view('admin.events.show', compact('event', 'galleries'));
    }

    public function datatable()
    {
        $region_id = \Session::get('region_id', \Auth::guard('admin')->user()->region_id);
        $events = Event::with(['user', 'sector', 'invitations'])->where('region_id', $region_id)->get();
        return DataTables::of($events)
            ->addColumn('sector_name', function (Event $event) {
                if ($event->sector) {
                    return $event->sector->name;
                } else {
                    return '';
                }
            })
            ->addColumn('distributor_name', function (Event $event) {
                if ($event->user) {
                    return $event->user->first_name . ' ' . $event->user->last_name;
                } else {
                    return '';
                }
            })
            ->addColumn('invitations_count', function (Event $event) {
                if ($event->invitations) {
                    return $event->invitations->count();
                } else {
                    return 0;
                }
            })
            ->make(true);
    }

}
