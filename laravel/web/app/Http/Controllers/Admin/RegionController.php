<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DataTables;
use File;
use Image;

use App\AdminLog;
use App\Region;

class RegionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.regions.index');
    }

    public function create()
    {
        $last_region = Region::orderBy('order', 'DESC')->first();
        $new_order = $last_region ? $last_region->order + 1 : 0;
        return view('admin.regions.create', compact('new_order'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:regions',
            'order' => 'required|numeric',
        ]);
        
        $region = new Region;

        $region->name = $request->input('name');
        $region->order = $request->input('order');

        $region->save();

        AdminLog::record('Created a region: ' . $region->name, ['region_id' => $region->id]);

        return redirect()->route('admin.regions.index');
    }

    public function edit($id)
    {
        $region = Region::find($id);

        return view('admin.regions.edit', compact('region'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:regions,id,' . $id,
            'order' => 'required|numeric',
            'active' => 'required',
        ]);

        $region = Region::find($id);

        $region->name = $request->input('name');
        $region->order = $request->input('order');
        $region->active = $request->input('active');

        $region->save();

        AdminLog::record('Updated a region: ' . $region->name, ['region_id' => $region->id]);

        return redirect()->back();
    }

    public function destroy($id)
    {
        $region = Region::find($id);
        AdminLog::record('Deleted a region: ' . $region->name, ['region_id' => $region->id]);
        $region->delete();
        return back();
    }

    public function datatable()
    {
        return DataTables::of(Region::all())
            ->make(true);
    }

    public function change($id)
    {
        \Session::put('region_id', $id);
        return back();
    }

}
