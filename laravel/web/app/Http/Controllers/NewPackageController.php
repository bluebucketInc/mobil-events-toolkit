<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\Eventgallery;
use App\Package;
use App\Jobs\CompressPackage;
use Session;

class NewPackageController extends Controller
{
    public function addAsset($asset_id)
    {
        $package_type = 1;
        $newPackage = Package::where('user_id', \Auth::user()->id)->where('status', 0)->where('package_type', $package_type)->first();
        if (!$newPackage) {
            $newPackage = new Package;
            $newPackage->user_id = \Auth::user()->id;
            $newPackage->status = 0;
            $newPackage->package_type = $package_type;
            $newPackage->save();
        }
        $count = $newPackage->assets->count();
        if (!$newPackage->assets->contains($asset_id)) {
            $newPackage->assets()->attach($asset_id);
            return response()->json([
                'status' => 'ok',
                'change' => 'add',
                'total' => $count + 1,
            ]);
        } else {
            $newPackage->assets()->detach($asset_id);
            return response()->json([
                'status' => 'ok',
                'change' => 'remove',
                'total' => $count - 1,
            ]);
        }
    }

    public function compress()
    {
        $package_type = 1;
        $newPackage = Package::where('user_id', \Auth::user()->id)->where('package_type', $package_type)->where('status', 0)->first();
        if ($newPackage) {
            CompressPackage::dispatch($newPackage);
        }
        Session::flash('compress', 'compress');
        return redirect()->back();
    }

    public function deselectAll()
    {
        $package_type = 1;
        $newPackage = Package::where('user_id', \Auth::user()->id)->where('package_type', $package_type)->where('status', 0)->first();
        if ($newPackage) {
            foreach ($newPackage->assets as $asset) {
                $newPackage->assets()->detach($asset->id);
            }
        }
        return redirect()->back();
    }





    public function addGallery($gallery_id)
    {
        $package_type = 2;
        $newPackage = Package::where('user_id', \Auth::user()->id)->where('status', 0)->where('package_type', $package_type)->first();
        if (!$newPackage) {
            $newPackage = new Package;
            $newPackage->user_id = \Auth::user()->id;
            $newPackage->status = 0;
            $newPackage->package_type = $package_type;
            $newPackage->save();
        }
        $count = $newPackage->galleries->count();
        if (!$newPackage->galleries->contains($gallery_id)) {
            $newPackage->galleries()->attach($gallery_id);
            return response()->json([
                'status' => 'ok',
                'change' => 'add',
                'total' => $count + 1,
            ]);
        } else {
            $newPackage->galleries()->detach($gallery_id);
            return response()->json([
                'status' => 'ok',
                'change' => 'remove',
                'total' => $count - 1,
            ]);
        }
    }

    public function compressGallery()
    {
        $package_type = 2;
        $newPackage = Package::where('user_id', \Auth::user()->id)->where('package_type', $package_type)->where('status', 0)->first();
        if ($newPackage) {
            CompressPackage::dispatch($newPackage);
        }
        Session::flash('compress', 'compress');
        return redirect()->back();
    }

    public function deselectAllGallery()
    {
        $package_type = 2;
        $newPackage = Package::where('user_id', \Auth::user()->id)->where('package_type', $package_type)->where('status', 0)->first();
        if ($newPackage) {
            foreach ($newPackage->assets as $asset) {
                $newPackage->assets()->detach($asset->id);
            }
        }
        return redirect()->back();
    }
}
