<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\AssetCategory;
use App\Package;
use App\Sector;
use App\Region;

class AssetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function selectSector()
    {
        $region_id = 1;//\Auth::user()->region_id;
        $sectors = Sector::with(['assets' => function($q) use ($region_id) {
            $q->where('region_id', $region_id);
        }])->where('active', 1)->orderBy('order')->get();
        return view('assets.sector', compact('sectors'));
    }

    public function browse($sector_slug, $region_id = 1, $view = 'grid', $sort = 'newest', $fc = 'all', $search = '')
    {

        if ($sector_slug == 'all') {
            $sector = Sector::first();
            $sector_label = 'All';
        } else {
            $sector = Sector::where('slug', $sector_slug)->first();
            $sector_label = $sector->name;
        }
        if (!$sector) {
            return redirect('assets');
        }

        $regions = Region::whereIn('id', [1, \Auth::user()->region_id])->get();
        $currentRegion = Region::find($region_id);

        $categories = AssetCategory::with('assets')
            ->where('active', 1)
            ->orderBy('order')
            ->get();

        $newPackage = Package::with('assets')->where('user_id', \Auth::user()->id)->where('status', 0)->where('package_type', 1)->first();

        $user = \Auth::user();
        $q = Asset::with('category')
                ->Where('name', 'LIKE', '%' . $search . '%')
                ->where('region_id', $region_id);

        if ($sector_slug != 'all') {
            $q = $q->where('sector_id', $sector->id);
        }

        if ($fc != 'all') {
            $q = $q->whereHas('category', function ($query) use ($fc) {
                $query->where('slug', $fc);
            });
        }

        if ($sort == 'oldest') {
            $q = $q->orderBy('created_at', 'asc');
        } else if ($sort == 'smallest') {
            $q = $q->orderBy('filename', 'asc');
        } else if ($sort == 'largest') {
            $q = $q->orderBy('filename', 'desc');
        } else {
            $q = $q->orderBy('created_at', 'desc');
        }

        $assets = $q->paginate(16);
        foreach ($assets as $asset) {
            $arr = explode(".", $asset->filename);
            $ext = end($arr);
            $asset->file_extension = $ext;
            if ($ext == 'mp4') {
                $asset->format = 'video';
            } else if ($ext == 'png' | $ext == 'jpg' | $ext == 'jpeg' | $ext == 'gif') {
                $asset->format = 'image';
            } else if ($ext == 'pdf') {
                $asset->format = 'pdf';
            } else {
                $asset->format = 'iframe';
            }
        }

        return view('assets.browse', compact('assets', 'categories', 'regions', 'currentRegion', 'newPackage', 'sector', 'sector_slug', 'sector_label', 'region_id', 'fc', 'sort', 'view', 'search'));
    }

}
