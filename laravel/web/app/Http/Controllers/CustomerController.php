<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use DB;
use App\Customer;
use App\JobTitle;
use App\Sector;
use App\Imports\CustomersImport;
use Maatwebsite\Excel\Facades\Excel;

class CustomerController extends Controller
{

    public function index($fc = 'a', $fs = 'a')
    {
        $user = \Auth::user();
        $companies = DB::table('customers')->select('company AS name')->distinct('company')->where('user_id', $user->id)->orderBy('company', 'ASC')->get();
        $sectors = Sector::where('active', 1)->orderBy('name', 'ASC')->get();
        return view('customers.index', compact('companies', 'sectors', 'user', 'fc', 'fs'));
    }

    public function create()
    {
        $jobTitles = JobTitle::orderBy('name', 'ASC')->get();
        $sectors = Sector::where('active', 1)->orderBy('order', 'ASC')->get();
        $user = \Auth::user();
        return view('customers.create', compact('jobTitles', 'sectors', 'user'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'company' => 'required',
            'sector_id' => 'required',
            'job_title_id' => 'required',
        ]);
        $checkReceive = isset($request->check_receive)?$request->check_receive:0;

        $user = \Auth::user();

        $customer = new Customer;
        $customer->email = $request->email;
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->company = $request->company;
        $customer->sector_id = $request->sector_id;
        $customer->job_title_id = $request->job_title_id;
        $customer->user_id = $user->id;
        $customer->region_id = $user->region_id;
        $customer->check_receive = $checkReceive;
        $customer->customer_source = 2; //Distributors

        $customer->save();

        $customer->hubspot_sync(\Auth::user()->id);
        $customer->sendinblue_sync();

        return redirect()->route('customers')->with('success', 'Contact list has been updated');
    }

    public function edit($id)
    {
        $customer = Customer::find($id);
        if (!$customer) return redirect()->route('customers');

        $jobTitles = JobTitle::orderBy('name', 'ASC')->get();
        $sectors = Sector::where('active', 1)->orderBy('order', 'ASC')->get();
        return view('customers.edit', compact('customer', 'jobTitles', 'sectors'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'company' => 'required',
            'sector_id' => 'required',
            'job_title_id' => 'required',
            'source' => 'required',
        ]);
        $checkReceive = isset($request->check_receive)?$request->check_receive:0;

        $customer = Customer::find($id);
        $customer->email = $request->email;
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->company = $request->company;
        $customer->sector_id = $request->sector_id;
        $customer->job_title_id = $request->job_title_id;
        $customer->check_receive = $checkReceive;
        $customer->customer_source = $request->source;

        $customer->save();

        $customer->hubspot_sync(\Auth::user()->id);
        $customer->sendinblue_sync();

        return redirect()->route('customers')->with('success', 'Contact list has been updated');
    }

    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete();

        return redirect()->route('customers')->with('success', 'Contact list has been updated');
    }

    public function import()
    {
        return view('customers.import');
    }

    public function uploadExcel(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xls,xlsx'
        ]);

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            Excel::import(new CustomersImport(\Auth::user()->id), $file);
            return redirect()->route('customers')->with(['success' => 'You have successfully imported customers from excel file']);
        }
        return redirect('customers');
    }

    public function datatable($fc, $fs)
    {
        $q = Customer::with(['sector', 'job_title']);
        $q = $q->where('user_id', \Auth::user()->id);
        if ($fc != 'a') { $q = $q->where('company', $fc); }
        if ($fs != 'a') { $q = $q->where('sector_id', $fs); }
        $customers = $q->get();
        return DataTables::of($customers)
            ->addColumn('name', function (Customer $customer) {
                return $customer->first_name . ' ' . $customer->last_name;
            })
            ->addColumn('sector', function (Customer $customer) {
                return $customer->sector->name;
            })
            ->addColumn('check_receive', function (Customer $customer) {
                return $customer->check_receive == 1?'Yes':'No';
            })
            ->addColumn('customer_source', function (Customer $customer) {
                return $customer->customer_source == 1?'Customer':($customer->customer_source == 2?'Distributor':'-');
            })
            ->make(true);
    }
}
