<?php

namespace App\Imports;

use App\Customer;
use App\Sector;
use App\JobTitle;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;

class CustomersImport implements ToCollection, WithHeadingRow
{
    var $user_id;
    var $sectors;

    public function __construct($user_id)
    {
        $this->user_id = $user_id;
        $this->sectors = Sector::all();
    }

    public function collection(Collection $collection)
    {
        $region_id = \Auth::user()->region_id;
        foreach ($collection as $row) {
            if ( !$row['first_name'] ||
                 !$row['last_name'] ||
                 !$row['email'] ) {
                     continue;
                 }

            $sector_id = 1;
            foreach ($this->sectors as $sector) {
                if ($sector->name == $row['sector']) {
                    $sector_id = $sector->id;
                    break;
                }
            }

            $email = $row['email'];
            $user_id = $this->user_id;
            $customer = Customer::where('email', $email)->where('user_id', $user_id)->first();
            if (!$customer) {
                $customer = new Customer;
                $customer->email = $email;
                $customer->user_id = $user_id;
            }
            $customer->hubspot_contact_id = null;
            $customer->first_name      = $row['first_name'];
            $customer->last_name       = $row['last_name'];
            $customer->company         = $row['company'];
            $customer->phone_number    = $row['phone_number'];
            $customer->sector_id       = $sector_id;
            $customer->region_id       = $region_id;
            $customer->check_receive   = $row['mailing_list'] == "Yes"?1:0;
            $customer->customer_source = $row['source'] == "Distributor"?2:1; //customer


            $customer->save();

            $customer->hubspot_sync($user_id);
            $customer->sendinblue_sync();

            /*
            Customer::updateOrCreate(
                [
                    'email' => $row['email'],
                    'user_id' => $this->user_id,
                ],
                [
                    'hubspot_contact_id' => null,
                    'first_name' => $row['first_name'],
                    'last_name' => $row['last_name'],
                    'company' => $row['company'],
                    'phone_number' => $row['phone_number'],
                    'sector_id' =>  $sector_id,
                    'job_title_id' => $job_title_id,
                ]
            );
            */
        }
    }

}
