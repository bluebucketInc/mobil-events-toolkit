<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Sendinblue;;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'company', 'email', 'password', 'region_id', 'job_title_id', 'code',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    protected $dates = ['approved_at'];

    public function name()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function approver()
    {
        return $this->hasOne('App\Admin', 'id', 'approved_by');
    }

    public function region()
    {
        return $this->hasMany('App\Region');
    }

    public function sendInBlue_createList()
    {
        $apiInstance = new \SendinBlue\Client\Api\SMTPApi(null, Sendinblue::getConfiguration());
        
        try {
            $result = $apiInstance->getSmtpTemplates();
            foreach ($result['templates'] as $template) {
                $et = EmailTemplate::find($template['id']);
                if (!$et) {
                    $et = new EmailTemplate;
                    $et->id = $template['id'];
                }

                if ($et->image_preview_filename) {
                    //delete previous image files
                    File::delete(public_path('uploads/email_templates/' . $et->image_preview_filename));
                    File::delete(public_path('uploads/email_templates/t-'. $et->image_preview_filename));
                }

                $et->name = $template['name'];
                $et->html_content = $template['htmlContent'];

                // Create email template image
                $filename = str_random(25) . '.png';
                Screenshot::loadHtml($et->html_content)
                    ->fullPage()
                    ->save(public_path('uploads/email_templates/' . $filename));
                $et->image_preview_filename = $filename;

                // Create image thumbnail
                Image::make(public_path('uploads/email_templates/' . $filename))
                    ->fit(800, 800, null, 'top')
                    ->save(public_path('uploads/email_templates/t-' . $filename));

                $et->save();
            }
        } catch (Exception $e) {

        }

        return back();
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new \App\Notifications\VerifyEmail);
    }

}
