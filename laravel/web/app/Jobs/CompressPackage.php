<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

use App\Package;
use App\Mail\SendPackageNotificationMailable;
use Zipper;


class CompressPackage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $package;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Package $package)
    {
        $this->package = $package;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $filename = 'EventToolkitPackage_'. date('Ymd') . '_' . Str::random(5) . '.zip';
        $this->package->status = 1;
        $this->package->filename = $filename;
        $this->package->save();

        $zipper = Zipper::make(storage_path('app/public/packages/' . $filename));

        if ($this->package->package_type == 1) {
          foreach ($this->package->assets as $asset) {
              $arr = explode('.', $asset->filename);
              $ext = end($arr);
              $zipper = $zipper->add(storage_path('app/public/event_assets/' . $asset->filename), $asset->name . '.' . $ext);
          }
        } else {
          foreach ($this->package->galleries as $gallery) {
              $arr = explode('.', $gallery->filename);
              $ext = end($arr);
              $zipper = $zipper->add(storage_path('app/public/event_gallery/' . $gallery->filename), $gallery->name . '.' . $ext);
          }
        }

        $zipper->close();

        $this->package->status = 2;
        $this->package->save();

        $link = asset('uploads/packages/' . $this->package->filename);
        $this->package->user->notify(new \App\Notifications\PackageReady($link));
        /*Mail::to($this->package->user->email)
            ->send(new SendPackageNotificationMailable($link));*/
    }
}
