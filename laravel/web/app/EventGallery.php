<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventGallery extends Model
{
  public function event() {
    $event = Event::where('id', $this->event_id)->first(); 
    return $event;
  }
}
