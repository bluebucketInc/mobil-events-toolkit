ALTER TABLE `customers` ADD `check_receive` SMALLINT NOT NULL DEFAULT '0' ;
ALTER TABLE `customers` ADD `customer_source` TINYINT NOT NULL DEFAULT '0' AFTER `check_receive`;


/** 2020023 **/
ALTER TABLE `packages` ADD `package_type` TINYINT NOT NULL DEFAULT '1' AFTER `filename`;
